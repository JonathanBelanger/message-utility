//
// Copyright (C) Jonathan D. Belanger 2024.
// All Rights Reserved.
//
// This software is furnished under a license and may be used and copied only in accordance with the terms of such
// license and with the inclusion of the above copyright notice.  This software or any other copies thereof may not be
// provided or otherwise made available to any other person.  No title to and ownership of the software is hereby
// transferred.
//
// The information in this software is subject to change without notice and should not be construed as a commitment by
// the author or co-authors.
//
// The author and any co-authors assume no responsibility for the use or reliability of this software.
//
// Description:
//
//! @file
//  This file contains definitions used internally in the Message utility to store the information parsed out of the
//  message definition file.
//
// Revision History:
//
//  V01.000 13-May-2024 Jonathan D. Belanger
//  Initially written.
//
#pragma once
#include "MessageDataTypes.hxx"
#include <string>

//
//! @namespace Msg
//! @brief This namespace is used throughout the definitions and code for the Message Programming Interface.
//
namespace Msg
{

    //
    //! @struct Symbol
    //! @brief This structure contains the parsed out data generated from the Message formatted file in order to be able
    //         to generate the language specific definition and code files.
    //
    struct Symbol
    {
        public:

            //
            //! @fn Symbol(const std::string& symb, const int64 val)
            //! @brief A constructor for the structure where there is only a symbol string and a value.
            //! @param symb A reference to a string for the symbol field
            //! @param val A 64-bit signed int for the value field
            //
            Symbol(const std::string& symb, const int64 val) :
                symbol(symb),
                value(val),
                userValue(0),
                faoCount(0)
                {}

            //
            //! @fn Symbol(const std::string& symb, const int64 val, const int64 usr, const std::string& txt, const int64 fao)
            //! @brief A constructor for the structure where all fields are provided.
            //! @param symb A reference to a string for the symbol field
            //! @param val A 64-bit signed int for the value field
            //! @param usr A 64-bit signed int for the userValue field
            //! @param txt A reference to a string for the text field
            //! @param fao A reference to a value for the faoCount field
            //
            Symbol(const std::string& symb,
                   const int64 val,
                   const int64 usr,
                   const std::string& txt,
                   const int64 fao) :
                symbol(symb),
                value(val),
                userValue(usr),
                faoCount(fao),
                text(txt)
                {}

            std::string symbol;     //!< This field contains the text representation for the symbol (symbol name)
            int64 value;            //!< This field contains the value for the indicated symbol
            int64 userValue;        //!< This field contains the value for the user value supplied in the MSG file
            int64 faoCount;         //!< This field contains the value for the number of FAO directives in text
            std::string text;       //!< This field contains the message text associated with the symbol/value
    };
}
