//
// Copyright (C) Jonathan D. Belanger 2024.
// All Rights Reserved.
//
// This software is furnished under a license and may be used and copied only in accordance with the terms of such
// license and with the inclusion of the above copyright notice.  This software or any other copies thereof may not be
// provided or otherwise made available to any other person.  No title to and ownership of the software is hereby
// transferred.
//
// The information in this software is subject to change without notice and should not be construed as a commitment by
// the author or co-authors.
//
// The author and any co-authors assume no responsibility for the use or reliability of this software.
//
// Description:
//
//! @file
//  This file contains the definitions used by the scanner to be able to call the parser and generate listing files.
//
// Revision History:
//
//  V01.000 09-May-2024 Jonathan D. Belanger
//  Initially written.
//
#pragma once
#include <fstream>

#if ! defined(yyFlexLexerOnce)
#include <FlexLexer.h>
#endif

#undef YY_DECL
#define YY_DECL Msg::Parser::symbol_type Msg::Scanner::get_next_token()

#include "MessageParser.hxx"
#include "location.hh"

//
//! @namespace Msg
//! @brief This namespace is used throughout the definitions and code for the Message Programming Interface.
//
namespace Msg
{

    //
    // @class Scanner
    // @brief This class wraps the scanner into a class with the additional information it will require.
    //
    class Scanner : public yyFlexLexer
    {
        public:

            //
            //! @fn explicit Scanner(std::istream* input, Driver& drvr)
            //! @brief This is the only constructor for the Scanner class.
            //! @param input A pointer to the input stream from the source message formatted file
            //! @param drvr A reference to the driver to process the scanned and parsed input data
            //
            explicit Scanner(std::istream* inStream, Driver& drvr) :
                yyFlexLexer(inStream),
                driver(drvr)
            {}

            using FlexLexer::yylex;

            //
            //! @fn virtual Parser::symbol_type get_next_token()
            //! @brief This function overrides the Flex provided function to get the next token from the input file.
            //
            virtual Parser::symbol_type
            get_next_token();

        private:
            Driver& driver;     //!< A reference to the driver class to generate the output files.
    };
}
