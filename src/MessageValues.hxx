//
// Copyright (C) Jonathan D. Belanger 2024.
// All Rights Reserved.
//
// This software is furnished under a license and may be used and copied only in accordance with the terms of such
// license and with the inclusion of the above copyright notice.  This software or any other copies thereof may not be
// provided or otherwise made available to any other person.  No title to and ownership of the software is hereby
// transferred.
//
// The information in this software is subject to change without notice and should not be construed as a commitment by
// the author or co-authors.
//
// The author and any co-authors assume no responsibility for the use or reliability of this software.
//
// Description:
//
//! @file
//  This file contains the definitions needed throughout the code so that literal values are not present in the code.
//
// Revision History:
//
//  V01.000 06-May-2024 Jonathan D. Belanger
//  Initially written.
//
#pragma once
#include "MessageDataTypes.hxx"

//
//! @namespace Msg
//! @brief This namespace is used throughout the definitions and code for the Message Programming Interface.
//
namespace Msg
{

    //
    //! @class Constants
    //! @brief This static class contains all the constant values needed through out the message utility code.
    //
    class Constants
    {
        public:

            //
            //! @brief The constructor, destructor, and all copy methods are deleted.
            //
            Constants() = delete;
            ~Constants() = delete;
            Constants(const Constants&) = delete;
            Constants& operator=(const Constants&) = delete;

            static constexpr uint32 Warning = 0;                                //!< Warning severity has a b'000' value
            static constexpr uint32 Success = 1;                                //!< Warning severity has a b'001' value
            static constexpr uint32 Error = 2;                                  //!< Warning severity has a b'010' value
            static constexpr uint32 Informational = 3;                          //!< Warning severity has a b'011' value
            static constexpr uint32 Fatal = 4;                                  //!< Warning severity has a b'100' value

            static constexpr uint16 IncludeText = 1;                            //!< Include message text
            static constexpr uint16 DoNotIncludeText = 0;                       //!< Do not include message text
            static constexpr uint16 IncludeIdent = 2;                           //!< Include message identifier
            static constexpr uint16 DoNotIncludeIdent = 0;                      //!< Do not include message identifier
            static constexpr uint16 IncludeSeverity = 4;                        //!< Include message severity
            static constexpr uint16 DoNotIncludeSeverity = 0;                   //!< Do not include message severity
            static constexpr uint16 IncludeFacility = 8;                        //!< Include the message facility
            static constexpr uint16 DoNotIncludeFacility = 0;                   //!< Do not include the message facility

            static constexpr uint32 MaxMessageNum32 = 0x00000FFFUL;             //!< The largest 32-bit message number
            static constexpr uint64 MaxMessageNum64 = 0x000000001FFFFFFFULL;    //!< The largest 64-bit message number
            static constexpr uint32 MaxFacilityNum32 = 0x000007FFUL;            //!< The largest 32-bit facility number
            static constexpr uint64 MaxFacilityNum64 = 0x000000000FFFFFFFULL;   //!< The largest 64-bit facility number

            static constexpr uint32 MaxFacilityNameLen = 9;                     //!< Longest allowed facility name
            static constexpr uint32 MaxPrefixLen = 9;                           //!< Longest allowed prefix length
            static constexpr uint32 MaxIdentLen = 31;                           //!< Longest allowed identifier length
            static constexpr uint32 MaxSymbolLen = 31;                          //!< Longest allowed symbol name
            static constexpr uint32 MaxUserValue = 255;                         //!< Largest allowed user value
            static constexpr uint32 MaxFaoCount = 255;                          //!< Maximum FAO arguments in a message
            static constexpr uint32 MaxMessageTextLen = 255;                    //!< Longest message text length
            static constexpr uint32 MaxModNameLen = 31;                         //!< Longest Module Name length
            static constexpr uint32 MaxListingTitleLen = 28;                    //!< Longest Listing Title length
    };
}
