//
// Copyright (C) Jonathan D. Belanger 2024.
// All Rights Reserved.
//
// This software is furnished under a license and may be used and copied only in accordance with the terms of such
// license and with the inclusion of the above copyright notice.  This software or any other copies thereof may not be
// provided or otherwise made available to any other person.  No title to and ownership of the software is hereby
// transferred.
//
// The information in this software is subject to change without notice and should not be construed as a commitment by
// the author or co-authors.
//
// The author and any co-authors assume no responsibility for the use or reliability of this software.
//
// Description:
//
//! @file
//  This file contains the definitions for the Message programming interface.  This should be the only file that a
//  user's program should require.
//
// Revision History:
//
//  V01.000 12-May-2024 Jonathan D. Belanger
//  Initially written.
//
#pragma once
#include "MessageDataTypes.hxx"
#include "MessageVector.hxx"
#include <cstdarg>
#include <cstring>
#include <string>
#include <tuple>
#include <unordered_map>
#include <utility>
#include <vector>

//
//! @namespace Msg
//! @brief This namespace is used throughout the definitions and code for the Message Programming Interface.
//
namespace Msg
{

    //
    //! @class Message MessageClass.hxx
    //! @brief This is the main class, containing all the defintions the user will need to be able to support using
    //!        OpenVMS-like messaging.
    //
    class Message
    {
        public:

            //
            //! @struct TableEntry
            //! @brief The generated output where the information used to convert a message code into text utilizes this
            //!        structure.
            //
            struct TableEntry
            {
                public:
                    MsgCode64 msgValue;
                    int64 userValue;
                    int64 faoCount;
                    std::string symbol;
                    std::string text;
            };

            static constexpr MsgCode32 Warning = 0;
            static constexpr MsgCode32 Success = 1;
            static constexpr MsgCode32 Error = 2;
            static constexpr MsgCode32 Informational = 3;
            static constexpr MsgCode32 Fatal = 4;

#ifdef BITS64
            static constexpr bool Address64Bits = true;
#else
            static constexpr bool Address64Bits = false;
#endif

            //
            //! @fn Message(const Message&) = delete
            //! @brief Delete the ability to copy the Message class using the Message constructor.
            //
            Message(const Message&) = delete;

            //
            //! @fn Message& operator=(const Message&) = delete
            //! @brief Copy the Message class using the '=' operator.
            //! @param
            //
            Message&
            operator=(const Message&) = delete;

            //
            //! @fn static Message* getInstance()
            //! @brief This function is called to retrieve the singleton instance of this class.  If the class has not
            //!        been created, then this function will create it first.
            //! @return A pointer to the singleton Message class.
            //
            static Message*
            getInstance()
            {

                //
                // If the singleton instance has not been created, then do so now.
                //
                if (instance == nullptr)
                {
                    instance = new Message();
                }

                //
                // Return the singleton instance back to the caller.
                //
                return instance;
            }

            //
            //! @fn static bool isWarning(MsgCode32 msgCode)
            //! @brief This function is called to determine if the 32-bit status code indicates a Warning severity.
            //! @param msgCode A status code value from which to determine the severity.
            //! @return false The status code is not a warning
            //! @return false The status code is a warning
            //
            static bool
            isWarning(MsgCode32 code)
            {
                return (code & MsgCodeMask) == Warning;
            }

            //
            //! @fn static bool isWarning(MsgCode64 msgCode)
            //! @brief This function is called to determine if the 64-bit status code indicates a Warning severity.
            //! @param msgCode A status code value from which to determine the severity.
            //! @return false The status code is not a warning
            //! @return false The status code is a warning
            //
            static bool
            isWarning(MsgCode64 code)
            {
                return (code & MsgCodeMask) == Warning;
            }

            //
            //! @fn static bool isSuccess(MsgCode32 msgCode)
            //! @brief This function is called to determine if the 32-bit status code indicates a Success severity.
            //! @param msgCode A status code value from which to determine the severity.
            //! @return false The status code is not a success
            //! @return false The status code is a success
            //
            static bool
            isSuccess(MsgCode32 code)
            {
                return (code & MsgCodeMask) == Success;
            }

            //
            //! @fn static bool isSuccess(MsgCode64 msgCode)
            //! @brief This function is called to determine if the 64-bit status code indicates a Success severity.
            //! @param msgCode A status code value from which to determine the severity.
            //! @return false The status code is not a success
            //! @return false The status code is a success
            //
            static bool
            isSuccess(MsgCode64 code)
            {
                return (code & MsgCodeMask) == Success;
            }

            //
            //! @fn static bool isError(MsgCode32 msgCode)
            //! @brief This function is called to determine if the 32-bit status code indicates an Error severity.
            //! @param msgCode A status code value from which to determine the severity.
            //! @return false The status code is not an error
            //! @return false The status code is an error
            //
            static bool
            isError(MsgCode32 code)
            {
                return (code & MsgCodeMask) == Error;
            }

            //
            //! @fn static bool isError(MsgCode64 msgCode)
            //! @brief This function is called to determine if the 64-bit status code indicates an Error severity.
            //! @param msgCode A status code value from which to determine the severity.
            //! @return false The status code is not an error
            //! @return false The status code is an error
            //
            static bool
            isError(MsgCode64 code)
            {
                return (code & MsgCodeMask) == Error;
            }

            //
            //! @fn static bool isInformational(MsgCode32 msgCode)
            //! @brief This function is called to determine if the 32-bit status code indicates an Informational
            //!        severity.
            //! @param msgCode A status code value from which to determine the severity.
            //! @return false The status code is not an informational
            //! @return false The status code is an informational
            //
            static bool
            isInformational(MsgCode32 code)
            {
                return (code & MsgCodeMask) == Informational;
            }

            //
            //! @fn static bool isInformational(MsgCode64 msgCode)
            //! @brief This function is called to determine if the 64-bit status code indicates a Warning severity.
            //! @param msgCode A status code value from which to determine the severity.
            //! @return false The status code is not an informational
            //! @return false The status code is an informational
            //
            static bool
            isInformational(MsgCode64 code)
            {
                return (code & MsgCodeMask) == Informational;
            }

            //
            //! @fn static bool isFatal(MsgCode32 msgCode)
            //! @brief This function is called to determine if the 32-bit status code indicates a Fatal severity.
            //! @param msgCode A status code value from which to determine the severity.
            //! @return false The status code is not a fatal
            //! @return false The status code is a fatal
            //
            static bool
            isFatal(MsgCode32 code)
            {
                return (code & MsgCodeMask) == Fatal;
            }

            //
            //! @fn static bool isWarning(MsgCode64 msgCode)
            //! @brief This function is called to determine if the 64-bit status code indicates a Fatal severity.
            //! @param msgCode A status code value from which to determine the severity.
            //! @return false The status code is not a fatal
            //! @return false The status code is a fatal
            //
            static bool
            isFatal(MsgCode64 code)
            {
                return (code & MsgCodeMask) == Fatal;
            }

            //
            //! @fn void clear();
            //! @brief This function is called to clear the contents of the message vector.
            //! @param msgVec A reference to the Message Vector to be cleared.
            //
            void
            clear(MsgVec&);

            //
            //! @fn MsgCode32 addMessage(MsgCode32 msgCode, MsgVec* msgVec, ...);
            //! @brief This function is called to add 32-bit another status code, and any associated FAO arguments.
            //! @param msgCode A 32-bit status code value to be added to the status (message) vector.
            //! @param msgVec A pointer to the Message Vector where the status information will be stored.
            //! @param ... Individual arguments needed to fill in FAO fields in the message text.
            //! @return The status code supplied in the first parameter.
            //
            MsgCode32
            addMessage(MsgCode32, MsgVec*, ...);

            //
            //! @fn MsgCode64 addMessage(MsgCode64 msgCode, MsgVec* msgVec, ...);
            //! @brief This function is called to add 64-bit another status code, and any associated FAO arguments.
            //! @param msgCode A 64-bit status code value to be added to the status (message) vector.
            //! @param msgVec A pointer to the Message Vector where the status information will be stored.
            //! @param ... Individual arguments needed to fill in FAO fields in the message text.
            //! @return The status code supplied in the first parameter.
            //
            MsgCode64
            addMessage(MsgCode64, MsgVec*, ...);

            //
            //! @fn std::string& getMessage(MsgVec& msgVec)
            //! @brief This function is called to convert the status vector into a string.
            //! @param A reference to the status vector to be converted.
            //! @return A reference to a string containing the converted status vector information.
            //
            std::string&
            getMessage(MsgVec&);

            //
            //! @fn void putMessage(MsgVec& msgVec)
            //! @brief This function is called to convert the status vector into a string and write it out to stderr.
            //! @param A reference to the status vector to be written out.
            //
            void
            putMessage(MsgVec&);

            //
            //! @fn void registerFacility(MsgCode32 facilityNumber, std::string& facilityName, const TableEntry* tableEntries)
            //! @brief This function is called to register a facility, and its associated 32-bit status code information
            //!        into the table of facilities and status codes.  If already registered, nothing is added.
            //! @param facilityNumber A 32-bit value for the facility code for the messages.
            //! @param facilityName A reference to a string containing the text representation for the facility name.
            //! @param tableEntries A pointer to a table containing all the status code definition information for the
            //         facility.
            //
            void
            registerFacility(MsgCode32, std::string&, const TableEntry*);

            //
            //! @fn void registerFacility(MsgCode64 facilityNumber, std::string& facilityName, const TableEntry* tableEntries)
            //! @brief This function is called to register a facility, and its associated 64-bit status code information
            //!        into the table of facilities and status codes.  If already registered, nothing is added.
            //! @param facilityNumber A 64-bit value for the facility code for the messages.
            //! @param facilityName A reference to a string containing the text representation for the facility name.
            //! @param tableEntries A pointer to a table containing all the status code definition information for the
            //         facility.
            //
            void
            registerFacility(MsgCode64, std::string&, const TableEntry*);

        private:
            static constexpr uint32 MsgCodeMask = 0x00000007;                   //!< Use to mask out the severity info

            typedef std::pair<std::string, const TableEntry*> FacilityEntry;    //!< A Facility definition typedef
            typedef std::unordered_map<uint64, FacilityEntry*> MessageEntry;    //!< A Message Table definition typedef

            //
            //! @fn Message() = default
            //! @brief A private constructor
            //
            Message() = default;

            static Message* instance;                                    //!< A pointer to the singleton instance
            MessageEntry messageTables;                                  //!< Message information storage
    };
}
