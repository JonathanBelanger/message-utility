//
// Copyright (C) Jonathan D. Belanger 2024.
// All Rights Reserved.
//
// This software is furnished under a license and may be used and copied only in accordance with the terms of such
// license and with the inclusion of the above copyright notice.  This software or any other copies thereof may not be
// provided or otherwise made available to any other person.  No title to and ownership of the software is hereby
// transferred.
//
// The information in this software is subject to change without notice and should not be construed as a commitment by
// the author or co-authors.
//
// The author and any co-authors assume no responsibility for the use or reliability of this software.
//
// Description:
//
//! @file
//  This file contains the code to implement the FAO functionality associated with the message utility.  This is called
//  to scan for FAO directives in an existing string and process a string containing FAO directives and replacing the
//  directive with the information location in the message vector.
//
// Revision History:
//
//  V01.000 13-May-2024 Jonathan D. Belanger
//  Initially written.
//
#include "MessageFao.hxx"
#include <cstring>
#include <cstdio>
#include <ctime>
#include <string>

//
//! @namespace Msg
//! @brief This namespace is used throughout the definitions and code for the Message Programming Interface.
//
namespace Msg
{

    //
    // Module specific FAO directive information.
    //
    static Fao::FaoInfo faoInfo[] =
    {
        {"!AC", 1}, {"!AD", 2}, {"!AF", 2}, {"!AS", 1},
        {"!AZ", 1}, {"!OB", 1}, {"!OW", 1}, {"!OL", 1},
        {"!OQ", 1}, {"!OA", 1}, {"!OI", 1}, {"!OH", 1},
        {"!OJ", 1}, {"!XB", 1}, {"!XW", 1}, {"!XL", 1},
        {"!XQ", 1}, {"!XA", 1}, {"!XI", 1}, {"!XH", 1},
        {"!XJ", 1}, {"!ZB", 1}, {"!ZW", 1}, {"!ZL", 1},
        {"!ZQ", 1}, {"!ZA", 1}, {"!ZI", 1}, {"!ZH", 1},
        {"!ZJ", 1}, {"!UB", 1}, {"!UW", 1}, {"!UL", 1},
        {"!UQ", 1}, {"!UA", 1}, {"!UI", 1}, {"!UH", 1},
        {"!UJ", 1}, {"!SB", 1}, {"!SW", 1}, {"!SL", 1},
        {"!SQ", 1}, {"!SA", 1}, {"!SI", 1}, {"!SH", 1},
        {"!SJ", 1}, {"!/",  0}, {"!_",  0}, {"!^",  0},
        {"!!",  0}, {"!%S", 0}, {"!%T", 1}, {"!%U", 1},
        {"!%I", 1}, {"!%D", 1}, {"!n%C",0}, {"!%E", 0},
        {"!%F", 0}, {"!n<", 0}, {"!>",  0}, {"!nx", 1},
        {"!n*c",0}, {"!–",  0}, {"!+",  0}, {"",    0}
    };

    std::string&
    Fao::processFaoString(const std::string& faoStr, MsgVec& msgVec, uint32* idx, bool bits64)
    {
        static std::string retStr;
        std::string msgVecStr;
        std::size_t curPos = 0;
        std::size_t faoPos = 0;
        char buffer[BufferLen];
        FaoInfo* fao;
        uint8 uByte;
        uint16 uWord;
        uint32 uLong;
        uint64 uQuad;
        int8 sByte;
        int16 sWord;
        int32 sLong;
        int64 sQuad;
        std::time_t timeSpec;
        bool lastNumberOne = false;

        retStr.clear();
        fao = getNextFaoTypeIndex(faoStr, &faoPos);
        while (fao != nullptr)
        {
            retStr += faoStr.substr(curPos, faoPos - curPos);
            switch (fao->faoCmd[1])
            {

                //
                // Directives for Character String Substitution
                //
                case 'A':
                    switch (fao->faoCmd[2])
                    {
                        case 'C':
                            *idx = msgVec.getData(MsgVec::CountedString, *idx, &msgVecStr);
                            retStr += msgVecStr;
                            break;

                        case 'S':
                        case 'D':
                            *idx = msgVec.getData(MsgVec::LengthString, *idx, &msgVecStr);
                            retStr += msgVecStr;
                            break;

                        case 'Z':
                            *idx = msgVec.getData(MsgVec::NullTerminatedString, *idx, &msgVecStr);
                            retStr += msgVecStr;
                            break;

                        case 'F':
                            *idx = msgVec.getData(MsgVec::LengthString, *idx, &msgVecStr);
                            for (int ii = 0; ii < msgVecStr.length(); ii++)
                            {
                                if (!std::isprint(static_cast<int>(msgVecStr[ii])))
                                {
                                    msgVecStr[ii] = '.';
                                }
                            }
                            retStr += msgVecStr;
                            break;

                        default:
                            break;
                    }
                    break;

                //
                // Directives for Zero-Filled Numeric Conversion
                //
                case 'O':
                    switch (fao->faoCmd[2])
                    {
                        case 'B':
                            *idx = msgVec.getData(*idx, &sLong);
                            sByte = sLong & ByteMask;
                            lastNumberOne = sByte == 1;
                            std::sprintf(buffer, "%03hho", sByte);
                            break;

                        case 'W':
                            *idx = msgVec.getData(*idx, &sLong);
                            sWord = sLong & WordMask;
                            lastNumberOne = sWord == 1;
                            std::sprintf(buffer, "%06ho", sWord);
                            break;

                        case 'L':
                            *idx = msgVec.getData(*idx, &sLong);
                            lastNumberOne = sLong == 1;
                            std::sprintf(buffer, "%011lo", sLong);
                            break;

                        case 'A':
                            if (bits64)
                            {
                                *idx = msgVec.getData(*idx, &sQuad);
                                lastNumberOne = sQuad == 1;
                                std::sprintf(buffer, "%022llo", sQuad);
                            }
                            else
                            {
                                *idx = msgVec.getData(*idx, &sLong);
                                lastNumberOne = sLong == 1;
                                std::sprintf(buffer, "%011lo", sLong);
                            }
                            break;

                        case 'I':
                        case 'Q':
                        case 'H':
                        case 'J':
                            *idx = msgVec.getData(*idx, &sQuad);
                            lastNumberOne = sQuad == 1;
                            std::sprintf(buffer, "%022llo", sQuad);
                            break;

                        default:
                            break;
                    }
                    retStr += std::string(buffer);
                    break;

                case 'X':
                    switch (fao->faoCmd[2])
                    {
                        case 'B':
                            *idx = msgVec.getData(*idx, &sLong);
                            sByte = sLong & ByteMask;
                            lastNumberOne = sByte == 1;
                            std::sprintf(buffer, "%02hhx", sByte);
                            break;

                        case 'W':
                            *idx = msgVec.getData(*idx, &sLong);
                            sWord = sLong & WordMask;
                            lastNumberOne = sWord == 1;
                            std::sprintf(buffer, "%04hx", sWord);
                            break;

                        case 'L':
                            *idx = msgVec.getData(*idx, &sLong);
                            lastNumberOne = sLong == 1;
                            std::sprintf(buffer, "%08lx", sLong);
                            break;

                        case 'A':
                            if (bits64)
                            {
                                *idx = msgVec.getData(*idx, &sQuad);
                                lastNumberOne = sQuad == 1;
                                std::sprintf(buffer, "%016llx", sQuad);
                            }
                            else
                            {
                                *idx = msgVec.getData(*idx, &sLong);
                                lastNumberOne = sLong == 1;
                                std::sprintf(buffer, "%08lx", sLong);
                            }
                            break;

                        case 'I':
                        case 'Q':
                        case 'H':
                        case 'J':
                            *idx = msgVec.getData(*idx, &sQuad);
                            lastNumberOne = sQuad == 1;
                            std::sprintf(buffer, "%016llx", sQuad);
                            break;

                        default:
                            break;
                    }
                    retStr += std::string(buffer);
                    break;

                case 'Z':
                    switch (fao->faoCmd[2])
                    {
                        case 'B':
                            *idx = msgVec.getData(*idx, &uLong);
                            uByte = uLong & ByteMask;
                            lastNumberOne = uByte == 1;
                            std::sprintf(buffer, "%03hhu", uByte);
                            break;

                        case 'W':
                            *idx = msgVec.getData(*idx, &uLong);
                            uWord = uLong & WordMask;
                            lastNumberOne = uWord == 1;
                            std::sprintf(buffer, "%05hu", uWord);
                            break;

                        case 'L':
                            *idx = msgVec.getData(*idx, &uLong);
                            lastNumberOne = uLong == 1;
                            std::sprintf(buffer, "%020lu", uLong);
                            break;

                        case 'A':
                            if (bits64)
                            {
                                *idx = msgVec.getData(*idx, &uQuad);
                                lastNumberOne = uQuad == 1;
                                std::sprintf(buffer, "%020llu", uQuad);
                            }
                            else
                            {
                                *idx = msgVec.getData(*idx, &uLong);
                                lastNumberOne = uLong == 1;
                                std::sprintf(buffer, "%010lu", uLong);
                            }
                            break;

                        case 'I':
                        case 'Q':
                        case 'H':
                        case 'J':
                            *idx = msgVec.getData(*idx, &uQuad);
                            lastNumberOne = uQuad == 1;
                            std::sprintf(buffer, "%020llu", uQuad);
                            break;

                        default:
                            break;
                    }
                    retStr += std::string(buffer);
                    break;

                //
                // Directives for Blank-Filled Numeric Conversion
                //
                case 'U':
                    switch (fao->faoCmd[2])
                    {
                        case 'B':
                            *idx = msgVec.getData(*idx, &uLong);
                            uByte = uLong & ByteMask;
                            lastNumberOne = uByte == 1;
                            std::sprintf(buffer, "%hhu", uByte);
                            break;

                        case 'W':
                            *idx = msgVec.getData(*idx, &uLong);
                            uWord = uLong & WordMask;
                            lastNumberOne = uWord == 1;
                            std::sprintf(buffer, "%hu", uWord);
                            break;

                        case 'L':
                            *idx = msgVec.getData(*idx, &uLong);
                            lastNumberOne = uLong == 1;
                            std::sprintf(buffer, "%lu", uLong);
                            break;

                        case 'A':
                            if (bits64)
                            {
                                *idx = msgVec.getData(*idx, &uQuad);
                                lastNumberOne = uQuad == 1;
                                std::sprintf(buffer, "%llu", uQuad);
                            }
                            else
                            {
                                *idx = msgVec.getData(*idx, &uLong);
                                lastNumberOne = uLong == 1;
                                std::sprintf(buffer, "%lu", uLong);
                            }
                            break;

                        case 'I':
                        case 'Q':
                        case 'H':
                        case 'J':
                            *idx = msgVec.getData(*idx, &uQuad);
                            lastNumberOne = uQuad == 1;
                            std::sprintf(buffer, "%llu", uQuad);
                            break;

                        default:
                            break;
                    }
                    retStr += std::string(buffer);
                    break;

                case 'S':
                    switch (fao->faoCmd[2])
                    {
                        case 'B':
                            *idx = msgVec.getData(*idx, &sLong);
                            sByte = sLong & ByteMask;
                            lastNumberOne = sByte == 1;
                            std::sprintf(buffer, "%hhd", sByte);
                            break;

                        case 'W':
                            *idx = msgVec.getData(*idx, &sLong);
                            sWord = sLong & WordMask;
                            lastNumberOne = sWord == 1;
                            std::sprintf(buffer, "%hd", sWord);
                            break;

                        case 'L':
                            *idx = msgVec.getData(*idx, &sLong);
                            lastNumberOne = sLong == 1;
                            std::sprintf(buffer, "%ld", sLong);
                            break;

                        case 'A':
                            if (bits64)
                            {
                                *idx = msgVec.getData(*idx, &sQuad);
                                lastNumberOne = sQuad == 1;
                                std::sprintf(buffer, "%lld", sQuad);
                            }
                            else
                            {
                                *idx = msgVec.getData(*idx, &sLong);
                                lastNumberOne = sLong == 1;
                                std::sprintf(buffer, "%ld", sLong);
                            }
                            break;

                        case 'I':
                        case 'Q':
                        case 'H':
                        case 'J':
                            *idx = msgVec.getData(*idx, &sQuad);
                            lastNumberOne = sQuad == 1;
                            std::sprintf(buffer, "%lld", sQuad);
                            break;

                        default:
                            break;
                    }
                    retStr += std::string(buffer);
                    break;

                //
                // Directives for Output String Formatting
                //
                case '/':
                    retStr += '\n';
                    break;

                case '_':
                    retStr += '\t';
                    break;

                case '!':
                    retStr += '!';
                    break;

                case '%':
                    switch (fao->faoCmd[2])
                    {
                        case 'S':
                            if (!lastNumberOne)
                            {
                                char lastChar = retStr[retStr.length() - 1];

                                if ((lastChar >= 'A') && (lastChar <= 'Z'))
                                {
                                    retStr += 'S';
                                }
                                else if ((lastChar >= 'a') && (lastChar <= 'z'))
                                {
                                    retStr += 's';
                                }
                            }
                            break;

                        case 'T':
                            *idx = msgVec.getData(*idx, &sQuad);
                            if (uQuad == 0)
                            {
                                timeSpec = std::time(nullptr);
                            }
                            else
                            {
                                timeSpec = uQuad;
                                timeSpec = std::time(&timeSpec);
                            }
                            std::strftime(buffer, BufferLen, "%H:%M:%S", std::localtime(&timeSpec));
                            retStr += std::string(buffer);
                            break;

                        case 'D':
                            *idx = msgVec.getData(*idx, &sQuad);
                            if (uQuad == 0)
                            {
                                timeSpec = std::time(nullptr);
                            }
                            else
                            {
                                timeSpec = uQuad;
                                timeSpec = std::time(&timeSpec);
                            }
                            std::strftime(buffer, BufferLen, "%d-%b-%Y %H:%M:%S", std::localtime(&timeSpec));
                            retStr += std::string(buffer);
                            break;

                        case 'U':   // [group-number, user-number]
                            *idx = msgVec.getData(*idx, &uLong);
                            break;

                        case 'I':   // [group-name, user-name]
                            *idx = msgVec.getData(*idx, &uLong);
                            break;

                        default:
                            break;
                    }
                    break;

                default:
                    // We currently cannot handle !nx, !n%C, !%E, !%F, !n<, !>, !n*c, !--, and !+
                    break;
            }
            curPos = faoPos + fao->faoCmd.length();
            faoPos = curPos;
            fao = getNextFaoTypeIndex(faoStr, &faoPos);
        }
        if (curPos < faoStr.length())
        {
            retStr += faoStr.substr(curPos);
        }
        return retStr;
    }

    Fao::FaoInfo*
    Fao::getNextFaoTypeIndex(const std::string& text, std::size_t* offset)
    {
        if (*offset == std::string::npos)
        {
            *offset = 0;
        }
        *offset = text.find('!', *offset);
        if (*offset != std::string::npos)
        {
            for (int32 ii = 0; faoInfo[ii].faoCmd != ""; ii++)
            {
                if (text.substr(*offset, faoInfo[ii].faoCmd.length()) == faoInfo[ii].faoCmd)
                {
                    return &faoInfo[ii];
                }
            }
        }
        return nullptr;
    }
}
