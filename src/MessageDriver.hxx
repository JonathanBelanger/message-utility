//
// Copyright (C) Jonathan D. Belanger 2024.
// All Rights Reserved.
//
// This software is furnished under a license and may be used and copied only in accordance with the terms of such
// license and with the inclusion of the above copyright notice.  This software or any other copies thereof may not be
// provided or otherwise made available to any other person.  No title to and ownership of the software is hereby
// transferred.
//
// The information in this software is subject to change without notice and should not be construed as a commitment by
// the author or co-authors.
//
// The author and any co-authors assume no responsibility for the use or reliability of this software.
//
// Description:
//
//! @file
//  This file contains the definitions to process the scanned and parsed message data to be used as part of the parsing
//  and generation of the language specific definitions and code.
//
// Revision History:
//
//  V01.000 06-May-2024 Jonathan D. Belanger
//  Initially written.
//
#pragma once
#include "MessageDataTypes.hxx"
#include "MessageInternalDataTypes.hxx"
#include "MessageScannerClass.hxx"
#include "MessageParser.hxx"
#include "MessageOutput.hxx"
#include "MessageList.hxx"
#include <ctime>
#include <string>
#include <vector>

//
//! @namespace Msg
//! @brief This namespace is used throughout the definitions and code for the Message Programming Interface.
//
namespace Msg
{

    //
    //! @class Driver
    //! @brief This class contains the definitions needed to process the scanned and parsed message data.
    //
    class Driver
    {
        public:

            //
            // The Parser and Scanner classes are friends if the Driver class.  This make the code a bit clearer and
            // simpler.
            //
            friend class Parser;
            friend class Scanner;

            //
            //! @enum NumBits
            //! @brief This enumeration contains the definitions of the number of bits to be generated in the output.
            //
            enum NumBits
            {
                Bits32,
                Bits64
            };

            //
            //! @enum Qualifier
            //! @brief This class contains symbols that represent the qualifiers allowed on the message directives.
            //
            enum Qualifier
            {
                None,
                Prefix,             // has string argument
                Shared,             // has no argument
                System,             // has no argument
                FaoCount,           // has integer argument
                Identification,     // has string argument
                UserValue,          // has integer argument
                Success,            // has no argument
                Informational,      // has no argument
                Warning,            // has no argument
                Error,              // has no argument
                Fatal               // has no argument
            };

            //
            //! @struct DirectiveQualifier
            //! @brief This structure is used to store qualifier information and any argument associated with it.
            //
            struct DirectiveQualifier
            {
                public:

                    //
                    //! @fn DirectiveQualifier(Qualifier qual)
                    //! @brief This constructor is called when a qualifier does not have an argument.
                    //! @param qual An enumerative value specifying the qualifier being processed.
                    //
                    DirectiveQualifier(Qualifier qual) :
                        qualifier(qual),
                        intValue(0)
                    {}

                    //
                    //! @fn DirectiveQualifier(Qualifier qual, int64 val)
                    //! @brief This constructor is called when a qualifier an integer argument.
                    //! @param qual An enumerative value specifying the qualifier being processed.
                    //! @param val A signed 64-bit integer for the qualifier argument.
                    //! @note We assume signed 64-bit values.  They will be truncated as necessary in later processing.
                    //
                    DirectiveQualifier(Qualifier qual, int64 val) :
                        qualifier(qual),
                        intValue(val)
                    {}

                    //
                    //! @fn DirectiveQualifier(Qualifier qual)
                    //! @brief This constructor is called when a qualifier does not have an argument.
                    //! @param qual An enumerative value specifying the qualifier being processed.
                    //! @param val A reference to a string for the qualifier argument.
                    //
                    DirectiveQualifier(Qualifier qual, std::string& val) :
                        qualifier(qual),
                        intValue(0),
                        strValue(val)
                    {}

                    Qualifier qualifier;    //!< Specifies the qualifier
                    int64 intValue;         //!< The integer value specified with the qualifier, if any.  Otherwise, 0.
                    std::string strValue;   //!< The string value specified with the qualifier, if any.
            };

            //
            //! @fn Driver()
            //! @brief The constructor for the Driver class.
            //
            Driver() :
                traceParse(false),
                traceScan(false),
                listingEnabled(false),
                shared(false),
                system(false),
                bits(Bits64),
                nextMessageNumber(1),
                nextLiteralNumber(1),
                facilityNumber(0),
                severity(0),
                scanner(nullptr),
                parser(nullptr),
                _location(0)
            {}

            //
            //! @fn ~Driver()
            //! @brief The default destructor for the Driver class.
            //
            ~Driver() = default;

            //
            //! @fn Driver(const Driver& driver)
            //! @brief Delete the ability to create a copy of the Driver class through the constructor.
            //! @param driver A constant reference to an existing Driver class.
            //! @return A new instance of the driver class (disabled).
            //
            Driver(const Driver&) = delete;

            //
            //! @fn Driver& operator=(const Driver& driver)
            //! @brief Delete the ability to create a copy of the Driver class through the '=' operator.
            //! @param driver A constant reference to an existing Driver class.
            //! @return A new instance of the driver class (disabled).
            //
            Driver&
            operator=(const Driver&) = delete;

            //
            //! @fn int parse(std::string& filename)
            //! @brief This function is called to start the scanning and parsing of a message formatted file
            //! @param filename A reference to a string containing the full path to the file name to be processed.
            //! @return 0 When the processing has been successful
            //! @return -1 When the processing has not been successful
            //
            int
            parse(std::string&);

            //
            //! @fn processBase(int64 newBase)
            //! @brief This function is called when the .BASE <number> directive is being processed.
            //! @param newBase A value for the new base for the next message to be processed.
            //
            void
            processBase(int64);

            //
            //! @fn processEnd()
            //! @brief This function is called when the .END directive is being processed.
            //! @note This function will call the Output class to generate the language specific definitions and code.
            //
            void
            processEnd();

            //
            //! @fn processFacility(std::string& facnam int64 facnum)
            //! @brief This function is called when the .FACILITY <facnam>, <facnum> directive is being processed.
            //! @param facnam A reference to a string for the facility name associated with all message defined under it
            //! @param facnum A value for the facility number associated with all messages defined under this facility.
            //
            void
            processFacility(std::string&, int64);

            //
            //! @fn processIdent(std::String& identification)
            //! @brief This function is called when the .IDENT <string> directive is being processed.
            //! @param identification A reference to a string for the identification assocaited with the facility.
            //
            void
            processIdent(std::string&);

            //
            //! @fn processLiteral(std::string& symbol)
            //! @brief This function is called when the .LITERAL <symbol>[,...] directive is being processed.
            //! @param symbol A reference to a string for a symbol to get the next automatic value assigned to it.
            //
            void
            processLiteral(std::string&);

            //
            //! @fn processLiteral(std::string& symbol, int64 value)
            //! @brief This function is called when the .LITERAL <symbol>=<expression>[,...] directive is being processed.
            //! @param symbol A reference to a string for a symbol to get the specified value assigned to it.
            //! @param value A value to be assigned to the symbol (this may be a calculated value or from a symbol look-up)
            //
            bool
            processLiteral(std::string&, int64);

            //
            //! @fn processMessage(std::string& symbol, std::string& text)
            //! @brief This function is called when the <message> <text> is being processed.
            //! @param symbol A reference to a string for a message symbol to get the next automatic message value.
            //! @param text A reference to a string containing the text associated with the message.
            //
            void
            processMessage(std::string&, std::string&);

            //
            //! @fn processPage()
            //! @brief This function is called when the .PAGE directive is being processed.
            //
            void
            processPage();

            //
            //! @fn processSeverity(uint32 newSeverity)
            //! @brief This function is called when the .SEVERITY <number> directive is being processed.
            //! @param newSeverity A value for the new severity level for the next message to be processed.
            //
            void
            processSeverity(uint32);

            //
            //! @fn processTitle(std::string& modname)
            //! @brief This function is called when the .TITLE <modname> directive is being processed.
            //! @param modname A reference to a string to be assigned to the module name.
            //
            void
            processTitle(std::string&);

            //
            //! @fn processTitle(std::string& modname, std::string& listingTitle)
            //! @brief This function is called when the .TITLE <modname> <listing title> directive is being processed.
            //! @param modname A reference to a string to be assigned to the module name.
            //! @param listingTitle A reference to a string containing the title to be output on the listing file
            //
            void
            processTitle(std::string&, std::string&);

            //
            //! @fn addQualifier(Qualifier qual)
            //! @brief This function is called when a qualifier, without value, is processed for a directive.
            //! @param qual An enumerated value for indicating the qualifier being processed.
            //
            void
            addQualifier(Qualifier);

            //
            //! @fn addQualifier(Qualifier qual, std::string& value)
            //! @brief This function is called when a qualifier, with a string value, is processed for a directive.
            //! @param qual A value for indicating the qualifier being processed.
            //! @param value A reference to a string containing the value associated with the qualifier.
            //
            void
            addQualifier(Qualifier, std::string&);

            //
            //! @fn addQualifier(Qualifier qual, int64 value)
            //! @brief This function is called when a qualifier, without value, is processed for a directive.
            //! @param qual A value for indicating the qualifier being processed.
            //! @param value A reference to a signed 64-bit containing the value associated with the qualifier.
            //
            void
            addQualifier(Qualifier, int64);

            //
            //! @fn int64 getSymbolValue(std::string& symbol)
            //! @brief This function is called to return the value assigned to the indicated symbol, if found.
            //! @param symbol A reference to a string containing the symbols name to be looked up.
            //! @return 0 if the symbol was not found
            //! @return val The value assigned to the symbol at the time of the call.
            //
            int64
            getSymbolValue(std::string&);

            //
            //! @fn uint64 getSymbolCount()
            //! @brief Return the number of symbols currently defined.
            //! @return A value indicating the total number of symbols that have been defined.
            //
            uint64
            getSymbolCount();

            //
            //! @fn Symbol* getSymbol(uint64 index)
            //! @brief This function is called to return the 'index' entry from the list of defined symbols
            //! @param index A value for the index into the list of defined symbols (between 0 and getSymbolCOunt())
            //! @return nullptr When an entry beyond the end of the list is requested.
            //! @return address A pointer to the symbol definition.
            //
            Symbol*
            getSymbol(uint64);

            //
            //! @fn NumBits getBits() const
            //! @brief This function is called to return the enumerated value of either 32-bits or 64-bits.
            //! @return Bits32 When generating 32-bit output
            //! @return Bits64 When generating 64-bit output
            //
            NumBits
            getBits() const;

            //
            //! @fn bool getListing()
            //! @brief This function is called to determine if generating a listing file has been enabled or not.
            //! @return false Do not generate a listing file
            //! @return true Generate a listing file
            //
            bool
            getListing() const;

            //
            //! @fn void setBits(NumBits bits)
            //! @brief This function is called to set the number of bits, 32 or 64, for the generated output.
            //! @param bits The number of bits to be used.
            //
            void
            setBits(NumBits);

            //
            //! @fn void setLanguage(Output::Language lang)
            //! @brief This function is called to pass the programming language to be used for the generated output
            //! @param lang An enumerated value for one of the supported languages.
            //
            void
            setLanguage(Output::Language);

            //
            //! @fn void setOutputDir(std::String& outDir)
            //! @brief This function is called to pass the output directory for the generated output
            //! @param outDir A reference to a string specifying the output directory.
            //
            void
            setOutputDir(std::string&);

            //
            //! @fn void setCopyrightFile(std::String& copyFile)
            //! @brief This function is called to pass the copyright file path to be added at the top of the generated output
            //! @param copyFile A reference to a string specifying the full path to where the copyright file template is located.
            //
            void
            setCopyrightFile(std::string&);

            //
            //! @fn void setClassName(std::String& className)
            //! @brief This function is called to pass the class name to be used when generating the definition and source file.
            //! @param className A reference to a string specifying the class name to be used (when the language is C++).
            //
            void
            setClassName(std::string&);

            //
            //! @fn void setTrace(bool parseTrace, bool scanTrace)
            //! @brief This function is called to turn on/off the parse and scan tracing.
            //! @param parseTrace if true, turn on parse tracing.  Otherwise, turn it off.
            //! @param scanTrace if true, turn on scan tracing.  Otherwise, turn it off.
            //
            void
            setTrace(bool, bool);

            //
            //! @fn void setListing(std::string& listingFile, std::string& inputFile)
            //! @brief This function is called to enable listing generation and pass the listing file to the listing code.
            //! @param listingFile A reference to a string specifying the full path for the generated listing file.
            //! @param inputFile A reference to a string specifying the full path for the input file.
            //
            void
            setListing(std::string&, std::string&);

        private:

            //
            //! @fn Symbol* findSymbol(const std::string& symbol)
            //! @brief This internal function is called to return the address of a symbol table entry or a nullptr.
            //! @symbol A constant reference to a string specifying the symbol to be looked-up.
            //! @return address The address of a symbol table entry
            //! @return nullptr A null pointer because the symbol was not found
            //
            Symbol*
            findSymbol(const std::string&);

            //
            //! @fn DirectiveQualifier* findQualifier(Qualifier qual)
            //! @brief This internal function is called to return the address of a qualifier table entry or a nullptr.
            //! @symbol An enumerated value specifying the qualifier to be looked-up.
            //! @return address The address of a qualifier table entry
            //! @return nullptr A null pointer because the qualifier was not found
            //
            DirectiveQualifier*
            findQualifier(Qualifier);

            //
            //! @fn void addSymbol(const std::string& symb, const int64 val)
            //! @brief This internal function is called to add a symbol to the symbol table.
            //! @param symb A constant reference to a string for the symbol to be added.
            //! @param val A constant signed 64-bit value to be assigned to the symbol.
            //
            void
            addSymbol(const std::string&, const int64);

            //
            //! @fn void addSymbol(const std::string& symb, const int64 val, const int64 usrVa, const std::string& text, const int64 faoCnt)
            //! @brief This internal function is called to add a symbol to the symbol table.
            //! @param symb A constant reference to a string for the symbol to be added.
            //! @param val A constant signed 64-bit value to be assigned to the symbol.
            //! @param usrVal A constant signed 64-bit user value to be assigned to the symbol.
            //! @param text A constant reference to a string containing the message text to be assigned to the symbol.
            //! @param faoCnt A constant value indicating the number of FAO arguments present in the message text.
            //
            void
            addSymbol(const std::string&, const int64, const int64, const std::string&, const int64);

            //
            //! @fn int64 faoCount(const std::string& text)
            //! @brief This function is called to determine the number of FAO arguments required for the message text
            //! @param text A constant reference to a string containing the message text to be parsed looking for FAO directives
            //! @return The number of arguments required for all FAO directives.
            //
            int64
            faoCount(const std::string&);

            //
            //! @fn void updateLocation(uint32 newLine, uint32 len)
            //! @brief This function is called by the scanner as it processes the input data.
            //! @param newLine A value indicating the number new-lines in the processed input data (0 or 1)
            //! @param len A value indicating the number of bytes for the data being processed.
            //
            void
            updateLocation(uint32, uint32);

            //
            //! @fn location& getLocation()
            //! @brief This function is called to return the current location within the scanned file being processed
            //! @return location A reference to the location structure containing the file location information.
            //
            location&
            getLocation();

            bool traceParse;                                //!< Parser tracing has been requested(true) or not(false)
            bool traceScan;                                 //!< Scanner tracing has been requested(true) or not(false)
            bool listingEnabled;                            //!< A list file has(true) or has not(false) been requested
            bool shared;                                    //!< /SHARED specified on the .FACILITY directive
            bool system;                                    //!< /SYSTEM specified on the .FACILITY directive
            NumBits bits;                                   //!< The number of bits, 32 or 64, for the generated output
            int64 nextMessageNumber;                        //!< Next automatically assigned message value (.BASE updates)
            int64 nextLiteralNumber;                        //!< Next automatically assigned .LITERAL value
            int64 facilityNumber;                           //!< Facility number from the .FACILITY directive
            std::string facilityName;                       //!< Facility name from the .FACILITY directive
            std::string prefix;                             //!< Prefix qualifier from the .FACILITy directive
            std::string identification;                     //!< Identification from the .IDENT directive
            std::string moduleName;                         //!< Module name from .TITLE directive
            std::string listingText;                        //!< Listing text from .TITLE directive
            uint32 severity;                                //!< The current severity level
            Scanner* scanner;                               //!< The address of the scanner class
            Parser* parser;                                 //!< The address of the parser class
            Output output;                                  //!< The output class
            Listing listOutput;                             //!< The listing class
            std::vector<Symbol*> symbols;                   //!< The symbol table
            std::vector<DirectiveQualifier*> qualifiers;    //!< The qualifier table
            location _location;                             //!< The input file location information
    };
}
