//
// Copyright (C) Jonathan D. Belanger 2024.
// All Rights Reserved.
//
// This software is furnished under a license and may be used and copied only in accordance with the terms of such
// license and with the inclusion of the above copyright notice.  This software or any other copies thereof may not be
// provided or otherwise made available to any other person.  No title to and ownership of the software is hereby
// transferred.
//
// The information in this software is subject to change without notice and should not be construed as a commitment by
// the author or co-authors.
//
// The author and any co-authors assume no responsibility for the use or reliability of this software.
//
// Description:
//
//! @file
//  This file contains the definitions needed to manage an opaque status (message) vector.
//
// Revision History:
//
//  V01.000 13-May-2024 Jonathan D. Belanger
//  Initially written.
//
#pragma once
#include "MessageDataTypes.hxx"
#include <string>
#include <vector>

//
//! @namespace Msg
//! @brief This namespace is used throughout the definitions and code for the Message Programming Interface.
//
namespace Msg
{

    //
    //! @class MsgVec
    //! @brief This class contains the definitions to be able to handle the message vector containing either 32-bit
    //!        or 64-bit status information
    //
    class MsgVec
    {
        public:

            //
            //! @fn MsgVec()
            //! @brief This is the default constructor for the class
            //
            MsgVec() = default;

            //
            //! @fn ~MsgVec()
            //! @brief This is the default destructor for the class
            //
            ~MsgVec() = default;

            //
            //! @fn MsgVec(const MsgVec& msgVec) = delete
            //! @brief The ability to create a copy of the MsgVec class using a constructor is deleted.
            //! @param msgVec A constant reference to a message vector
            //
            MsgVec(const MsgVec&) = delete;

            //
            //! @fn MsgVec& operator=(MsgVec& statusVec)
            //! @brief The ability to create a copy of the MsgVec class using the '=' operator.
            //! @param statusVec A reference to the source Message Vector.
            //! @return A reference to the class.
            //
            MsgVec&
            operator=(MsgVec&);

            static constexpr uint16 IncludeText = 1;                //!< Include message text
            static constexpr uint16 DoNotIncludeText = 0;           //!< Do not include message text
            static constexpr uint16 IncludeIdent = 2;               //!< Include message identifier
            static constexpr uint16 DoNotIncludeIdent = 0;          //!< Do not include message identifier
            static constexpr uint16 IncludeSeverity = 4;            //!< Include message severity
            static constexpr uint16 DoNotIncludeSeverity = 0;       //!< Do not include message severity
            static constexpr uint16 IncludeFacility = 8;            //!< Include the message facility
            static constexpr uint16 DoNotIncludeFacility = 0;       //!< Do not include the message facility

            //
            //! @struct FirstEntry
            //! @brief This structure contains the fields for the first quadword of the status vector.
            //
            struct FirstEntry
            {
                public:
                    FirstEntry() :
                        argumentCount(0),
                        defMessageOpt(0),
                        signal64(0)
                    {}

                    FirstEntry&
                    operator=(const FirstEntry& fstEnt)
                    {
                        this->argumentCount = fstEnt.argumentCount;
                        this->defMessageOpt = fstEnt.defMessageOpt;
                        this->signal64 = fstEnt.signal64;

                        return *this;
                    }

                    void
                    set(uint32 first)
                    {
                        argumentCount = uint16(first & 0x0000ffff);
                        defMessageOpt = uint16((first >> 16) & 0x0000ffff);
                    }

                    void
                    set(uint16 argc, uint16 options)
                    {
                        argumentCount = argc;
                        defMessageOpt = options;
                    }

                    uint32
                    get()
                    {
                        return (uint32(argumentCount) & 0x0000ffff) + ((uint32(defMessageOpt) << 16) & 0xffff0000);
                    }

                    uint16 argumentCount;
                    uint16 defMessageOpt;
                    uint32 signal64;
            };

            //
            //! @struct MessageOptions32
            //! @brief This struct is used handle the message option after each status (message) code and before any
            //!        arguments.
            //
            struct MessageOptions32
            {

                    //
                    //! @fn MessageOptions& operator=(const MsgCode32 msgCode)
                    //! @brief This function is called to copy the 32-bit option field for other processing.
                    //! @param msgCode A 32-bit status code value to be set into the struct's fields.
                    //! @return A reference to this class.
                    //
                    MessageOptions32&
                    operator=(const MsgCode32 msgCode)
                    {
                        this->argumentCount = uint16(msgCode & 0x0000ffff);
                        this->defMessageOpt = uint16((msgCode >> 16) & 0x0000ffff);

                        return *this;
                    }

                    //
                    //! @fn void set(uint16 argc, uint16 options)
                    //! @brief This function is called to set the fields within this structure
                    //! @param argc An unsigned 16-bit value for the number of FAO arguments
                    //! @param options An unsigned 16-bit value for the message options to be set
                    //
                    void
                    set(uint16 argc, uint16 options)
                    {
                        this->argumentCount = argc;
                        this->defMessageOpt = options;
                    }

                    //
                    //! @fn uint32 get()
                    //! @brief This function is called to return the 32-buit value for the 2 16-bit fields.
                    //! @return A 32-bit unsigned value.
                    //
                    uint32
                    get()
                    {
                        return uint32(this->argumentCount) + (uint32(this->defMessageOpt) << 16);
                    }

                public:
                    uint16 argumentCount;
                    uint16 defMessageOpt;
            };

            //
            //! @struct MessageOptions64
            //! @brief This struct is used handle the message option after each status (message) code and before any
            //!        arguments.
            //
            struct MessageOptions64
            {

                    //
                    //! @fn MessageOptions& operator=(const MsgCode32 msgCode)
                    //! @brief This function is called to copy the 32-bit option field for other processing.
                    //! @param msgCode A 32-bit status code value to be set into the struct's fields.
                    //! @return A reference to this class.
                    //
                    MessageOptions64&
                    operator=(const MsgCode64 msgCode)
                    {
                        this->argumentCount = uint32(msgCode & 0x00000000ffffffff);
                        this->defMessageOpt = uint32((msgCode >> 32) & 0x000000000ffffffff);

                        return *this;
                    }

                    //
                    //! @fn void set(uint32 argc, uint32 options)
                    //! @brief This function is called to set the fields within this structure
                    //! @param argc An unsigned 16-bit value for the number of FAO arguments
                    //! @param options An unsigned 16-bit value for the message options to be set
                    //
                    void
                    set(uint32 argc, uint32 options)
                    {
                        this->argumentCount = argc;
                        this->defMessageOpt = options;
                    }

                    //
                    //! @fn uint64 get()
                    //! @brief This function is called to return the 32-buit value for the 2 16-bit fields.
                    //! @return A 32-bit unsigned value.
                    //
                    uint64
                    get()
                    {
                        return uint64(this->argumentCount) + (uint64(this->defMessageOpt) << 32);
                    }

                public:
                    uint16 argumentCount;
                    uint16 defMessageOpt;
            };

            //
            //! @enum ArgType
            //! @brief This enumeration is used to be able to pass FAO arguments into the message vector
            //
            enum ArgType
            {
                None,
                SignedLong,
                UnsignedLong,
                SignedQuad,
                UnsignedQuad,
                CountedString,
                NullTerminatedString,
                LengthString
            };

            //
            //! @struct Argument
            //! @brief This structure is used to be able to provide FAO argument data in a unified way
            //
            struct Argument
            {
                public:

                    //
                    //! @fn Argument()
                    //! @brief This constructor is used to initialize the type field.
                    //
                    Argument() :
                        type(None),
                        faoCount(0)
                    {}

                    ArgType type;
                    union
                    {
                        public:
                            int32 signedLong;
                            uint32 unsignedLong;
                            int64 signedQuad;
                            uint64 unsignedQuad;
                    } argData;
                    std::string text;
                    uint16 faoCount;
            };
            typedef std::vector<Argument*> ArgVec;

            //
            //! @fn uint64 operator[](uint32 index)
            //! @brief This function is called to extract an entry, 32-bit or 64-bit, from the message vector.
            //! @param index A value indicating the index entry to be retrieved.
            //! @return A 64-bit value from the message vector.
            //
            uint64
            operator[](uint32);

            //
            //! @fn FirstEntry& argOpt()
            //! @brief This function is called to retrieve the Message argument and option data from the message vector.
            //! @return A reference to the structure to contain the information.
            //
            FirstEntry&
            argOpt();

            //
            //! @fn void initialize(bool bits64)
            //! @brief This function is called to initialize the entire message vector.
            //! @param bits64 Is this for a 64-bit message vector.
            //
            void
            initialize(bool);

            //
            //! @fn void clear()
            //! @brief This function is called to clear out and re-initialize the status vector.
            //
            void
            clear();

            //
            //! @fn std::size_t size()
            //! @brief This function is called to return the size of the message vector
            //! @return A value indicating the number of entries (32-bit or 64-bit) in the message vector
            //
            std::size_t
            size();

            //
            //! @fn void insert(MsgCode32 msgCode, ArgVec& argVec)
            //! @brief This function is called to insert a status code and FAO arguments into the message vector.
            //! @param msgCode A 32-bit status code value to be inserted to the message vector.
            //! @param argVec A reference to an argument vector containing zero or more FAO arguments to be inserted.
            //
            void
            insert(MsgCode32, ArgVec&);

            //
            //! @fn void insert(MsgCode64 msgCode, ArgVec& argVec)
            //! @brief This function is called to insert a status code and FAO arguments into the message vector.
            //! @param msgCode A 64-bit status code value to be inserted to the message vector.
            //! @param argVec A reference to an argument vector containing zero or more FAO arguments to be inserted.
            //
            void
            insert(MsgCode64, ArgVec&);

            //
            //! @fn uint32 getData(uint32 index, int32* retVal)
            //! @brief This function is called to get a signed 32-bit value out of the message vector
            //! @param index A 32-bit index into the message vector
            //! @param retVal A pointer to a location to receive a signed 32-bit value.
            //! @return The updated index into the message vector for the next entry.
            //
            uint32
            getData(uint32, int32*);

            //
            //! @fn uint32 getData(uint32 index, uint32* retVal)
            //! @brief This function is called to get an unsigned 32-bit value out of the message vector
            //! @param index A 32-bit index into the message vector
            //! @param retVal A pointer to a location to receive an unsigned 32-bit value.
            //! @return The updated index into the message vector for the next entry.
            //
            uint32
            getData(uint32, uint32*);

            //
            //! @fn uint32 getData(uint32 index, int64* retVal)
            //! @brief This function is called to get a signed 64-bit value out of the message vector
            //! @param index A 32-bit index into the message vector
            //! @param retVal A pointer to a location to receive a signed 64-bit value.
            //! @return The updated index into the message vector for the next entry.
            //
            uint32
            getData(uint32, int64*);

            //
            //! @fn uint32 getData(uint32 index, uint64* retVal)
            //! @brief This function is called to get an unsigned 64-bit value out of the message vector
            //! @param index A 32-bit index into the message vector
            //! @param retVal A pointer to a location to receive an unsigned 64-bit value.
            //! @return The updated index into the message vector for the next entry.
            //
            uint32
            getData(uint32, uint64*);

            //
            //! @fn uint32 getData(ArgType stringType, uint32 index, std::string* retStr)
            //! @brief This function is called to get a string out of the message vector
            //! @param index A 32-bit index into the message vector
            //! @param retStr A pointer to a location to receive the string.
            //! @return The updated index into the message vector for the next entry.
            //
            uint32
            getData(ArgType, uint32, std::string*);

            //
            //! @fn bool endOfmessages(uint32 index)
            //! @brief This function is called to return an indicator that the index represents the end of the message
            //!        vector
            //! @param index A value representing an index into the 32-/64-bit message vector
            //! @return false The index is not at or beyond the end of the message vector
            //! @return true The index is at or beyond the end of the message vector
            //
            bool
            endOfMessages(uint32);

            //
            //! @fn bool is64Bits()
            //! @brief This function is called to return an indicator that the message vector is 32-bit or 64-bit.
            //! @return false The message vector is 32-bit
            //! @return true The message vector is 64-bit.
            //
            bool
            is64Bits();

            friend std::ostream&
            operator<<(std::ostream&, MsgVec&);

            bool
            operator==(std::vector<uint32>&);

        private:
            static constexpr int64 LowLongwordMask = 0x00000000ffffffff;
            static constexpr uint32 HighLongwordShift = 32;
            static constexpr int32 LowWordMask = 0x0000ffff;
            static constexpr uint32 HighWordShift = 16;
            static constexpr MsgCode32 SS_SIGNAL64 = 0x0000fff1;    //!< My version of this value.
            static constexpr uint32 MessageCodeSize32 = 1;          //!< Number of longwords in a 32-bit value
            static constexpr uint32 MessageCodeSize64 = 2;          //!< number of longwords in a 64-bit value
            static constexpr uint32 MessageOptionsSize = 1;         //!< number of longwords in the message options
            static constexpr int32 BytesPerChar = 1;
            static constexpr int32 BytesPerShort = 2;
            static constexpr int32 BytesPerLong = 4;
            static constexpr int32 BytesPerQuad = 8;
            static constexpr uint32 MaxCountedStringLen = 0x000000ff;

            //
            //! @fn uint32 getRawIndex(uint32 index)
            //! @brief This function is called to return the index into the 32-bit vector from the 32-/64-bit index.
            //! @index An index value to be converted
            //! @return A value for the index into the std::vector<uint32> message vector.
            //
            uint32
            getRawIndex(uint32);

            //
            //! @fn uint32 cvtRawIndex(uint32 index)
            //! @brief This function is called to return the index into the 32-/64-bit vector from the 32- index.
            //! @index An index value to be converted
            //! @return A value for the index into the 32-/64-bit message vector.
            //
            uint32
            cvtRawIndex(uint32);

            //
            //! @fn uint32 getArgumentCount()
            //! @brief This private function is used to return the number of longwords in the message vector.
            //! @return A value indicating the number of message vector longwords.
            //
            uint32
            getArgumentCount();

            //
            //! @fn void updateArgumentCount()
            //! @brief This private function is called to update the number of longwords in the message vector.
            //
            void
            updateArgumentCount();

            //
            //! @fn void pushCountedStr(std::string& text, bool bits64)
            //! @brief This private function is called to push a counted string onto the status vector stack.
            //! @param text A reference to the string to be pushed onto to the stack as a counted string (byte 0 = length)
            //! @param bits64 A boolean to indicate if the stack need to be quadword or longword aligned.
            //
            void
            pushCountedStr(std::string&, bool);

            //
            //! @fn void pushNullStr(std::string& text, bool bits64)
            //! @brief This private function is called to push a null-terminated string onto the status vector stack.
            //! @param text A reference to the string to be pushed onto to the stack as a null terminated string
            //! @param bits64 A boolean to indicate if the stack need to be quadword or longword aligned.
            //
            void
            pushNullStr(std::string&, bool);

            //
            //! @fn void pushLengthStr(std::string& text, bool bits64)
            //! @brief This private function is called to push a length and then the string onto the status vector stack.
            //! @param text A reference to the string to be pushed onto to the stack as length and string fields.
            //! @param bits64 A boolean to indicate if the stack need to be quadword or longword aligned.
            //
            void
            pushLengthStr(std::string&, bool);

            std::vector<uint32> msgVec;     //!< This is the message vector (the reason we are here)
    };
}
