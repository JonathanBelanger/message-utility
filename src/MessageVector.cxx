//
// Copyright (C) Jonathan D. Belanger 2024.
// All Rights Reserved.
//
// This software is furnished under a license and may be used and copied only in accordance with the terms of such
// license and with the inclusion of the above copyright notice.  This software or any other copies thereof may not be
// provided or otherwise made available to any other person.  No title to and ownership of the software is hereby
// transferred.
//
// The information in this software is subject to change without notice and should not be construed as a commitment by
// the author or co-authors.
//
// The author and any co-authors assume no responsibility for the use or reliability of this software.
//
// Description:
//
//! @file
//  This file contains the code needed to manage a status (message) vector.
//
// Revision History:
//
//  V01.000 13-May-2024 Jonathan D. Belanger
//  Initially written.
//
#include "MessageVector.hxx"
#include <iostream>
#include <iomanip>

//
//! @namespace Msg
//! @brief This namespace is used throughout the definitions and code for the Message Programming Interface.
//
namespace Msg
{
    uint64
    MsgVec::operator[](uint32 index32)
    {
        bool bits64 = is64Bits();

        if (bits64)
        {
            int32 index64 = index32 * 2;
            if (index64 < msgVec.size() - 2)
            {
                return (msgVec[index64 + 1] << 32) + msgVec[index64];
            }
        }
        else
        {
            if (index32 < (msgVec.size() - 1))
            {
                return msgVec[index32];
            }
        }
        return 0;
    }

    MsgVec&
    MsgVec::operator=(MsgVec& statusVec)
    {
        msgVec.clear();
        for(int32 ii = 0; ii < statusVec.msgVec.size(); ii++)
        {
            msgVec.push_back(statusVec.msgVec[ii]);
        }
        return *this;
    }

    MsgVec::FirstEntry&
    MsgVec::argOpt()
    {
        static MsgVec::FirstEntry retVal;

        if (is64Bits())
        {
            retVal.argumentCount = msgVec[0] & LowWordMask;
            retVal.defMessageOpt = (msgVec[0] >> HighWordShift) & LowWordMask;
            retVal.signal64 = msgVec[1];
        }
        else if (msgVec.size() == 1)
        {
            retVal.argumentCount = msgVec[0] & LowWordMask;
            retVal.defMessageOpt = (msgVec[0] >> HighWordShift) & LowWordMask;
            retVal.signal64 = 0;
        }
        return retVal;
    }

    void
    MsgVec::initialize(bool bits64)
    {
        msgVec.clear();
        msgVec.push_back(0);
        if (bits64)
        {
            msgVec.push_back(SS_SIGNAL64);
        }
    }

    void
    MsgVec::clear()
    {
        bool bits64 = is64Bits();

        msgVec.clear();
        initialize(bits64);
    }

    std::size_t
    MsgVec::size()
    {
        if (msgVec.size() > 0)
        {
            if (is64Bits())
            {
                return (msgVec.size() / 2) - 1;
            }
            return msgVec.size() - 1;
        }
        return 0;
    }

    void
    MsgVec::insert(MsgCode32 msgCode, ArgVec& args)
    {
        MessageOptions32 msgOpt;
        uint16 argCount = 0;

        for (int32 ii = 0; ii < args.size(); ii++)
        {
            argCount += args[ii]->faoCount;
        }
        msgOpt.set(argCount, 0);
        msgVec.push_back(msgCode);
        msgVec.push_back(msgOpt.get());

        for (int32 ii = 0; ii < args.size(); ii++)
        {
            Argument* arg = args[ii];

            switch (arg->type)
            {
                case SignedLong:
                    msgVec.push_back(arg->argData.signedLong);
                    break;

                case UnsignedLong:
                    msgVec.push_back(arg->argData.unsignedLong);
                    break;

                case SignedQuad:
                    msgVec.push_back(uint32(arg->argData.signedQuad & LowLongwordMask));
                    msgVec.push_back(uint32((arg->argData.signedQuad >> HighLongwordShift) & LowLongwordMask));
                    break;

                case UnsignedQuad:
                    msgVec.push_back(uint32(arg->argData.unsignedQuad & LowLongwordMask));
                    msgVec.push_back(uint32((arg->argData.unsignedQuad >> HighLongwordShift) & LowLongwordMask));
                    break;

                case CountedString:
                    pushCountedStr(arg->text, false);
                    break;

                case NullTerminatedString:
                    pushNullStr(arg->text, false);
                    break;

                case LengthString:
                    pushLengthStr(arg->text, false);
                    break;

                default:
                    break;
            }
        }
        updateArgumentCount();
    }

    void
    MsgVec::insert(MsgCode64 msgCode, ArgVec& args)
    {
        MessageOptions64 msgOpt;
        uint64 options;
        uint32 argCount = 0;

        for (int32 ii = 0; ii < args.size(); ii++)
        {
            argCount += args[ii]->faoCount;
        }
        msgOpt.set(argCount, 0);
        msgVec.push_back(uint32(msgCode & LowLongwordMask));
        msgVec.push_back(uint32((msgCode >> HighLongwordShift) & LowLongwordMask));
        options = msgOpt.get();
        msgVec.push_back(options & LowLongwordMask);
        msgVec.push_back((options >> HighLongwordShift) & LowLongwordMask);

        for (int32 ii = 0; ii < args.size(); ii++)
        {
            Argument* arg = args[ii];

            switch (arg->type)
            {
                case SignedLong:
                    msgVec.push_back(arg->argData.signedLong);
                    msgVec.push_back(0);
                    break;

                case UnsignedLong:
                    msgVec.push_back(arg->argData.unsignedLong);
                    msgVec.push_back(0);
                    break;

                case SignedQuad:
                    msgVec.push_back(uint32(arg->argData.signedQuad & LowLongwordMask));
                    msgVec.push_back(uint32((arg->argData.signedQuad >> HighLongwordShift) & LowLongwordMask));
                    break;

                case UnsignedQuad:
                    msgVec.push_back(uint32(arg->argData.unsignedQuad & LowLongwordMask));
                    msgVec.push_back(uint32((arg->argData.unsignedQuad >> HighLongwordShift) & LowLongwordMask));
                    break;

                case CountedString:
                    pushCountedStr(arg->text, true);
                    break;

                case NullTerminatedString:
                    pushNullStr(arg->text, true);
                    break;

                case LengthString:
                    pushLengthStr(arg->text, true);
                    break;

                default:
                    break;
            }
        }
        updateArgumentCount();
    }

    uint32
    MsgVec::getData(uint32 index, int32* retVal)
    {
        uint32 rawIdx = getRawIndex(index);

        *retVal = static_cast<int32>(msgVec[rawIdx]);
        rawIdx += 1;
        return cvtRawIndex(rawIdx);
    }

    uint32
    MsgVec::getData(uint32 index, uint32* retVal)
    {
        uint32 rawIdx = getRawIndex(index);

        *retVal = static_cast<uint32>(msgVec[rawIdx]);
        rawIdx += 1;
        return cvtRawIndex(rawIdx);
    }

    uint32
    MsgVec::getData(uint32 index, int64* retVal)
    {
        uint32 rawIdx = getRawIndex(index);

        *retVal = uint64(msgVec[rawIdx]) + (uint64(msgVec[rawIdx + 1]) << HighLongwordShift);
        rawIdx += 2;
        return cvtRawIndex(rawIdx);
    }

    uint32
    MsgVec::getData(uint32 index, uint64* retVal)
    {
        uint32 rawIdx = getRawIndex(index);

        *retVal = uint64(msgVec[rawIdx]) + (uint64(msgVec[rawIdx + 1]) << HighLongwordShift);
        rawIdx += 2;
        return cvtRawIndex(rawIdx);
    }

    uint32
    MsgVec::getData(ArgType stringType, uint32 index, std::string* retStr)
    {
        int idx = 0;
        uint32 charCount = 0;
        uint32 offset = 0;
        uint32 length;
        char longChar[BytesPerLong];
        uint32 rawIdx = getRawIndex(index);

        retStr->clear();
        switch (stringType)
        {
            case CountedString:
                *reinterpret_cast<uint32*>(longChar) = msgVec[rawIdx];
                offset++;
                length = longChar[idx++];
                while (charCount < length)
                {
                    while ((idx < BytesPerLong) && (charCount < length))
                    {
                        *retStr += longChar[idx++];
                        charCount++;
                    }
                    if (charCount < length)
                    {
                        idx = 0;
                        *reinterpret_cast<uint32*>(longChar) = msgVec[rawIdx + offset];
                        offset++;
                    }
                }
                break;

            case NullTerminatedString:
                *reinterpret_cast<uint32*>(longChar) = msgVec[rawIdx];
                offset++;
                while (longChar[idx] != '\0')
                {
                    *retStr += longChar[idx++];
                    if ((idx % BytesPerLong) == 0)
                    {
                        idx = 0;
                        *reinterpret_cast<uint32*>(longChar) = msgVec[rawIdx + offset];
                        offset++;
                    }
                }
                break;

            case LengthString:
                length = msgVec[rawIdx];
                offset++;
                if (length > 0)
                {
                    *reinterpret_cast<uint32*>(longChar) = msgVec[rawIdx + offset];
                    offset++;
                    while (charCount < length)
                    {
                        while ((idx < BytesPerLong) && (charCount < length))
                        {
                            *retStr += longChar[idx++];
                            charCount++;
                        }
                        if (charCount < length)
                        {
                            *reinterpret_cast<uint32*>(longChar) = msgVec[rawIdx + offset];
                            offset++;
                            idx = 0;
                        }
                    }
                }
                break;

            default:
                break;
        }
        rawIdx += offset;
        return cvtRawIndex(rawIdx);
    }

    uint32
    MsgVec::getRawIndex(uint32 index)
    {
        if (is64Bits())
        {
            return index * 2;
        }
        return index;
    }

    uint32
    MsgVec::cvtRawIndex(uint32 index)
    {
        if (is64Bits())
        {
            return (index + ((index % 2) == 1 ? 1 : 0)) / 2;
        }
        return index;
    }

    bool
    MsgVec::endOfMessages(uint32 index)
    {
        return !(index <= getArgumentCount());
    }

    bool
    MsgVec::is64Bits()
    {
        if (msgVec.size() >= 2)
        {
            return msgVec[1] == SS_SIGNAL64;
        }
        return false;
    }

    std::ostream&
    operator<<(std::ostream& outStream, MsgVec& statusVec)
    {
        outStream << "Message Vector:" << std::endl;
        outStream << "    64-bits:      " << (statusVec.is64Bits() ? "True" : "False") << std::endl;
        outStream << "    actual size:  " << statusVec.msgVec.size() << std::endl;
        outStream << "    logical size: " << statusVec.size();
        for (int ii = 0; ii < statusVec.msgVec.size(); ii++)
        {
            outStream << std::endl << "    [" << std::setw(3) << ii << "]: " << std::hex << std::setw(8)
                << std::setfill('0') << statusVec.msgVec[ii] << std::setfill(' ') << std::dec;
        }
        return outStream;
    }

    bool
    MsgVec::operator==(std::vector<uint32>& rhs)
    {
        bool result = this->msgVec.size() == rhs.size();

        for (int ii = 0; ((ii < this->msgVec.size()) && result); ii++)
        {
            if (rhs[ii] != 0xdeadbeef)
            {
                result = this->msgVec[ii] == rhs[ii];
            }
        }
        return result;
    }

    uint32
    MsgVec::getArgumentCount()
    {
        if (msgVec.size() > 0)
        {
            FirstEntry msgOpt;

            msgOpt.set(msgVec[0]);
            return msgOpt.argumentCount;
        }
        return 0;
    }

    void
    MsgVec::updateArgumentCount()
    {
        if (msgVec.size() > 0)
        {
            FirstEntry msgOpt;

            msgOpt.set(uint16(size()), 0);
            msgVec[0] = msgOpt.get();
        }
    }
    void
    MsgVec::pushCountedStr(std::string& text, bool bits64)
    {
        uint8 len = ((text.length() > MaxCountedStringLen) ? MaxCountedStringLen : text.length());
        uint8 longChar[BytesPerLong] = {len, 0, 0, 0};  // Set the first byte to the length

        for (uint8 ii = 1; ii <= len; ii++)
        {
            longChar[ii % BytesPerLong] = text[ii - 1];

            // If we filled the longChar array, push it onto the stack and clear it out.
            if ((ii % BytesPerLong) == 3)
            {
                uint32 longVal = longChar[0] + (longChar[1] << 8) + (longChar[2] << 16) + (longChar[3] << 24);

                msgVec.push_back(longVal);
                std::fill_n(longChar, BytesPerLong, 0);
            }
        }

        // If we did not write out the last long word, taking into account the count byte, then do so now.
        if (((len + 1) % BytesPerLong) != 0)
        {
            uint32 longVal = longChar[0] + (longChar[1] << 8) + (longChar[2] << 16) + (longChar[3] << 24);

            msgVec.push_back(longVal);
        }

        // If we need to quadword align the stack, do so before returning to the caller.
        if (bits64 && (msgVec.size() % 2) == 1)
        {
            msgVec.push_back(0);
        }
    }

    void
    MsgVec::pushNullStr(std::string& text, bool bits64)
    {
        uint8 longChar[BytesPerLong] = {0, 0, 0, 0};

        for (uint32 ii = 0; ii <= text.length(); ii++)
        {
            longChar[ii % BytesPerLong] = (ii == text.length() ? 0 : text[ii]);

            // If we filled the longChar array, push it onto the stack and clear it out.
            if ((ii % BytesPerLong) == 3)
            {
                uint32 longVal = longChar[0] + (longChar[1] << 8) + (longChar[2] << 16) + (longChar[3] << 24);

                msgVec.push_back(longVal);
                std::fill_n(longChar, BytesPerLong, 0);
            }
        }

        // If we did not write out the last long word, taking into account the null character, then do so now.
        if (((text.length() + 1) % BytesPerLong) != 0)
        {
            uint32 longVal = longChar[0] + (longChar[1] << 8) + (longChar[2] << 16) + (longChar[3] << 24);

            msgVec.push_back(longVal);
        }

        // If we need to quadword align the stack, do so before returning to the caller.
        if (bits64 && (msgVec.size() % 2) == 1)
        {
            msgVec.push_back(0);
        }
    }

    void
    MsgVec::pushLengthStr(std::string& text, bool bits64)
    {
        uint8 longChar[BytesPerLong] = {0, 0, 0, 0};
        uint32 idx = 0;

        msgVec.push_back(static_cast<uint32>(text.length()));
        for (uint32 ii = 0; ii < text.length(); ii++)
        {
            longChar[idx++] = text[ii];

            // If we filled the longChar array, push it onto the stack and clear it out.
            if (idx == BytesPerLong)
            {
                uint32 longVal = longChar[0] + (longChar[1] << 8) + (longChar[2] << 16) + (longChar[3] << 24);

                msgVec.push_back(longVal);
                std::fill_n(longChar, BytesPerLong, 0);
                idx = 0;
            }
        }

        // If we did not write out the last long word, then do so now.
        if (idx != 0)
        {
            uint32 longVal = longChar[0] + (longChar[1] << 8) + (longChar[2] << 16) + (longChar[3] << 24);

            msgVec.push_back(longVal);
        }

        // If we need to quadword align the stack, do so before returning to the caller.
        if (bits64 && (msgVec.size() % 2) == 1)
        {
            msgVec.push_back(0);
        }
    }
}
