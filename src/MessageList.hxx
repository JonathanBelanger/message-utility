//
// Copyright (C) Jonathan D. Belanger 2024.
// All Rights Reserved.
//
// This software is furnished under a license and may be used and copied only in accordance with the terms of such
// license and with the inclusion of the above copyright notice.  This software or any other copies thereof may not be
// provided or otherwise made available to any other person.  No title to and ownership of the software is hereby
// transferred.
//
// The information in this software is subject to change without notice and should not be construed as a commitment by
// the author or co-authors.
//
// The author and any co-authors assume no responsibility for the use or reliability of this software.
//
// Description:
//
//! @file
//  This file contains definitions used to generate a listing file generated directly from the message definition file.
//
// Revision History:
//
//  V01.000 12-May-2024 Jonathan D. Belanger
//  Initially written.
//
#pragma once
#include "MessageDataTypes.hxx"
#include "MessageParser.hxx"
#include <ctime>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

//
//! @namespace Msg
//! @brief This namespace is used throughout the definitions and code for the Message Programming Interface.
//
namespace Msg
{

    //
    //! @class Listing
    //! @brief This class contains the functions used to generate a listing file.
    //
    class Listing
    {
        public:

            //
            //! @fn Listing()
            //! @brief This constructor contains the initialization code for this class.
            //
            Listing() :
                outputStream(nullptr),
                pageNumber(1),
                pageLine(1),
                columnNumber(0),
                listingLine(1)
            {}

            //
            //! @fn ~Listing()
            //! @brief This is the default destructor for this class.
            //
            ~Listing() = default;

            //
            //! @fn void open()
            //! @brief This function is called to open the listing file and writer the header information at the top of
            //!        the first page.
            //
            void
            open();

            //
            //! @fn void writeData(std::string& text)
            //! @brief This function is called to write the specified text to the listing file.
            //! @param text A reference to one or more characters to write to the listing file.
            //
            void
            writeData(std::string&);

            //
            //! @fn void writeError(std::string& text, const Parser::location_type& loc)
            //! @brief This function is called to write the specified error text to the listing file.
            //! @param text A reference to string containing an error message to write to the listing file.
            //! @param loc A reference to the location information maintained by the parser.
            //
            void
            writeError(std::string&, const Parser::location_type&);

            //
            //! @fn void close()
            //! @brief This function is called to close the listing file.
            //
            void
            close();

            //
            //! @fn void setListingFilename(std::string& filename, std::string& inputFile)
            //! @brief This function is called to supply the filename for the listing file to be written.
            //! @param filename A reference to a string containing the full path for the listing file.
            //! @param inputFile A reference to a string containing the full path for the input file.
            //
            void
            setListingFilename(std::string&, std::string&);

        private:

            struct ErrorInfo
            {
                std::string text;
                int32 startLine;
                int32 endLine;
                int32 startColumn;
                int32 endColumn;
            };

            static constexpr uint32 HeaderLeadingSpaces = 58;
            static constexpr uint32 HeaderTrailingSpaces = 52;
            static constexpr uint32 BufferWidth = 80;
            static constexpr uint32 PageNumberLocation = 122;
            static constexpr uint32 LineNumberWidth = 6;
            static constexpr uint32 LineNumberFieldWidth = 8;
            static constexpr uint32 PageLength = 66;
            static constexpr uint32 PageWidth = 132;
            static constexpr uint32 PageNumberWidth = 4;
            static constexpr uint32 ErrorMessageLines = 2;

            //
            //! @fn void initializeHeader()
            //! @brief This internal function is called to initialize the header information to the top of each page of
            //!        a listing file
            //
            void
            initializeHeader();

            //
            //! @fn void writeMessages()
            //! @brief This internal function is called to display any buffer error messages.
            //
            void
            writeMessages();

            //
            //! @fn void endPage()
            //! @brief This internal function is called to end one page and put the header information on the next.
            //
            void
            endPage();

            //
            //! @fn std::vector<std::string> split(const std::string& text, const char splitChar)
            //! @brief this function is called to split a string into parts based on the supplied character.
            //! @param text A constant string by reference containing the string to be split
            //! @param splitChar A character to be used to split the string.
            //! @return A vector containing the split strings.
            //
            std::vector<std::string>
            split(const std::string&, const char);

            std::ofstream outputStream;     //!< The output stream for the listing file
            std::string listFile;           //!< A field for the file name for the listing file
            std::string inputFile;          //!< A field to hold the input file name
            std::string listingHeader1;     //!< A location to store the header at the top of each page
            std::string listingHeader2;     //!< A location to store the header at the top of each page
            std::vector<ErrorInfo> messages;    //!< A location to hold any error message the current input line generates.
            uint32 pageNumber;              //!< A value indicating the current page number
            uint32 pageLine;                //!< A value indicating the current line number within the listing page page
            uint32 columnNumber;            //!< A value indicating the column number for the next character
            uint32 listingLine;             //!< A value indicating the input line number
            struct tm runTime;              //!< A pointer to a value indicating when the processing has started
            struct tm modifyTime;           //!< A pointer to a value indicating the modification time of the input file.
    };
}
