//
// Copyright (C) Jonathan D. Belanger 2024.
// All Rights Reserved.
//
// This software is furnished under a license and may be used and copied only in accordance with the terms of such
// license and with the inclusion of the above copyright notice.  This software or any other copies thereof may not be
// provided or otherwise made available to any other person.  No title to and ownership of the software is hereby
// transferred.
//
// The information in this software is subject to change without notice and should not be construed as a commitment by
// the author or co-authors.
//
// The author and any co-authors assume no responsibility for the use or reliability of this software.
//
// Description:
//
//! @file
//  This file contains the definitions needed to generate language specific code and definitions files.
//
//  TestMessage3.hxx and TestMessage3.cxx are generated as the result of running the message compiler on
//  TestMessage3.msg, indicating a language of c++
//
//  TestMessage3.hxx
//  ----------------
//
//      #include "MessageClass.hxx"
//
//      namespace Msg
//      {
//          class SAMPLE
//          {
//              public:
//                  SAMPLE() = delete;
//                  ~SAMPLE() = delete;
//                  SAMPLE(const SAMPLE&) = delete;
//                  SAMPLE operator=(const SAMPLE&) = delete;
//
//                  static constexpr MsgCode64 SAMPLE_FACILITY = 0x0000000000000001;
//                  static constexpr MsgCode64 MSG$_FIRST = 0x000000010000000a;
//                  static constexpr MsgCode64 MSG$_LAST = 0x0000000100000012;
//                  static constexpr MsgCode64 MSG$_LASTMSG = 0x0000000100000012;
//                  static constexpr MsgCode64 MSG$_NUMSG = 0x0000000000000002;
//                  static const msgTable[];
//
//                  static Message*
//                  register();
//          }
//      }
//
//  TestMessage3.cxx
//  ----------------
//
//      #include "TestMessage3.hxx"
//
//      namespace Msg
//      {
//          TableEntry SAMPLE::msgTable[] =
//          {
//              {MSG$_FIRST, 0, 0, "FIRST", "first error"},
//              {MSG$_LAST, 0, 0, "LAST", "last error"},
//              {0, 0, 0, "", ""}
//          };
//
//          Message*
//          SAMPLE::register()
//          {
//              Message* msg = Message::getInstance();
//
//              msg->registerFacility(SAMPLE_FACILITY, "SAMPLE", SAMPLE::msgTable);
//              return msg;
//          }
//      }
//
// Revision History:
//
//  V01.000 09-May-2024 Jonathan D. Belanger
//  Initially written.
//
#pragma once
#include "MessageDriver.hxx"
#include <string>

//
//! @namespace Msg
//! @brief This namespace is used throughout the definitions and code for the Message Programming Interface.
//
namespace Msg
{

    //
    //! @class Output
    //! @brief This class contains the functions used to generate language specific output files.
    //
    class Output
    {
        public:

            //
            //! @enum Language
            //! @brief This enumeration contains the definitions for all supported languages
            //
            enum Language
            {
                None,
                cxx
            };

            //
            //! @fn void generateOutput(Driver* driver)
            //! @brief This function is called by the driver to generate the output for the just parsed message
            //!        formatted file and the .END directive was just processed.
            //! @param driver A pointer to the Driver class so the output functions can get the data they need
            //! @note This is language independent and calls the language dependent function.
            //
            void
            generateOutput(Driver*);

            //
            //! @fn void setLanguage(Language lang)
            //! @brief This function is called by the driver to set the language to be generated.
            //! @param lang An enumerated value indicating the language to be processed.
            //
            void
            setLanguage(Language);

            //
            //! @fn void setOutputDir(std::string& outputDir)
            //! @brief This function is called to set the directory where the output is to be written.
            //! @param outputDir A reference to a string containing the output directory path.
            //! @note The default is the current directory.
            //
            void
            setOutputDir(std::string&);

            //
            //! @fn void setOutputBase(std::string& filename)
            //! @brief This function is called to set the directory where the output is to be written.
            //! @param filename A reference to a string containing the base name for the output file, sans extension.
            //! @note The default is the filename for the input message file.
            //
            void
            setOutputBase(std::string&);

            //
            //! @fn setCopyrightFile(std::string& copyrightFile)
            //! @brief This function is called to set the full path to the file containing the copyright information.
            //! @param copyrightFile A reference to a string containing the full path name for the source copyright file
            //! @note The default is no copyright information is inserted at the top of the output files.
            //
            void
            setCopyrightFile(std::string&);

            //
            //! @fn setClassName(std::string& _className)
            //! @brief This function is called to set the name to be associated with the class.
            //! @param _className A reference to a string containing the name for the class
            //! @note The default is to use the facility as the name for the class.
            //
            void
            setClassName(std::string&);

        private:
            static constexpr int YearLoc = 20;
            static constexpr int YearLen = 4;
            static constexpr int MonthLoc = 4;
            static constexpr int MonthLen = 3;
            static constexpr int DateLoc = 8;
            static constexpr int DataLen = 2;
            static constexpr int hexWidth32 = 8;
            static constexpr int hexWidth64 = 16;

            //
            //! @fn void generateCxxOutput(Driver* driver)
            //! @brief This function is called by the driver to generate the output for the C++ language.
            //! @param driver A pointer to the Driver class so the output functions can get the data they need
            //
            void
            generateCxxOutput(Driver*);

            Language language;              //!< A field to indicate the language to be generated
            std::string outputFileDir;      //!< A field to contain the full path to the output directory
            std::string outputFileBase;     //!< A field to contain the output filename to be used to write the output
            std::string copyrightFile;      //!< A field to contain the full path to the copyright file to include
            std::string className;          //!< A field to contain the class name to used when the language is C++
    };
}
