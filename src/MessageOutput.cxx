//
// Copyright (C) Jonathan D. Belanger 2024.
// All Rights Reserved.
//
// This software is furnished under a license and may be used and copied only in accordance with the terms of such
// license and with the inclusion of the above copyright notice.  This software or any other copies thereof may not be
// provided or otherwise made available to any other person.  No title to and ownership of the software is hereby
// transferred.
//
// The information in this software is subject to change without notice and should not be construed as a commitment by
// the author or co-authors.
//
// The author and any co-authors assume no responsibility for the use or reliability of this software.
//
// Description:
//
//! @file
//  This file contains the code needed to generate the language specific definition and code files from the processed
//  message definition file data.
//
// Revision History:
//
//  V01.000 11-May-2024 Jonathan D. Belanger
//  Initially written.
//
#include "MessageInternalDataTypes.hxx"
#include "MessageDriver.hxx"
#include <ctime>
#include <filesystem>
#include <iostream>
#include <iomanip>

//
//! @namespace Msg
//! @brief This namespace is used throughout the definitions and code for the Message Programming Interface.
//
namespace Msg
{
    void
    Output::generateOutput(Driver* driver)
    {
        switch (language)
        {
            case cxx:
                generateCxxOutput(driver);
                break;

            default:
                std::cerr << "%MSG-E-NOLANGUAGE, output language has not been set" << std::endl;
        }
    }

    void
    Output::setLanguage(Language lang)
    {
        language = lang;
    }

    void
    Output::setOutputDir(std::string& filename)
    {
        outputFileDir = filename;
    }

    void
    Output::setOutputBase(std::string& inputFile)
    {
        outputFileBase = std::filesystem::path(inputFile).stem();
    }

    void
    Output::setCopyrightFile(std::string& filename)
    {
        copyrightFile = filename;
    }

    void
    Output::setClassName(std::string& _className)
    {
        className = _className;
    }

    void
    Output::generateCxxOutput(Driver* driver)
    {
        std::string filename = (outputFileDir.length() == 0 ? "./" : "/") + outputFileBase;
        std::ofstream cxxFile(filename + ".cxx", std::ios::out);
        std::ofstream hxxFile(filename + ".hxx", std::ios::out);

        if (copyrightFile.size() > 0)
        {
            std::filesystem::directory_entry entry(copyrightFile);
            std::error_code errorCode;

            if (entry.exists(errorCode))
            {
                std::ifstream copyFile(copyrightFile, std::ios::in);
                std::string line;
                std::time_t currentTime = time(nullptr);
                char* tm = ctime(&currentTime);
                std::string date = tm;

                if (date[DateLoc] == ' ')
                {
                    date[DateLoc] = '0';
                }
                while (std::getline(copyFile, line))
                {
                    std::string token;
                    std::size_t prev = 1;
                    std::size_t start = line.find('@', prev);
                    std::size_t end = line.find(start + 1);

                    cxxFile << "//";
                    hxxFile << "//";

                    while (end != std::string::npos)
                    {
                        cxxFile << line.substr(prev, start - prev);
                        hxxFile << line.substr(prev, start - prev);

                        prev = end + 1;
                        token = line.substr(start, end - start);
                        if ((token == "@copyright-owner@") || (token == "@author@"))
                        {
                            cxxFile << "Jonathan D. Belanger";
                            hxxFile << "Jonathan D. Belanger";
                        }
                        else if (token == "@yyyy@")
                        {
                            cxxFile << date.substr(YearLoc, YearLen);
                            hxxFile << date.substr(YearLoc, YearLen);
                        }
                        else if (token == "@file-purpose@")
                        {
                            cxxFile << "the source code needed to support the Message Utility";
                            hxxFile << "the header definitions needed to support the Message Utility";
                        }
                        else if (token == "@dd-mmm-yyyy@")
                        {
                            cxxFile << date.substr(DateLoc, DataLen) << "-" << date.substr(MonthLoc, MonthLen) << "-"
                                << date.substr(YearLoc, YearLen);
                            hxxFile << date.substr(DateLoc, DataLen) << "-" << date.substr(MonthLoc, MonthLen) << "-"
                                << date.substr(YearLoc, YearLen);
                        }
                    }
                    cxxFile << line.substr(prev);
                    hxxFile << line.substr(prev);
                }
                copyFile.close();
            }
            else
            {
                std::cerr << "%MSG-E-COPYRIGHTNF, copyright file, " << copyrightFile << ", not found" << std::endl;
                std::cerr << "%MSG-I-CONTINUE, continuing..." << std::endl;
            }
        }

        hxxFile << "#pragma once" << std::endl;
        hxxFile << "#include \"MessageClass.hxx\"" << std::endl << std::endl;
        hxxFile << "namespace Msg" << std::endl << "{" << std::endl;

        cxxFile << "#include \"" << outputFileBase << ".hxx\"" << std::endl << std::endl;
        cxxFile << "namespace Msg" << std::endl << "{" << std::endl;

        uint64 symbolCount = driver->getSymbolCount();
        Symbol* symbol = driver->getSymbol(0);
        std::string facilitySymbol = symbol->symbol;
        std::string facility = facilitySymbol.substr(0, symbol->symbol.find('_'));

        if (className.length() == 0)
        {
            className = facility;
        }
        hxxFile << "    class " << className << std::endl;
        hxxFile << "    {" << std::endl;
        hxxFile << "        public:" << std::endl;
        if (driver->getBits() == Driver::Bits32)
        {
            hxxFile << "            typedef MsgCode32 MsgCode;" << std::endl;
        }
        else
        {
            hxxFile << "            typedef MsgCode64 MsgCode;" << std::endl;
        }
        hxxFile << "            " << className << "() = delete;" << std::endl;
        hxxFile << "            ~" << className << "() = delete;" << std::endl;
        hxxFile << "            " << className << "(const " << className << "&) = delete;" << std::endl;
        hxxFile << "            " << className << "& operator=(const " << className << "&) = delete;" << std::endl
            << std::endl;
        hxxFile << "            static constexpr ";
        if (driver->getBits() == Driver::Bits32)
        {
            hxxFile << "MsgCode " << facilitySymbol << " = 0x";
            hxxFile << std::hex << std::setfill('0') << std::setw(hexWidth32) << static_cast<uint32>(symbol->value)
                << ";" << std::endl;
        }
        else
        {
            hxxFile << "MsgCode " << facilitySymbol << " = 0x";
            hxxFile << std::hex << std::setfill('0') << std::setw(hexWidth64) << symbol->value << ";" << std::endl;
        }
        hxxFile << std::setfill(' ') << std::dec;

        cxxFile << "    const Message::TableEntry " << className << "::msgTable[] =" << std::endl;
        cxxFile << "    {" << std::endl;

        std::string symbolStr;
        for(uint64 ii = 1; ii < symbolCount; ii++)
        {
            symbol = driver->getSymbol(ii);

            hxxFile << "            static constexpr ";
            if (driver->getBits() == Driver::Bits32)
            {
                hxxFile << "MsgCode " << symbol->symbol << " = 0x";
                hxxFile << std::hex << std::setfill('0') << std::setw(hexWidth32) << static_cast<uint32>(symbol->value)
                    << ";" << std::endl;
            }
            else
            {
                hxxFile << "MsgCode " << symbol->symbol << " = 0x";
                hxxFile << std::hex << std::setfill('0') << std::setw(hexWidth64) << symbol->value << ";" << std::endl;
            }
            hxxFile << std::setfill(' ') << std::dec;

            if (symbol->text.length() > 0)
            {
                symbolStr = symbol->symbol.substr(symbol->symbol.find('_') + 1);

                cxxFile << "        {" << symbol->symbol << ", " << symbol->userValue << ", " << symbol->faoCount
                    << ", \"" << symbolStr << "\", \"" << symbol->text << "\"}," << std::endl;;
            }
        }
        cxxFile << "        {0, 0, 0, \"\"}" << std::endl << "    };" << std::endl << std::endl;
        hxxFile << "            static const Message::TableEntry msgTable[];" << std::endl << std::endl;

        hxxFile << "            static Message*" << std::endl;
        hxxFile << "            registerMessages();" << std::endl;
        hxxFile << "    };" << std::endl;

        cxxFile << "    Message*" << std::endl;
        cxxFile << "    " << className << "::registerMessages()" << std::endl;
        cxxFile << "    {" << std::endl;
        cxxFile << "        std::string facNam = \"" << facility << "\";" << std::endl;
        cxxFile << "        Message* msg = Message::getInstance();" << std::endl << std::endl;
        cxxFile << "        msg->registerFacility(" << facilitySymbol << ", facNam, " << facility << "::msgTable);"
            << std::endl;
        cxxFile << "        return msg;" << std::endl;
        cxxFile << "    }" << std::endl;

        cxxFile << "}" << std::endl << std::endl;
        hxxFile << "}" << std::endl << std::endl;

        cxxFile.close();
        hxxFile.close();
    }
}
