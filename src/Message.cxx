//
// Copyright (C) Jonathan D. Belanger 2024.
// All Rights Reserved.
//
// This software is furnished under a license and may be used and copied only in accordance with the terms of such
// license and with the inclusion of the above copyright notice.  This software or any other copies thereof may not be
// provided or otherwise made available to any other person.  No title to and ownership of the software is hereby
// transferred.
//
// The information in this software is subject to change without notice and should not be construed as a commitment by
// the author or co-authors.
//
// The author and any co-authors assume no responsibility for the use or reliability of this software.
//
// Description:
//
//! @file
//  This file contains the main function used to process Message Utility formatted files and generate programming
//  language specific output files.  These files can be compiled into other programs that want to use OpenVMS-like
//  status vectors to be able to report a hierarchy of error messages.  For example:
//
//          %TYPE-W-SEARCHFAIL, error searching for DKA0:[000000]NOTHERE.LIS;
//          -RMS-E-FNF, file not found
//
// Revision History:
//
//  V01.000 06-May-2024 Jonathan D. Belanger
//  Initially written.
//
#include "MessageScannerClass.hxx"
#include "MessageParser.hxx"
#include "MessageDriver.hxx"
#include "MessageOutput.hxx"
#include <cstring>
#include <iostream>
#include <getopt.h>

static std::string version = "message 1.0.0";

void
usage(char* programName)
{
    std::cerr << "Usage: " << programName << " [OPTION]... FILE..." << std::endl;
    std::cerr << "Generate language specific header for the purpose of supporting OpenVMS-like messages" << std::endl;
    std::cerr << "\t-3, --32-bit            generate 32-bit compatible code (exclusive of -6/--64-bit)" << std::endl;
    std::cerr << "\t-6, --64-bit            generate 64-bit compatible code (exclusive of -3/--32-bit)" << std::endl;
    std::cerr << "\t-o, --output=DIR        write output to DIR directory" << std::endl;
    std::cerr << "\t-c, --copyright=FILE    use FILE as the copyright comment at start of output" << std::endl;
    std::cerr << "\t-C, --class=NAME        use NAME as the name of the class to be defined" << std::endl;
    std::cerr << "\t-l, --list[=FILE]       generate a listing file to FILE" << std::endl;
    std::cerr << "\t-L, --language=LANG     specify the output programming language" << std::endl;
    std::cerr << "\t-p, --parse-trace       write parser trace information to stdout" << std::endl;
    std::cerr << "\t-t, --scan-trace        write scanner trace information to stdout" << std::endl;
    std::cerr << "\t-v, --version           display message version" << std::endl;
    std::cerr << "\t-h, --help              display this help message" << std::endl;
}

int
main(int argc, char** argv)
{
    Msg::Driver driver;
    int result = 0;
    Msg::Driver::NumBits bits = Msg::Driver::Bits32;
    std::string outputDir;
    std::string copyrightFile;
    std::string listingFile;
    std::string className;
    Msg::Output::Language lang = Msg::Output::None;
    bool traceParse = false;
    bool traceScan = false;
    bool bitsSpecified = false;
    bool optionError = false;
    bool generateListFile = false;
    static const char* shortOptions = "36o:c:C:l:L:ptvh";
    static const struct option longOptions[] =
    {
        {"32-bit",      no_argument,        nullptr, '3'},
        {"64-bit",      no_argument,        nullptr, '6'},
        {"output",      required_argument,  nullptr, 'o'},
        {"copyright",   required_argument,  nullptr, 'c'},
        {"class",       required_argument,  nullptr, 'C'},
        {"list",        optional_argument,  nullptr, 'l'},
        {"language",    required_argument,  nullptr, 'L'},
        {"parse-trace", no_argument,        nullptr, 'p'},
        {"scan-trace",  no_argument,        nullptr, 's'},
        {"version",     no_argument,        nullptr, 'v'},
        {"help",        no_argument,        nullptr, 'h'},
        {nullptr,       no_argument,        nullptr,  0 }
    };
    int optChar;
    int optionIndex;

    //
    // Process the command-line arguments
    //
    while (true)
    {
        optChar = getopt_long(argc, argv, shortOptions, longOptions, &optionIndex);
        if (optChar == -1)
        {
            break;
        }

        switch (optChar)
        {
            case '3':
                if (!bitsSpecified)
                {
                    bits = Msg::Driver::Bits32;
                    bitsSpecified = true;
                }
                else
                {
                    std::cerr << "ERROR: Only one of --32-bit (-3) and/or --64-bit (-6) can be specified" << std::endl;
                    optionError = true;
                }
                break;

            case '6':
                if (!bitsSpecified)
                {
                    bits = Msg::Driver::Bits64;
                    bitsSpecified = true;
                }
                else
                {
                    std::cerr << "ERROR: Only one of --32-bit (-3) and/or --64-bit (-6) can be specified" << std::endl;
                    optionError = true;
                }
                break;

            case 'c':
                copyrightFile = optarg;
                break;

            case 'C':
                className = optarg;
                break;

            case 'o':
                outputDir = optarg;
                break;

            case 'l':
                generateListFile = true;
                if (std::strlen(optarg) > 0)
                {
                    if (optarg[0] != '-')
                    {
                        listingFile = std::string(optarg);
                        std::cerr << listingFile << std::endl;
                    }
                    else
                    {
                        optind--;
                    }
                }
                break;

            case 'L':
                if ((std::string("cxx") == std::string(optarg)) || (std::string("cpp") == std::string(optarg))
                    || (std::string("c++") == std::string(optarg)))
                {
                    lang = Msg::Output::cxx;
                }
                else
                {
                    std::cerr << "ERROR: Unrecognized language, " << optarg << ", specified" << std::endl;
                    optionError = true;
                }
                break;

            case 'p':
                traceParse = true;
                break;

            case 's':
                traceScan = true;
                break;

            case 'v':
                std::cerr << version << std::endl;
                return 0;

            case 'h':
                usage(argv[0]);
                return 0;

            default:
                break;
        }
    }

    //
    // At the very least, we need to have the number of bits, 32 or 64, and the language, c++, specified.
    //
    if (!bitsSpecified)
    {
        std::cerr << "ERROR: At least one of --32-bit (-3) or --64-bit (-6) must be specified" << std::endl;
    }
    if (lang == Msg::Output::None)
    {
        std::cerr << "ERROR: The language to generate (cxx, cpp, or c++) must be specified" << std::endl;
    }

    //
    // If all the required arguments have been specified and there were no other option errors, then start processing
    // the supplied files, one at a time.
    //
    if (bitsSpecified && (lang != Msg::Output::None) && !optionError)
    {
        if (optind < argc)
        {

            //
            // First set up the driver with the information from the command-line it will need to be able to parse the
            // input message file(s).
            //
            driver.setBits(bits);
            driver.setLanguage(lang);
            driver.setOutputDir(outputDir);
            driver.setCopyrightFile(copyrightFile);
            driver.setTrace(traceParse, traceScan);

            //
            // Loop through each of the remaining command-line arguments, which should be filenames, and process them
            // through the parser.
            //
            while ((optind < argc) && (result == 0))
            {
                std::string inputFile = argv[optind];

                if (generateListFile)
                {
                    driver.setListing(listingFile, inputFile);
                }

                result = driver.parse(inputFile);
                optind++;
            }
        }
        else
        {
            std::cerr << "%MSG-F-NOINFILE, no input file specified" << std::endl;
            result = -1;
        }
    }
    else
    {
        result = -1;
    }
    return result;
}
