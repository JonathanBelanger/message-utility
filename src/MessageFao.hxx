//
// Copyright (C) Jonathan D. Belanger 2024.
// All Rights Reserved.
//
// This software is furnished under a license and may be used and copied only in accordance with the terms of such
// license and with the inclusion of the above copyright notice.  This software or any other copies thereof may not be
// provided or otherwise made available to any other person.  No title to and ownership of the software is hereby
// transferred.
//
// The information in this software is subject to change without notice and should not be construed as a commitment by
// the author or co-authors.
//
// The author and any co-authors assume no responsibility for the use or reliability of this software.
//
// Description:
//
//! @file
//  This file contains the definitions needed to determine the number of arguments required for a particular FAO
//  directive, as well as the code to take a string with or without FAO directives and generate a formatted string
//  using information from a particular location within a status vector.
//
// Revision History:
//
//  V01.000 13-May-2024 Jonathan D. Belanger
//  Initially written.
//
#pragma once
#include "MessageDataTypes.hxx"
#include "MessageVector.hxx"

//
//! @namespace Msg
//! @brief This namespace is used throughout the definitions and code for the Message Programming Interface.
//
namespace Msg
{

    //
    //! @class Fao
    //! @brief This class is effectively a static class in that it cannot be instantiated (Constructor and Destructor
    //!        has been deleted).  It contains the definitions to be able to get FAO information and processing this
    //!        information into a string.
    //
    class Fao
    {
        public:
            Fao() = delete;
            ~Fao() = delete;
            Fao(const Fao&) = delete;
            Fao& operator=(const Fao&) = delete;

            //
            //! @struct FaoInfo
            //! @brief This structure is used to store information about each of the FAO directives.
            //
            struct FaoInfo
            {
                public:
                    std::string faoCmd;         //!< The specific FAO directive string
                    int argCount;               //!< The number of arguments required for this directive.
            };

            static std::string&
            processFaoString(const std::string&, MsgVec&, uint32*, bool);

            static FaoInfo*
            getNextFaoTypeIndex(const std::string&, std::size_t*);

        private:
            static constexpr uint32 BufferLen = 32;         //!< Number of bytes used to convert a value to a string
            static constexpr uint32 ByteMask = 0x000000ff;  //!< A mask used to isolate the low-order byte in a long
            static constexpr uint32 WordMask = 0x0000ffff;  //!< A mask used to isolate the low-order short in a long
    };
}
