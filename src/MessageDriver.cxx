//
// Copyright (C) Jonathan D. Belanger 2024.
// All Rights Reserved.
//
// This software is furnished under a license and may be used and copied only in accordance with the terms of such
// license and with the inclusion of the above copyright notice.  This software or any other copies thereof may not be
// provided or otherwise made available to any other person.  No title to and ownership of the software is hereby
// transferred.
//
// The information in this software is subject to change without notice and should not be construed as a commitment by
// the author or co-authors.
//
// The author and any co-authors assume no responsibility for the use or reliability of this software.
//
// Description:
//
//! @file
//  This file contains the code to process the scanned and parsed data to be used as part of the parsing, as well as the
//  generation of the language specific definitions and code.
//
// Revision History:
//
//  V01.000 06-May-2024 Jonathan D. Belanger
//  Initially written.
//
#include "MessageDriver.hxx"
#include "MessageValues.hxx"
#include <string>
#include <sstream>
#include <fstream>

namespace Msg
{
    struct FaoPars
    {
        public:
            std::string faoCmd;
            int numPars;
    };
    static FaoPars faoPars[] =
    {
        {"!AC", 1}, {"!AD", 2}, {"!AF", 2}, {"!AS", 1}, {"!AZ", 1}, {"!OB", 1}, {"!OW", 1}, {"!OL", 1}, {"!OQ", 1},
        {"!OA", 1}, {"!OI", 1}, {"!OH", 1}, {"!OJ", 1}, {"!XB", 1}, {"!XW", 1}, {"!XL", 1}, {"!XQ", 1}, {"!XA", 1},
        {"!XI", 1}, {"!XH", 1}, {"!XJ", 1}, {"!ZB", 1}, {"!ZW", 1}, {"!ZL", 1}, {"!ZQ", 1}, {"!ZA", 1}, {"!ZI", 1},
        {"!ZH", 1}, {"!ZJ", 1}, {"!UB", 1}, {"!UW", 1}, {"!UL", 1}, {"!UQ", 1}, {"!UA", 1}, {"!UI", 1}, {"!UH", 1},
        {"!UJ", 1}, {"!SB", 1}, {"!SW", 1}, {"!SL", 1}, {"!SQ", 1}, {"!SA", 1}, {"!SI", 1}, {"!SH", 1}, {"!SJ", 1},
        {"!/", 0},  {"!_", 0},  {"!^", 0},  {"!!", 0},  {"!%S", 0}, {"!%T", 1}, {"!%U", 1}, {"!%I", 1}, {"!%D", 1},
        {"!–", 0}, {"!+", 0}, {"", 0}
    };

    static std::string qualifierStr[] =
    {
        "", "/PREFIX=prefix", "/SHARED", "/SYSTEM", "/FAO_COUNT=n", "/IDENTIFICATION=name", "/USER_VALUE=n", "/SUCCESS",
        "/INFORMATIONAL", "/WARNING", "/ERROR", "/FATAL or /SEVERE"
    };

    static Driver::Qualifier qualList[] =
    {
        Driver::Success,
        Driver::Informational,
        Driver::Warning,
        Driver::Error,
        Driver::Fatal
    };

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //  Public Methods                                                                                              //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    int
    Driver::parse(std::string& inputFile)
    {
        int result = 0;
        std::stringstream ss;
        std::string msg;

        _location.initialize(&inputFile);
        if (inputFile.length() > 0)
        {
            std::ifstream inFile(inputFile.c_str(), std::ios::in);

            if (inFile.good())
            {
                output.setOutputBase(inputFile);

                //
                // Try to create the scanner
                //
                try
                {
                    scanner = new Scanner(&inFile, *this);
                }
                catch (std::bad_alloc &ba)
                {
                    ss << "%MSG-F-INSUFMEM, Insufficient memory to allocate scanner, " << ba.what();
                    msg = ss.str();
                    listOutput.writeError(msg, _location);
                    ss.flush();
                    msg.clear();
                    return -1;
                }

                //
                // Turn on scanner tracing, if enabled.
                //
                scanner->set_debug(traceScan);

                //
                // Try to create the parser
                //
                try
                {
                    parser = new Parser(*scanner, *this);
                }
                catch (std::bad_alloc &ba)
                {
                    ss << "%MSG-F-INSUFMEM, Insufficient memory to allocate parser, " << ba.what();
                    msg = ss.str();
                    listOutput.writeError(msg, _location);
                    ss.flush();
                    msg.clear();
                    return -1;
                }

                //
                // Turn on parser tracing, if enabled.
                //
                parser->set_debug_level(traceParse);

                //
                // If listing is enabled, then open it up.
                //
                if (listingEnabled)
                {
                    listOutput.open();
                }

                //
                // Start parsing the input file.
                //
                result = parser->parse();
            }
            else
            {
                ss << "%MSG-F-FNF, file not found, " << inputFile;
                msg = ss.str();
                listOutput.writeError(msg, _location);
                ss.flush();
                msg.clear();
            }
        }
        else
        {
            ss << "%MSG-F-FILNSPR, file not specified";
            msg = ss.str();
            listOutput.writeError(msg, _location);
            ss.flush();
            msg.clear();
        }
        return result;
    }

    void
    Driver::processBase(int64 base)
    {
        int64 maxValue = bits == Bits32 ? Constants::MaxMessageNum32 : Constants::MaxMessageNum64;
        std::stringstream ss;
        std::string msg;

        if ((base > 0)  && (base <= maxValue))
        {
            nextMessageNumber = base;
        }
        else if (base <= 0)
        {
            ss << "%MSG-E-BASETOOLOW, .BASE value must be greater than 0";
            msg = ss.str();
            listOutput.writeError(msg, _location);
            ss.flush();
            msg.clear();
        }
        else
        {
            ss << "%MSG-E-BASETOOLARG, .BASE value must be less than " << maxValue;
            msg = ss.str();
            listOutput.writeError(msg, _location);
            ss.flush();
            msg.clear();
        }
    }

    void
    Driver::processEnd()
    {
        output.generateOutput(this);
    }

    void
    Driver::processFacility(std::string& facnam, int64 facnum)
    {
        int64 maxValue = bits == Bits32 ? Constants::MaxFacilityNum32 : Constants::MaxFacilityNum64;
        std::stringstream ss;
        std::string msg;

        if ((facnum > 0)  && (facnum <= maxValue) &&
            (facnam.length() > 0) && (facnam.length() <= Constants::MaxFacilityNameLen))
        {
            DirectiveQualifier* qual;
            std::string facSym = facnam + "_FACILITY";
            Symbol *symb = findSymbol(facSym);

            if (symb == nullptr)
            {
                facilityName = facnam;
                facilityNumber = facnum;

                addSymbol(facSym, facnum);

                qual = findQualifier(Prefix);
                if (qual != nullptr)
                {
                    prefix = qual->strValue;
                }
                qual = findQualifier(Shared);
                if (qual != nullptr)
                {
                    shared = true;
                }
                qual = findQualifier(System);
                if (qual != nullptr)
                {
                    system = true;
                }
            }
            else
            {
                ss << "%MSG-E-FACDEFN, Facility " << facnam << " already defined";
                msg = ss.str();
                listOutput.writeError(msg, _location);
                ss.flush();
                msg.clear();
            }
        }
        else if ((facnum <= 0) || (facnum > maxValue))
        {
            ss << "%MSG-E-FACNUMVAL, .FACILITY facnum must be between 1 and " << maxValue;
            msg = ss.str();
            listOutput.writeError(msg, _location);
            ss.flush();
            msg.clear();
        }
        else
        {
            ss << "%MSG-E-FACNAMLEN, .FACILITY facnam length must be between than 1 and "
                << Constants::MaxFacilityNameLen << " characters long";
            msg = ss.str();
            listOutput.writeError(msg, _location);
            ss.flush();
            msg.clear();
        }
        qualifiers.clear();
    }

    void
    Driver::processIdent(std::string& string)
    {
        std::size_t start = 0;
        std::size_t len = string.size();
        std::stringstream ss;
        std::string msg;

        if ((len > 1) && (string[0] == '\''))
        {
            start = 1;
            len -= 2;
        }
        if ((len <= 0) || (len > Constants::MaxIdentLen))
        {
            ss << "%MSG-E-IDENTLEN, .IDENT string length must be between 1 and "
                << Constants::MaxIdentLen << " characters";
            msg = ss.str();
            listOutput.writeError(msg, _location);
            ss.flush();
            msg.clear();
        }
        else
        {
            identification = string.substr(start, len);
        }
    }

    void
    Driver::processLiteral(std::string& symb)
    {
        if (processLiteral(symb, nextLiteralNumber))
        {
            nextLiteralNumber++;
        }
    }

    bool
    Driver::processLiteral(std::string& symb, int64 val)
    {
        std::string qualSymb = ((prefix.length() > 0) ? (prefix + "_" + symb) : symb);
        Symbol* found = findSymbol(qualSymb);
        std::stringstream ss;
        std::string msg;

        if (found == nullptr)
        {
            Symbol* newSymbol = new Symbol(qualSymb, val);

            symbols.push_back(newSymbol);
        }
        else if (found->text.length() == 0)
        {
            found->symbol = qualSymb;
            found->value = val;
        }
        else
        {
            ss << "%MSG-E-SYMBOVERR, " << qualSymb << " already defined as a message value";
            msg = ss.str();
            listOutput.writeError(msg, _location);
            ss.flush();
            msg.clear();
            return false;
        }
        return true;
    }

    void
    Driver::processMessage(std::string& name, std::string& text)
    {
        std::size_t start = 1;
        std::size_t len = text.size() - 2;
        std::stringstream ss;
        std::string msg;

        if (len > 0)
        {
            for (std::size_t ii = start; ii < len; ii++)
            {
                if ((text[ii] == ' ') || (text[ii] == '\t'))
                {
                    start++;
                    len--;
                    continue;
                }
                break;
            }
        }
        if (len > 0)
        {
            for (std::size_t ii = start + len; ii > start; ii--)
            {
                if ((text[ii] == ' ') || (text[ii] == '\t'))
                {
                    len--;
                    continue;
                }
                break;
            }
        }
        if ((name.length() > 0) && (name.length() < Constants::MaxSymbolLen) &&
            (len > 0) && (len < Constants::MaxMessageTextLen))
        {
            DirectiveQualifier* qual;
            int64 count = 0;
            int64 userValue = 0;
            uint32 sev = severity;
            std::string msgText;
            std::string msgSym = ((prefix.length() > 0) ? (prefix + "_" + name) : name);
            Msg64 msg64;
            Msg32 msg32;

            qual = findQualifier(FaoCount);
            if (qual != nullptr)
            {
                count = qual->intValue;
            }

            if (faoCount(text) == count)
            {
                qual = findQualifier(Identification);
                if (qual != nullptr)
                {
                    msgText += qual->strValue;
                }
                else
                {
                    msgText += name;
                }

                qual = nullptr;
                for (uint32 ii = 0; ((ii < sizeof(qualList)) && (qual == nullptr)); ii++)
                {
                    qual = findQualifier(qualList[ii]);
                }
                if (qual != nullptr)
                {
                    sev = qual->intValue;
                }
                if (bits == Bits32)
                {

                    msg32.setOutputControl(0);
                    msg32.setFacilityNumber(static_cast<uint32>(facilityNumber));
                    msg32.setMessageNumber(static_cast<uint32>(nextMessageNumber));
                    msg32.setSeverity(sev);
                }
                else
                {
                    msg64.setOutputControl(0);
                    msg64.setFacilityNumber(static_cast<uint64>(facilityNumber));
                    msg64.setMessageNumber(static_cast<uint64>(nextMessageNumber));
                    msg64.setSeverity(sev);
                }
                if (nullptr == findSymbol(msgSym))
                {
                    int64 value = ((bits == Bits32) ? static_cast<int64>(msg32.get()) : msg64.get());

                    addSymbol(msgSym, value, userValue, text.substr(start, len), count);
                    nextMessageNumber++;
                }
                else
                {
                    ss << "%MSG-E-MSGDEFND, Message, " << msgSym << ", already defined";
                    msg = ss.str();
                    listOutput.writeError(msg, _location);
                    ss.flush();
                    msg.clear();
                }
            }
            else
            {
                ss << "%MSG-E-FAOMISM, /FAO_COUNT=" << count << " and supplied FAO directives in message," << faoCount(text) << ", do not match";
                msg = ss.str();
                listOutput.writeError(msg, _location);
                ss.flush();
                msg.clear();
            }
        }
        else if ((name.length() > 0) && (name.length() < Constants::MaxSymbolLen))
        {
            ss << "%MSG-E-MSGNAMELEN, Message name length must be between 1 and " << Constants::MaxSymbolLen
                << " characters long";
            msg = ss.str();
            listOutput.writeError(msg, _location);
            ss.flush();
            msg.clear();
        }
        else
        {
            ss << "%MSG-E-MSGTEXTLEN, Message text length must be between 1 and " << Constants::MaxMessageTextLen
                << " characters long";
            msg = ss.str();
            listOutput.writeError(msg, _location);
            ss.flush();
            msg.clear();
        }
        qualifiers.clear();
    }

    void
    Driver::processPage()
    {
        // For now, we are ignoring .PAGE directive.  Maybe when we support generating a listing file.
    }

    void
    Driver::processSeverity(uint32 sev)
    {
        severity = sev;
    }

    void
    Driver::processTitle(std::string& modname)
    {
        moduleName = modname;
    }

    void
    Driver::processTitle(std::string& modname, std::string& listingTitle)
    {
        moduleName = modname;
        listingText = listingTitle;
    }

    void
    Driver::addQualifier(Qualifier qual)
    {
        DirectiveQualifier* found = findQualifier(qual);
        std::stringstream ss;
        std::string msg;

        if (found == nullptr)
        {
            bool addQual = true;
            bool isSeverity = false;

            for (uint32 ii = 0; ((ii < sizeof(qualList)) && !isSeverity); ii++)
            {
                isSeverity = qualList[ii] == qual;
            }
            if (isSeverity)
            {
                for (uint32 ii = 0; ((ii < sizeof(qualList)) && addQual); ii++)
                {
                    if (qualList[ii] != qual)
                    {
                        addQual = nullptr == findQualifier(qualList[ii]);
                    }
                }
            }

            if (addQual)
            {
                DirectiveQualifier* newQual = new DirectiveQualifier(qual);
                qualifiers.push_back(newQual);
            }
        }
        else
        {
            ss << "%MSG-E-DUPQUAL, duplicate qualifier, " << qualifierStr[static_cast<int>(qual)]
                << ", specified";
            msg = ss.str();
            listOutput.writeError(msg, _location);
            ss.flush();
            msg.clear();
        }
    }

    void
    Driver::addQualifier(Qualifier qual, int64 val)
    {
        DirectiveQualifier* found = findQualifier(qual);
        std::stringstream ss;
        std::string msg;

        if ((qual == FaoCount) && ((val < 0) || (val > static_cast<int64>(Constants::MaxFaoCount))))
        {
            ss << "%MSG-E-FAOCNTVAL, /FAO_COUNT=n value must be between 0 and " << Constants::MaxFaoCount;
            msg = ss.str();
            listOutput.writeError(msg, _location);
            ss.flush();
            msg.clear();
        }
        else if ((qual == UserValue) && ((val < 0) || (val > static_cast<int64>(Constants::MaxUserValue))))
        {
            ss << "%MSG-E-USEVALVAL, /USER_VALUE=n value must be between 0 and " << Constants::MaxUserValue;
            msg = ss.str();
            listOutput.writeError(msg, _location);
            ss.flush();
            msg.clear();
        }
        else if (found != nullptr)
        {
            ss << "%MSG-E-DUPQUAL, duplicate qualifier, " << qualifierStr[static_cast<int>(qual)]
                << ", specified";
            msg = ss.str();
            listOutput.writeError(msg, _location);
            ss.flush();
            msg.clear();
        }
        else
        {
            DirectiveQualifier* newQual = new DirectiveQualifier(qual, val);

            qualifiers.push_back(newQual);
        }
    }

    void
    Driver::addQualifier(Qualifier qual, std::string& val)
    {
        DirectiveQualifier* found = findQualifier(qual);
        std::stringstream ss;
        std::string msg;

        if ((qual == Prefix) && ((val.length() == 0) || (val.length() > Constants::MaxPrefixLen)))
        {
            ss << "%MSG-E-PREFIXLEN, /PREFIX=prefix length must be between 1 and " << Constants::MaxPrefixLen
                << " characters";
            msg = ss.str();
            listOutput.writeError(msg, _location);
            ss.flush();
            msg.clear();
        }
        else if ((qual == Identification) && ((val.length() == 0) || (val.length() > Constants::MaxIdentLen)))
        {
            ss << "%MSG-E-IDENTQLEN, /IDENTIFICATION=name length must be between 1 and "
                << Constants::MaxIdentLen << " characters";
            msg = ss.str();
            listOutput.writeError(msg, _location);
            ss.flush();
            msg.clear();
        }
        else if (found != nullptr)
        {
            ss << "%MSG-E-DUPQUAL, duplicate qualifier, " << qualifierStr[static_cast<int>(qual)]
                << ", specified";
            msg = ss.str();
            listOutput.writeError(msg, _location);
            ss.flush();
            msg.clear();
        }
        else
        {
            std::string value;
            int lastChar = val.size() - 1;

            if ((qual == Prefix) && (val[lastChar] == '_'))
            {
                value = val.substr(0, lastChar);
            }
            else
            {
                value = val;
            }
            DirectiveQualifier* newQual = new DirectiveQualifier(qual, value);

            qualifiers.push_back(newQual);
        }
    }

    int64
    Driver::getSymbolValue(std::string& symbol)
    {
        Symbol* found = findSymbol(symbol);

        if (found != nullptr)
        {
            return found->value;
        }
        return 0;
    }

    uint64
    Driver::getSymbolCount()
    {
        return symbols.size();
    }

    Symbol*
    Driver::getSymbol(uint64 index)
    {
        if (index < symbols.size())
        {
            return symbols[index];
        }
        return nullptr;
    }

    Driver::NumBits
    Driver::getBits() const
    {
        return bits;
    }

    bool
    Driver::getListing() const
    {
        return listingEnabled;
    }

    void
    Driver::setBits(NumBits bitsToSet)
    {
        bits = bitsToSet;
    }

    void
    Driver::setLanguage(Output::Language lang)
    {
        output.setLanguage(lang);
    }

    void
    Driver::setOutputDir(std::string& outputDir)
    {
        output.setOutputDir(outputDir);
    }

    void
    Driver::setCopyrightFile(std::string& copyrightFile)
    {
        output.setCopyrightFile(copyrightFile);
    }

    void
    Driver::setClassName(std::string& className)
    {
        output.setClassName(className);
    }

    void
    Driver::setTrace(bool parse, bool scan)
    {
        traceParse = parse;
        traceScan = scan;
    }

    void
    Driver::setListing(std::string& listingFile, std::string& inputFile)
    {
        if ((listingFile.size() > 0) || (inputFile.size() > 0))
        {
            listingEnabled = true;
            listOutput.setListingFilename(listingFile, inputFile);
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //  Private Methods                                                                                             //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    Msg::Symbol*
    Driver::findSymbol(const std::string& symbol)
    {
        for (uint64 ii = 0; ii < symbols.size(); ii++)
        {
            if (symbol == symbols[ii]->symbol)
            {
                return symbols[ii];
            }
        }
        return nullptr;
    }

    Driver::DirectiveQualifier*
    Driver::findQualifier(Qualifier qual)
    {
        for (uint64 ii = 0; ii < qualifiers.size(); ii++)
        {
            if (qual == qualifiers[ii]->qualifier)
            {
                return qualifiers[ii];
            }
        }
        return nullptr;
    }

    void
    Driver::addSymbol(const std::string& symbol, const int64 value)
    {
        Symbol* found = findSymbol(symbol);

        if (found == nullptr)
        {
            Symbol* newSymbol = new Symbol(symbol, value);

            symbols.push_back(newSymbol);
        }
    }

    void
    Driver::addSymbol(const std::string& symbol,
                      const int64 value,
                      const int64 userVal,
                      const std::string& msgText,
                      const int64 fao)
    {
        Symbol* found = findSymbol(symbol);

        if (found == nullptr)
        {
            Symbol* newSymbol = new Symbol(symbol, value, userVal, msgText, fao);

            symbols.push_back(newSymbol);
        }
    }

    int64
    Driver::faoCount(const std::string& msgText)
    {
        int64 count = 0;
        std::size_t len = msgText.length();
        std::size_t pos = 0;

        while (pos < len)
        {
            if (msgText[pos] == '!')
            {
                int faoIdx = 0;

                while (faoPars[faoIdx].faoCmd.length() != 0)
                {
                    if (msgText.substr(pos, faoPars[faoIdx].faoCmd.length()) == faoPars[faoIdx].faoCmd)
                    {
                        count += faoPars[faoIdx].numPars;
                        break;
                    }
                    faoIdx++;
                }
            }
            pos++;
        }
        return count;
    }

    void
    Driver::updateLocation(uint32 newLine, uint32 len)
    {
        _location.step();
        if (newLine != 0)
        {
            _location.lines(newLine);
        }
        _location.columns(len);
    }

    location&
    Driver::getLocation()
    {
        return _location;
    }
}
