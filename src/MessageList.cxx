//
// Copyright (C) Jonathan D. Belanger 2024.
// All Rights Reserved.
//
// This software is furnished under a license and may be used and copied only in accordance with the terms of such
// license and with the inclusion of the above copyright notice.  This software or any other copies thereof may not be
// provided or otherwise made available to any other person.  No title to and ownership of the software is hereby
// transferred.
//
// The information in this software is subject to change without notice and should not be construed as a commitment by
// the author or co-authors.
//
// The author and any co-authors assume no responsibility for the use or reliability of this software.
//
// Description:
//
//! @file
//  This file contains the code needed to generate a listing file from the input data.  It also displays error messages
//  to standard error, even if a listing file is not being generated (see writeError() function).
//
// Revision History:
//
//  V01.000 16-May-2024 Jonathan D. Belanger
//  Initially written.
//
#include "MessageList.hxx"
#include <chrono>
#include <filesystem>

namespace Msg
{
    void
    Listing::open()
    {
        if (listFile.length() != 0)
        {
            outputStream.open(listFile, std::ios::out);
            if (outputStream.good())
            {
                initializeHeader();
            }
            else
            {
                std::cerr << "ERROR: Unable to open listing file, " << listFile << std::endl;
            }
        }
    }

    void
    Listing::writeData(std::string& buffer)
    {
        if (outputStream.is_open())
        {
            std::vector<std::string> lines = split(buffer, '\n');

            for(uint32 ii = 0; ii < lines.size(); ii++)
            {
                uint32 remainingColumns = PageWidth - columnNumber;

                if (pageLine == 1)
                {
                    endPage();
                }

                if (columnNumber == 0)
                {
                    outputStream << " " << std::setw(LineNumberWidth) << listingLine << " ";
                    columnNumber = LineNumberFieldWidth;
                }

                if ((remainingColumns + lines[ii].length()) >= columnNumber)
                {
                    lines[ii] = lines[ii].substr(0, columnNumber - remainingColumns);
                }
                outputStream << lines[ii];

                if (((ii + 1) != lines.size()) || ((buffer.length() == 1) && (buffer[0] == '\n')))
                {
                    outputStream << std::endl;
                    listingLine++;
                    pageLine++;
                    columnNumber = 0;
                    writeMessages();
                }
            }
        }
    }

    void
    Listing::writeError(std::string& buffer, const Parser::location_type& location)
    {
        if (outputStream.is_open())
        {
            ErrorInfo message = {buffer, location.begin.line, location.end.line, location.begin.column, location.end.column};

            message.text = buffer;
            message.startLine = location.begin.line;
            message.endLine = location.end.line;
            message.startColumn = location.begin.column;
            message.endColumn = location.end.column;
            messages.push_back(message);
        }
        std::cerr << buffer << std::endl;
    }

    void
    Listing::close()
    {
        if (outputStream.is_open())
        {
            outputStream.close();
        }
    }

    void
    Listing::setListingFilename(std::string& listFilename, std::string& inFile)
    {
        inputFile = inFile;
        if ((listFile.length() == 0) && (inputFile.length() != 0))
        {
            std::time_t now = std::time(nullptr);
            std::filesystem::path inputPath(inputFile);
            const auto filTime = std::filesystem::last_write_time(inputPath);
            std::time_t modTime = std::chrono::system_clock::to_time_t(std::chrono::file_clock::to_sys(filTime));

            if (listFilename.length() == 0)
            {
                std::size_t directoryEnd = inputFile.rfind('/');

                if (directoryEnd != std::string::npos)
                {
                    directoryEnd++;
                }
                else
                {
                    directoryEnd = 0;
                }
                listFile = inputFile.substr(directoryEnd, inputFile.rfind('.') - directoryEnd) + ".lis";
            }
            else
            {
                listFile = listFilename;
            }
            inputFile = std::filesystem::absolute(inputPath).string();
            runTime = *std::localtime(&now);
            modifyTime = *std::localtime(&modTime);
        }
    }

    void
    Listing::initializeHeader()
    {
        if (listingHeader1.length() == 0)
        {
            char buffer[BufferWidth];

            std::strftime(buffer, BufferWidth, "%d-%b-%Y %H:%M:%S Message V1.0-0", &runTime);
            listingHeader1 = std::string(HeaderLeadingSpaces, ' ') + std::string(buffer);
            listingHeader1 += std::string(PageNumberLocation - listingHeader1.length(), ' ') + "Page ";

            std::strftime(buffer, BufferWidth, "%d-%b-%Y %H:%M:%S", &modifyTime);
            listingHeader2 = std::string(HeaderLeadingSpaces, ' ') + std::string(buffer) + " "
                + inputFile.substr(0, HeaderTrailingSpaces);
        }
    }

    void
    Listing::writeMessages()
    {
        for (uint32 ii = 0; ii < messages.size(); ii++)
        {
            if ((pageLine + ErrorMessageLines) == PageLength)
            {
                endPage();
            }
            if (messages[ii].startColumn < PageWidth)
            {
                std::string buffer;

                outputStream << std::string(messages[ii].startColumn + LineNumberFieldWidth - 1, ' ') << "^";
                if (messages[ii].endColumn < PageWidth)
                {
                    if ((messages[ii].endColumn - messages[ii].startColumn - 1) > 1)
                    {
                        buffer = std::string(messages[ii].endColumn - messages[ii].startColumn - 2, '-') + "^";
                    }
                    if (messages[ii].endColumn != messages[ii].startColumn - 1)
                    {
                        buffer += "^";
                    }
                }
                else
                {
                    buffer = std::string(PageWidth - messages[ii].startColumn, '-');
                }
                outputStream << buffer << std::endl;
                pageLine++;
            }
            if (messages[ii].text.length() > PageWidth)
            {
                messages[ii].text = messages[ii].text.substr(0, PageWidth);
            }
            outputStream << messages[ii].text << std::endl;
            pageLine++;
        }
        messages.clear();
    }

    void
    Listing::endPage()
    {
        if (listingLine > 0)
        {
            outputStream << "\f" << std::endl;
        }
        outputStream << listingHeader1 << std::setw(PageNumberWidth) << pageNumber++ << std::endl;
        outputStream << listingHeader2 << std::endl;
        pageLine = 2;
        columnNumber = 0;
    }

    std::vector<std::string>
    Listing::split(const std::string& text, const char splitChar)
    {
        std::vector<std::string> retStrs;
        std::size_t start = 0;
        std::size_t end = text.find(splitChar);

        retStrs.push_back(text.substr(start, end));
        start = end + 1;
        while (start >= std::string::npos)
        {
            end = text.find(splitChar);
            retStrs.push_back(text.substr(start, end));
            start = end + 1;
        }
        return retStrs;
    }
}
