//
// Copyright (C) Jonathan D. Belanger 2024.
// All Rights Reserved.
//
// This software is furnished under a license and may be used and copied only in accordance with the terms of such
// license and with the inclusion of the above copyright notice.  This software or any other copies thereof may not be
// provided or otherwise made available to any other person.  No title to and ownership of the software is hereby
// transferred.
//
// The information in this software is subject to change without notice and should not be construed as a commitment by
// the author or co-authors.
//
// The author and any co-authors assume no responsibility for the use or reliability of this software.
//
// Description:
//
//! @file
//  This file contains common data type definitions used throughout the code, utility and programming interface.
//
// Revision History:
//
//  V01.000 07-May-2024 Jonathan D. Belanger
//  Initially written.
//
#pragma once
#include <string>

//
//! @namespace Msg
//! @brief This namespace is used throughout the definitions and code for the Message Programming Interface.
//
namespace Msg
{

    //
    //! @typedef int8
    //! @typedef int16
    //! @typedef int32
    //! @typedef int64
    //! @typedef uint8
    //! @typedef uint16
    //! @typedef uint32
    //! @typedef uint64
    //
    typedef char int8;
    typedef unsigned char uint8;
    typedef short int16;
    typedef unsigned short uint16;
    typedef long int32;
    typedef unsigned long uint32;
    typedef long long int64;
    typedef unsigned long long uint64;

    //
    //! @typedef MsgCode32
    //! @typedef MsgCode64
    //
    typedef uint32 MsgCode32;
    typedef uint64 MsgCode64;

    //
    //! @struct Msg32
    //
    struct Msg32
    {
        public:
            explicit Msg32()
            {
                severity = 0;
                msgNum = 0;
                facNum = 0;
                ctrl = 0;
            }

            explicit Msg32(uint32 msgCode)
            {
                severity = msgCode & 0x00000007;
                msgNum = (msgCode & 0x0000fff8) >> 3;
                facNum = (msgCode & 0x0fff0000) >> 16;
                ctrl = (msgCode &0xf0000000) >> 28;
            }

            uint32
            getSeverity() const
            {
                return severity;
            }

            uint32
            getMessageNumber() const
            {
                return msgNum;
            }

            uint32
            getFacilityNumber() const
            {
                return facNum;
            }

            uint32
            getOutputControl() const
            {
                return ctrl;
            }

            MsgCode64
            get() const
            {
                return (ctrl << 28) + (facNum << 16) + (msgNum << 3) + severity;
            }

            void
            setSeverity(MsgCode32 sev)
            {
                severity = sev;
            }

            void
            setMessageNumber(MsgCode32 _msgNum)
            {
                msgNum = _msgNum;
            }

            void
            setFacilityNumber(MsgCode32 _facNum)
            {
                facNum = _facNum;
            }

            void
            setOutputControl(MsgCode32 outCtrl)
            {
                ctrl = outCtrl;
            }

            MsgCode32 severity : 3;
            MsgCode32 msgNum : 13;
            MsgCode32 facNum : 12;
            MsgCode32 ctrl : 4;
    };

    //
    //! @struct Msg64
    //
    struct Msg64
    {
        public:
            explicit Msg64()
            {
                severity = 0;
                msgNum = 0;
                facNum = 0;
                ctrl = 0;
            }

            explicit Msg64(uint64 msgCode)
            {
                severity = (msgCode & 0x0000000000000007);
                msgNum = (msgCode & 0x00000000fffffff8) >> 3;
                facNum = (msgCode & 0x0fffffff00000000) >> 32;
                ctrl = (msgCode & 0xf000000000000000) >> 56;
            }

            Msg64&
            operator=(const Msg32& msgCode)
            {
                severity = uint64(msgCode.getSeverity());
                msgNum = uint64(msgCode.getMessageNumber());
                facNum = uint64(msgCode.getFacilityNumber());
                ctrl = uint64(msgCode.getOutputControl());
                return *this;
            }

            Msg64&
            operator=(const uint64& msgCode)
            {
                severity = (msgCode & 0x0000000000000007);
                msgNum = (msgCode & 0x00000000fffffff8) >> 3;
                facNum = (msgCode & 0x0fffffff00000000) >> 32;
                ctrl = (msgCode & 0xf000000000000000) >> 56;
                return *this;
            }

            uint64
            getSeverity() const
            {
                return severity;
            }

            uint64
            getMessageNumber() const
            {
                return msgNum;
            }

            uint64
            getFacilityNumber() const
            {
                return facNum;
            }

            uint64
            getOutputControl() const
            {
                return ctrl;
            }

            MsgCode64
            get() const
            {
                return (uint64(ctrl) << 56) + (uint64(facNum) << 32) + (uint64(msgNum) << 3) + uint64(severity);
            }

            void
            setSeverity(uint64 sev)
            {
                severity = sev;
            }

            void
            setMessageNumber(uint64 _msgNum)
            {
                msgNum = _msgNum;
            }

            void
            setFacilityNumber(uint64 _facNum)
            {
                facNum = _facNum;
            }

            void
            setOutputControl(uint64 outCtrl)
            {
                ctrl = outCtrl;
            }

            MsgCode64 severity : 3;
            MsgCode64 msgNum : 29;
            MsgCode64 facNum : 28;
            MsgCode64 ctrl : 4;
    };
}
