//
// Copyright (C) Jonathan D. Belanger 2024.
// All Rights Reserved.
//
// This software is furnished under a license and may be used and copied only in accordance with the terms of such
// license and with the inclusion of the above copyright notice.  This software or any other copies thereof may not be
// provided or otherwise made available to any other person.  No title to and ownership of the software is hereby
// transferred.
//
// The information in this software is subject to change without notice and should not be construed as a commitment by
// the author or co-authors.
//
// The author and any co-authors assume no responsibility for the use or reliability of this software.
//
// Description:
//
//! @file
//  This file contains the code to implement the Message programming interface.  This is the code that a user will call
//  to manage statuses at various severity levels and be able to put a hierarchy of messages onto a status vector for
//  later reporting or processing.
//
// Revision History:
//
//  V01.000 12-May-2024 Jonathan D. Belanger
//  Initially written.
//
#include "MessageClass.hxx"
#include "MessageFao.hxx"
#include <iostream>

//
//! @namespace Msg
//! @brief This namespace is used throughout the definitions and code for the Message Programming Interface.
//
namespace Msg
{
    static char msgSeverityStr[] = {'W', 'S', 'E', 'I', 'F'};
    static char msgPrefixStr[] = {'%', '-'};

    Message* Message::instance = nullptr;

    void
    Message::registerFacility(MsgCode32 facilityNumber, std::string& facilityName, const TableEntry* table)
    {
        auto found = messageTables.find(facilityNumber);

        if (found == messageTables.end())
        {
            FacilityEntry* facEntry = new FacilityEntry{facilityName, table};

            messageTables.emplace(std::make_pair(facilityNumber, facEntry));
        }
    }

    void
    Message::registerFacility(MsgCode64 facilityNumber, std::string& facilityName, const TableEntry* table)
    {
        auto found = messageTables.find(facilityNumber);

        if (found == messageTables.end())
        {
            FacilityEntry* facEntry = new FacilityEntry{facilityName, table};

            messageTables.emplace(std::make_pair(facilityNumber, facEntry));
        }
    }

    void
    Message::clear(MsgVec& msgVec)
    {
        msgVec.clear();
    }

    MsgCode32
    Message::addMessage(MsgCode32 msgCode, MsgVec* msgVec, ...)
    {
        Msg32 messageCode(msgCode);
        auto msgEntry = messageTables.find(messageCode.getFacilityNumber());
        va_list args;

        va_start(args, msgVec);

        //
        // If the message vector has not been initialize, then do so now.
        //
        if (msgVec->size() == 0)
        {
            msgVec->initialize(false);
        }

        //
        // If the message vector has been initialized and we are reporting a success status, then clear all existing
        // entries from the message vector (this will also re-initialize it).
        //
        else if (isSuccess(msgCode))
        {
            msgVec->clear();
        }

        //
        // If we found the message entry for this facility, then we have more to process.
        //
        if (msgEntry != messageTables.end())
        {
            FacilityEntry* facEntry = msgEntry->second;         // Facility entry containing the message table
            const TableEntry* tableEntry = facEntry->second;    // The message table.
            std::size_t position = 0;
            int32 tableIdx = 0;
            Fao::FaoInfo* faoInfo;
            MsgVec::ArgVec argVec;
            MsgVec::Argument* argument = nullptr;
            int8 sByte;
            uint8 uByte;
            int16 sWord;
            uint16 uWord;
            int32 sLong;
            uint32 uLong;
            int64 sQuad;
            uint64 uQuad;

            //
            // Find the message specific message table entry.
            //
            while (tableEntry[tableIdx].msgValue != 0)
            {
                if (tableEntry[tableIdx].msgValue == msgCode)
                {
                    break;
                }
                tableIdx++;
            }

            //
            // Process any FAO arguments that may have been supplied on the call.
            //
            int64 faoWithArgs = tableEntry[tableIdx].faoCount;
            while (faoWithArgs > 0)
            {

                //
                // Get the next (first) FAO directive, if any.
                //
                faoInfo = Fao::getNextFaoTypeIndex(tableEntry[tableIdx].text, &position);
                if (faoInfo != nullptr)
                {
                    switch (faoInfo->faoCmd[1])
                    {

                        //
                        // Strings
                        //
                        case 'A':
                            argument = new MsgVec::Argument();
                            argument->text = std::string(va_arg(args, char*));
                            argument->faoCount = faoInfo->argCount;
                            switch (faoInfo->faoCmd[2])
                            {
                                case 'C':
                                    argument->type = MsgVec::CountedString;
                                    break;

                                case 'D':
                                case 'F':
                                case 'S':
                                    argument->type = MsgVec::LengthString;
                                    break;

                                case 'Z':
                                    argument->type = MsgVec::NullTerminatedString;
                                    break;

                                default:
                                    break;
                            }
                            argVec.push_back(argument);
                            break;

                        //
                        // Unsigned integers
                        //
                        case 'O':
                        case 'X':
                        case 'S':
                            argument = new MsgVec::Argument();
                            argument->faoCount = faoInfo->argCount;
                            switch (faoInfo->faoCmd[2])
                            {
                                case 'B':
                                    argument->argData.unsignedLong = va_arg(args, uint32);
                                    argument->type = MsgVec::UnsignedLong;
                                    break;

                                case 'W':
                                    argument->argData.unsignedLong = va_arg(args, uint32);
                                    argument->type = MsgVec::UnsignedLong;
                                    break;

                                case 'L':
                                    argument->argData.unsignedLong = va_arg(args, uint32);
                                    argument->type = MsgVec::UnsignedLong;
                                    break;

                                case 'A':
                                    if (Address64Bits)
                                    {
                                        argument->argData.unsignedQuad = va_arg(args, uint64);
                                        argument->type = MsgVec::UnsignedQuad;
                                    }
                                    else
                                    {
                                        argument->argData.unsignedLong = va_arg(args, uint32);
                                        argument->type = MsgVec::UnsignedLong;
                                    }
                                    break;

                                case 'I':
                                case 'H':
                                case 'J':
                                case 'Q':
                                    argument->argData.unsignedQuad = va_arg(args, uint64);
                                    argument->type = MsgVec::UnsignedQuad;
                                    break;

                                default:
                                    break;
                            }
                            argVec.push_back(argument);
                            break;

                        //
                        // Signed integers
                        //
                        case 'Z':
                        case 'U':
                            argument = new MsgVec::Argument();
                            argument->faoCount = faoInfo->argCount;
                            switch (faoInfo->faoCmd[2])
                            {
                                case 'B':
                                    argument->argData.signedLong = va_arg(args, int32);
                                    argument->type = MsgVec::SignedLong;
                                    break;

                                case 'W':
                                    argument->argData.signedLong = va_arg(args, int32);
                                    argument->type = MsgVec::SignedLong;
                                    break;

                                case 'L':
                                    argument->argData.signedLong = va_arg(args, int32);
                                    argument->type = MsgVec::SignedLong;
                                    break;

                                case 'A':
                                    if (Address64Bits)
                                    {
                                        argument->argData.signedQuad = va_arg(args, uint64);
                                        argument->type = MsgVec::SignedQuad;
                                    }
                                    else
                                    {
                                        argument->argData.signedLong = va_arg(args, uint32);
                                        argument->type = MsgVec::SignedLong;
                                    }
                                    break;

                                case 'I':
                                case 'H':
                                case 'J':
                                case 'Q':
                                    argument->argData.signedQuad = va_arg(args, int64);
                                    argument->type = MsgVec::SignedQuad;
                                    break;

                                default:
                                    break;
                            }
                            argVec.push_back(argument);
                            break;

                        //
                        // Special directives
                        //
                        case '%':
                            switch (faoInfo->faoCmd[2])
                            {
                                case 'T':
                                case 'D':
                                    argument = new MsgVec::Argument();
                                    argument->faoCount = faoInfo->argCount;
                                    argument->argData.unsignedQuad = va_arg(args, uint64);
                                    argument->type = MsgVec::UnsignedQuad;
                                    argVec.push_back(argument);
                                    break;

                                case 'I':
                                case 'U':
                                    argument = new MsgVec::Argument();
                                    argument->faoCount = faoInfo->argCount;
                                    argument->argData.unsignedLong = va_arg(args, uint32);
                                    argument->type = MsgVec::UnsignedLong;
                                    argVec.push_back(argument);
                                    break;

                                default:
                                    break;
                            }
                            break;

                        default:
                            break;
                    }
                    faoWithArgs -= faoInfo->argCount;
                    position += faoInfo->faoCmd.length();
                }
            }
            va_end(args);

            msgVec->insert(msgCode, argVec);
            argVec.clear();
        }
        return msgCode;
    }

    MsgCode64
    Message::addMessage(MsgCode64 msgCode, MsgVec* msgVec, ...)
    {
        Msg64 messageCode(msgCode);
        auto msgEntry = messageTables.find(messageCode.getFacilityNumber());
        va_list args;

        va_start(args, msgVec);

        //
        // If the message vector has not been initialize, then do so now.
        //
        if (msgVec->size() == 0)
        {
            msgVec->initialize(true);
        }

        //
        // If the message vector has been initialized and we are reporting a success status, then clear all existing
        // entries from the message vector (this will also re-initialize it).
        //
        else if (isSuccess(msgCode))
        {
            msgVec->clear();
        }

        //
        // If we found the message entry for this facility, then we have more to process.
        //
        if (msgEntry != messageTables.end())
        {
            FacilityEntry* facEntry = msgEntry->second;         // Facility entry containing the message table
            const TableEntry* tableEntry = facEntry->second;    // The message table.
            std::size_t position = 0;
            int32 tableIdx = 0;
            Fao::FaoInfo* faoInfo;
            MsgVec::ArgVec argVec;
            MsgVec::Argument* argument = nullptr;
            int8 sByte;
            uint8 uByte;
            int16 sWord;
            uint16 uWord;
            int32 sLong;
            uint32 uLong;
            int64 sQuad;
            uint64 uQuad;

            //
            // Find the message specific message table entry.
            //
            while (tableEntry[tableIdx].msgValue != 0)
            {
                if (tableEntry[tableIdx].msgValue == msgCode)
                {
                    break;
                }
                tableIdx++;
            }

            //
            // Process any FAO arguments that may have been supplied on the call.
            //
            int64 faoWithArgs = tableEntry[tableIdx].faoCount;
            while (faoWithArgs > 0)
            {

                //
                // Get the next (first) FAO directive, if any.
                //
                faoInfo = Fao::getNextFaoTypeIndex(tableEntry[tableIdx].text, &position);
                switch (faoInfo->faoCmd[1])
                {

                    //
                    // Strings
                    //
                    case 'A':
                        argument = new MsgVec::Argument();
                        argument->faoCount = faoInfo->argCount;
                        argument->text = std::string(va_arg(args, char*));
                        switch (faoInfo->faoCmd[2])
                        {
                            case 'C':
                                argument->type = MsgVec::CountedString;
                                break;

                            case 'D':
                            case 'F':
                            case 'S':
                                argument->type = MsgVec::LengthString;
                                break;

                            case 'Z':
                                argument->type = MsgVec::NullTerminatedString;
                                break;

                            default:
                                break;
                        }
                        argVec.push_back(argument);
                        break;

                    //
                    // Unsigned integers
                    //
                    case 'O':
                    case 'X':
                    case 'S':
                        argument = new MsgVec::Argument();
                        argument->faoCount = faoInfo->argCount;
                        switch (faoInfo->faoCmd[2])
                        {
                            case 'B':
                                argument->argData.unsignedLong = va_arg(args, uint32);
                                argument->type = MsgVec::UnsignedLong;
                                break;

                            case 'W':
                                argument->argData.unsignedLong = va_arg(args, uint32);
                                argument->type = MsgVec::UnsignedLong;
                                break;

                            case 'L':
                                argument->argData.unsignedLong = va_arg(args, uint32);
                                argument->type = MsgVec::UnsignedLong;
                                break;

                            case 'A':
                                if (Address64Bits)
                                {
                                    argument->argData.unsignedQuad = va_arg(args, uint64);
                                    argument->type = MsgVec::UnsignedQuad;
                                }
                                else
                                {
                                    argument->argData.unsignedLong = va_arg(args, uint32);
                                    argument->type = MsgVec::UnsignedLong;
                                }
                                break;

                            case 'I':
                            case 'H':
                            case 'J':
                            case 'Q':
                                argument->argData.unsignedQuad = va_arg(args, uint64);
                                argument->type = MsgVec::UnsignedQuad;
                                break;

                            default:
                                break;
                        }
                        argVec.push_back(argument);
                        break;

                    //
                    // Signed integers
                    //
                    case 'Z':
                    case 'U':
                        argument = new MsgVec::Argument();
                        argument->faoCount = faoInfo->argCount;
                        switch (faoInfo->faoCmd[2])
                        {
                            case 'B':
                                argument->argData.signedLong = va_arg(args, int32);
                                argument->type = MsgVec::SignedLong;
                                break;

                            case 'W':
                                argument->argData.signedLong = va_arg(args, int32);
                                argument->type = MsgVec::SignedLong;
                                break;

                            case 'L':
                                argument->argData.signedLong = va_arg(args, int32);
                                argument->type = MsgVec::SignedLong;
                                break;

                            case 'A':
                                if (Address64Bits)
                                {
                                    argument->argData.signedQuad = va_arg(args, uint64);
                                    argument->type = MsgVec::SignedQuad;
                                }
                                else
                                {
                                    argument->argData.signedLong = va_arg(args, uint32);
                                    argument->type = MsgVec::SignedLong;
                                }
                                break;

                            case 'I':
                            case 'H':
                            case 'J':
                            case 'Q':
                                argument->argData.signedQuad = va_arg(args, int64);
                                argument->type = MsgVec::SignedQuad;
                                break;

                            default:
                                break;
                        }
                        argVec.push_back(argument);
                        break;

                    //
                    // Special directives
                    //
                    case '%':
                        switch (faoInfo->faoCmd[2])
                        {
                            case 'T':
                            case 'D':
                                argument = new MsgVec::Argument();
                                argument->faoCount = faoInfo->argCount;
                                argument->argData.unsignedQuad = va_arg(args, uint64);
                                argument->type = MsgVec::UnsignedQuad;
                                argVec.push_back(argument);
                                break;

                            case 'I':
                            case 'U':
                                argument = new MsgVec::Argument();
                                argument->faoCount = faoInfo->argCount;
                                argument->argData.unsignedLong = va_arg(args, uint32);
                                argument->type = MsgVec::UnsignedLong;
                                argVec.push_back(argument);
                                break;

                            default:
                                break;
                        }
                        break;

                    default:
                        break;
                }
                faoWithArgs -= faoInfo->argCount;
                position += faoInfo->faoCmd.length();
            }
            va_end(args);
            msgVec->insert(msgCode, argVec);
            argVec.clear();
        }
        return msgCode;
    }

    std::string&
    Message::getMessage(MsgVec& msgVec)
    {
        static std::string retStr;
        uint32 index = 1;
        bool first = true;
        FacilityEntry* facEntry;
        const TableEntry* tableEntry;
        MsgCode32 severity;

        retStr.clear();
        while (!msgVec.endOfMessages(index))
        {
            if (msgVec.is64Bits())
            {
                MsgCode64 msgCode = msgVec[index];
                Msg64 messageCode(msgCode);
                auto msgEntry = messageTables.find(messageCode.getFacilityNumber());

                severity = messageCode.getSeverity();
                facEntry = msgEntry->second;
                tableEntry = facEntry->second;
                while (tableEntry != nullptr)
                {
                    if (tableEntry->msgValue == msgCode)
                    {
                        break;
                    }
                    tableEntry++;
                    if (tableEntry->msgValue == 0)
                    {
                        tableEntry = nullptr;
                    }
                }
            }
            else
            {
                MsgCode32 msgCode = msgVec[index];
                Msg32 messageCode(msgCode);
                auto msgEntry = messageTables.find(messageCode.getFacilityNumber());

                severity = messageCode.getSeverity();
                facEntry = msgEntry->second;
                tableEntry = facEntry->second;
                while (tableEntry != nullptr)
                {
                    if (tableEntry->msgValue == msgCode)
                    {
                        break;
                    }
                    tableEntry++;
                     if (tableEntry->msgValue == 0)
                     {
                         tableEntry = nullptr;
                     }
                 }
            }
            index += 2;     // Skip over the current message and the fao count and output control information.
            if (tableEntry != nullptr)
            {
                std::string formattedString;

                if (first)
                {
                    retStr += msgPrefixStr[0];
                    first = false;
                }
                else
                {
                    retStr += "\n";
                    retStr += msgPrefixStr[1];
                }
                retStr += facEntry->first + "-" + msgSeverityStr[severity] + "-" + tableEntry->symbol + ", ";
                if (tableEntry->faoCount > 0)
                {
                    retStr += Fao::processFaoString(tableEntry->text, msgVec, &index, Address64Bits);
                }
                else
                {
                    retStr += tableEntry->text;
                }
            }
        }

        return retStr;
    }

    void
    Message::putMessage(MsgVec& msgVec)
    {
        std::cerr << getMessage(msgVec) << std::endl;
    }
}
