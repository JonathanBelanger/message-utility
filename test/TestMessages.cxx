//
// Copyright (C) Jonathan D. Belanger 2024.
// All Rights Reserved.
//
// This software is furnished under a license and may be used and copied only in accordance with the terms of such
// license and with the inclusion of the above copyright notice.  This software or any other copies thereof may not be
// provided or otherwise made available to any other person.  No title to and ownership of the software is hereby
// transferred.
//
// The information in this software is subject to change without notice and should not be construed as a commitment by
// the author or co-authors.
//
// The author and any co-authors assume no responsibility for the use or reliability of this software.
//
// Description:
//
//! @file
//  This file contains the test code to generate status vectors as well as parse the data out into a string.
//
// Revision History:
//
//  V01.000 17-May-2024 Jonathan D. Belanger
//  Initially written.
//
#include "TestMessage1.hxx"
#include "TestMessage5.hxx"
#include "TestMessage6.hxx"
#include <gtest/gtest.h>

TEST(TestMessage, MessageErrors)
{
    Msg::MsgVec statusVec;
    Msg::Message* msgUtil = Msg::TEST::registerMessages();
    std::vector<Msg::uint32>expectedMsgVec =
    {
        0x00000002,
        0x0000fff1,
        0x00000012,
        0x00000001,
        0x00000000,
        0x00000000
    };

    EXPECT_TRUE(Msg::Message::isError(Msg::TEST::MSG_ERRORS));
    EXPECT_FALSE(Msg::Message::isFatal(Msg::TEST::MSG_ERRORS));
    EXPECT_FALSE(Msg::Message::isInformational(Msg::TEST::MSG_ERRORS));
    EXPECT_FALSE(Msg::Message::isSuccess(Msg::TEST::MSG_ERRORS));
    EXPECT_FALSE(Msg::Message::isWarning(Msg::TEST::MSG_ERRORS));
    EXPECT_EQ(Msg::TEST::MSG_ERRORS, msgUtil->addMessage(Msg::TEST::MSG_ERRORS, &statusVec));
    EXPECT_TRUE(statusVec == expectedMsgVec);
}

TEST(TestMessage, MessageSyntax)
{
    Msg::MsgVec statusVec;
    Msg::Message* msgUtil = Msg::TEST::registerMessages();
    std::string foo = "BAR";
    std::vector<Msg::uint32>expectedMsgVec =
    {
        0x00000003,
        0x0000fff1,
        0x0000000a,
        0x00000001,
        0x00000001,
        0x00000000,
        0x00000003,
        0x00524142
    };

    EXPECT_TRUE(Msg::Message::isError(Msg::TEST::MSG_SYNTAX));
    EXPECT_FALSE(Msg::Message::isFatal(Msg::TEST::MSG_SYNTAX));
    EXPECT_FALSE(Msg::Message::isInformational(Msg::TEST::MSG_SYNTAX));
    EXPECT_FALSE(Msg::Message::isSuccess(Msg::TEST::MSG_SYNTAX));
    EXPECT_FALSE(Msg::Message::isWarning(Msg::TEST::MSG_SYNTAX));
    EXPECT_EQ(Msg::TEST::MSG_SYNTAX, msgUtil->addMessage(Msg::TEST::MSG_SYNTAX, &statusVec, foo.c_str()));
    EXPECT_TRUE(statusVec == expectedMsgVec);
}

TEST(TestMessage, ErrorSyntax)
{
    Msg::MsgVec statusVec;
    Msg::Message* msgUtil = Msg::TEST::registerMessages();
    std::string foo = "BAR";
    std::vector<Msg::uint32>expectedMsgVec =
    {
        0x00000005,
        0x0000fff1,
        0x00000012,
        0x00000001,
        0x00000000,
        0x00000000,
        0x0000000a,
        0x00000001,
        0x00000001,
        0x00000000,
        0x00000003,
        0x00524142
    };

    EXPECT_EQ(Msg::TEST::MSG_ERRORS, msgUtil->addMessage(Msg::TEST::MSG_ERRORS, &statusVec));
    EXPECT_EQ(Msg::TEST::MSG_SYNTAX, msgUtil->addMessage(Msg::TEST::MSG_SYNTAX, &statusVec, foo.c_str()));
    EXPECT_TRUE(statusVec == expectedMsgVec);
}

TEST(TestMessage, ErrorsOutput)
{
    Msg::MsgVec statusVec;
    Msg::Message* msgUtil = Msg::TEST::registerMessages();
    std::string expectedMessage = "%TEST-E-ERRORS, Errors encountered during processing";

    statusVec.initialize(true);
    EXPECT_EQ(Msg::TEST::MSG_ERRORS, msgUtil->addMessage(Msg::TEST::MSG_ERRORS, &statusVec));
    EXPECT_TRUE(expectedMessage == msgUtil->getMessage(statusVec));
}

TEST(TestMessage, SyntaxOutput)
{
    Msg::MsgVec statusVec;
    Msg::Message* msgUtil = Msg::TEST::registerMessages();
    std::string foo = "BAR";
    std::string expectedMessage = "%TEST-E-SYNTAX, Syntax error in string BAR";

    EXPECT_EQ(Msg::TEST::MSG_SYNTAX, msgUtil->addMessage(Msg::TEST::MSG_SYNTAX, &statusVec, foo.c_str()));
    EXPECT_TRUE(expectedMessage == msgUtil->getMessage(statusVec));
}

TEST(TestMessage, BothOutput)
{
    Msg::MsgVec statusVec;
    Msg::Message* msgUtil = Msg::TEST::registerMessages();
    std::string foo = "BAR";
    std::string expectedMessage = "%TEST-E-SYNTAX, Syntax error in string BAR\n"
        "-TEST-E-ERRORS, Errors encountered during processing";

    EXPECT_EQ(Msg::TEST::MSG_SYNTAX, msgUtil->addMessage(Msg::TEST::MSG_SYNTAX, &statusVec, foo.c_str()));
    EXPECT_EQ(Msg::TEST::MSG_ERRORS, msgUtil->addMessage(Msg::TEST::MSG_ERRORS, &statusVec));
    EXPECT_TRUE(expectedMessage == msgUtil->getMessage(statusVec));
}

TEST(TestMessage, ErrorThenNormal)
{
    Msg::MsgVec statusVec;
    Msg::Message* msgUtil = Msg::TEST32::registerMessages();
    std::string badKeyword = "SUPERCALIFRAGILISTICEXPIALIDOCIOUS";
    std::vector<Msg::uint32>expectedErrorMsgVec =
    {
        0x0000000c,
        0x00020012,
        0x00000001,
        0x00000022,
        0x45505553,
        0x4c414352,
        0x41524649,
        0x494c4947,
        0x43495453,
        0x49505845,
        0x44494c41,
        0x4f49434f,
        0x00005355
    };
    std::vector<Msg::uint32>expectedNormalMsgVec =
    {
        0x00000002,
        0x00020009,
        0x00000000
    };

    EXPECT_EQ(Msg::TEST32::ABC_UNRECOG, msgUtil->addMessage(Msg::TEST32::ABC_UNRECOG, &statusVec, badKeyword.c_str()));
    EXPECT_TRUE(statusVec == expectedErrorMsgVec);

    EXPECT_EQ(Msg::TEST32::ABC_NORMAL, msgUtil->addMessage(Msg::TEST32::ABC_NORMAL, &statusVec));
    EXPECT_TRUE(statusVec == expectedNormalMsgVec);
}

TEST(TestMessage, OutputFormatting32)
{
    Msg::TEST32::MsgCode statusCode;
    Msg::MsgVec statusVec;
    Msg::Message* msgUtil = Msg::TEST32::registerMessages();
    std::string syntaxStr1 = "Rhode";
    std::string syntaxStr2 = "Island";
    std::string syntaxStr3 = "String with non-printable characters, \x01\x05\x0a\x15";
    void* strAddr = &syntaxStr1;
    std::vector<Msg::uint32>expectedMsgVec1 =
    {
        0x00000006,
        0x00020020,
        0x00000002,
        0x646f6852,
        0x00000065,
        0x6c734906,
        0x00646e61
    };
    std::string expectedMsgStr1 = "%TEST32-W-SYNTAX, Invalid syntax keywords Rhode, Island";
    std::vector<Msg::uint32>expectedMsgVec2 =
    {
        0x00000017,
        0x00020020,
        0x00000002,
        0x646f6852,
        0x00000065,
        0x6c734906,
        0x00646e61,
        0x00020033,
        0x00000004,
        0x00000006,
        0x616c7349,
        0x0000646e,
        0x0000002a,
        0x69727453,
        0x7720676e,
        0x20687469,
        0x2d6e6f6e,
        0x6e697270,
        0x6c626174,
        0x68632065,
        0x63617261,
        0x73726574,
        0x0501202c,
        0x0000150a
    };
    std::string expectedMsgStr2 = "%TEST32-W-SYNTAX, Invalid syntax keywords Rhode, Island\n"
                                  "-TEST32-I-TEST1, STRING: Island - String with non-printable characters, ....";
    std::vector<Msg::uint32>expectedMsgVec3 =
    {
        0x00000026,
        0x00020020,
        0x00000002,
        0x646f6852,
        0x00000065,
        0x6c734906,
        0x00646e61,
        0x00020033,
        0x00000004,
        0x00000006,
        0x616c7349,
        0x0000646e,
        0x0000002a,
        0x69727453,
        0x7720676e,
        0x20687469,
        0x2d6e6f6e,
        0x6e697270,
        0x6c626174,
        0x68632065,
        0x63617261,
        0x73726574,
        0x0501202c,
        0x0000150a,
        0x0002003b,
        0x00000008,
        0x0000004d,
        0x00001e61,
        0x04a2cb71,
        0x01961c24,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c25,
        0x001ba1d9,
        0x01961c26,
        0x001ba1d9,
        0x01961c27,
        0x001ba1d9
    };
    std::vector<Msg::uint32>expectedMsgVec4 =
    {
        0x00000035,
        0x00020020,
        0x00000002,
        0x646f6852,
        0x00000065,
        0x6c734906,
        0x00646e61,
        0x00020033,
        0x00000004,
        0x00000006,
        0x616c7349,
        0x0000646e,
        0x0000002a,
        0x69727453,
        0x7720676e,
        0x20687469,
        0x2d6e6f6e,
        0x6e697270,
        0x6c626174,
        0x68632065,
        0x63617261,
        0x73726574,
        0x0501202c,
        0x0000150a,
        0x0002003b,
        0x00000008,
        0x0000004d,
        0x00001e61,
        0x04a2cb71,
        0x01961c24,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c25,
        0x001ba1d9,
        0x01961c26,
        0x001ba1d9,
        0x01961c27,
        0x001ba1d9,
        0x00020043,
        0x00000008,
        0x0000004d,
        0x00001e61,
        0x04a2cb71,
        0x01961c28,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c29,
        0x001ba1d9,
        0x01961c2a,
        0x001ba1d9,
        0x01961c2b,
        0x001ba1d9
    };
    std::vector<Msg::uint32>expectedMsgVec5 =
    {
        0x00000044,
        0x00020020,
        0x00000002,
        0x646f6852,
        0x00000065,
        0x6c734906,
        0x00646e61,
        0x00020033,
        0x00000004,
        0x00000006,
        0x616c7349,
        0x0000646e,
        0x0000002a,
        0x69727453,
        0x7720676e,
        0x20687469,
        0x2d6e6f6e,
        0x6e697270,
        0x6c626174,
        0x68632065,
        0x63617261,
        0x73726574,
        0x0501202c,
        0x0000150a,
        0x0002003b,
        0x00000008,
        0x0000004d,
        0x00001e61,
        0x04a2cb71,
        0x01961c24,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c25,
        0x001ba1d9,
        0x01961c26,
        0x001ba1d9,
        0x01961c27,
        0x001ba1d9,
        0x00020043,
        0x00000008,
        0x0000004d,
        0x00001e61,
        0x04a2cb71,
        0x01961c28,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c29,
        0x001ba1d9,
        0x01961c2a,
        0x001ba1d9,
        0x01961c2b,
        0x001ba1d9,
        0x0002004b,
        0x00000008,
        0x0000004d,
        0x00001e61,
        0x04a2cb71,
        0x01961c2c,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c2d,
        0x001ba1d9,
        0x01961c2e,
        0x001ba1d9,
        0x01961c2f,
        0x001ba1d9
    };
    std::vector<Msg::uint32>expectedMsgVec6 =
    {
        0x00000053,
        0x00020020,
        0x00000002,
        0x646f6852,
        0x00000065,
        0x6c734906,
        0x00646e61,
        0x00020033,
        0x00000004,
        0x00000006,
        0x616c7349,
        0x0000646e,
        0x0000002a,
        0x69727453,
        0x7720676e,
        0x20687469,
        0x2d6e6f6e,
        0x6e697270,
        0x6c626174,
        0x68632065,
        0x63617261,
        0x73726574,
        0x0501202c,
        0x0000150a,
        0x0002003b,
        0x00000008,
        0x0000004d,
        0x00001e61,
        0x04a2cb71,
        0x01961c24,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c25,
        0x001ba1d9,
        0x01961c26,
        0x001ba1d9,
        0x01961c27,
        0x001ba1d9,
        0x00020043,
        0x00000008,
        0x0000004d,
        0x00001e61,
        0x04a2cb71,
        0x01961c28,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c29,
        0x001ba1d9,
        0x01961c2a,
        0x001ba1d9,
        0x01961c2b,
        0x001ba1d9,
        0x0002004b,
        0x00000008,
        0x0000004d,
        0x00001e61,
        0x04a2cb71,
        0x01961c2c,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c2d,
        0x001ba1d9,
        0x01961c2e,
        0x001ba1d9,
        0x01961c2f,
        0x001ba1d9,
        0x00020053,
        0x00000008,
        0x0000004d,
        0x00001e61,
        0x04a2cb71,
        0x01961c30,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c31,
        0x001ba1d9,
        0x01961c32,
        0x001ba1d9,
        0x01961c33,
        0x001ba1d9
    };
    std::vector<Msg::uint32>expectedMsgVec7 =
    {
        0x00000062,
        0x00020020,
        0x00000002,
        0x646f6852,
        0x00000065,
        0x6c734906,
        0x00646e61,
        0x00020033,
        0x00000004,
        0x00000006,
        0x616c7349,
        0x0000646e,
        0x0000002a,
        0x69727453,
        0x7720676e,
        0x20687469,
        0x2d6e6f6e,
        0x6e697270,
        0x6c626174,
        0x68632065,
        0x63617261,
        0x73726574,
        0x0501202c,
        0x0000150a,
        0x0002003b,
        0x00000008,
        0x0000004d,
        0x00001e61,
        0x04a2cb71,
        0x01961c24,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c25,
        0x001ba1d9,
        0x01961c26,
        0x001ba1d9,
        0x01961c27,
        0x001ba1d9,
        0x00020043,
        0x00000008,
        0x0000004d,
        0x00001e61,
        0x04a2cb71,
        0x01961c28,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c29,
        0x001ba1d9,
        0x01961c2a,
        0x001ba1d9,
        0x01961c2b,
        0x001ba1d9,
        0x0002004b,
        0x00000008,
        0x0000004d,
        0x00001e61,
        0x04a2cb71,
        0x01961c2c,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c2d,
        0x001ba1d9,
        0x01961c2e,
        0x001ba1d9,
        0x01961c2f,
        0x001ba1d9,
        0x00020053,
        0x00000008,
        0x0000004d,
        0x00001e61,
        0x04a2cb71,
        0x01961c30,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c31,
        0x001ba1d9,
        0x01961c32,
        0x001ba1d9,
        0x01961c33,
        0x001ba1d9,
        0x0002005b,
        0x00000008,
        0x0000004d,
        0x00001e61,
        0x04a2cb71,
        0x01961c34,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c35,
        0x001ba1d9,
        0x01961c36,
        0x001ba1d9,
        0x01961c37,
        0x001ba1d9
    };
    std::vector<Msg::uint32>expectedMsgVec8 =
    {
        0x0000006b,
        0x00020020,
        0x00000002,
        0x646f6852,
        0x00000065,
        0x6c734906,
        0x00646e61,
        0x00020033,
        0x00000004,
        0x00000006,
        0x616c7349,
        0x0000646e,
        0x0000002a,
        0x69727453,
        0x7720676e,
        0x20687469,
        0x2d6e6f6e,
        0x6e697270,
        0x6c626174,
        0x68632065,
        0x63617261,
        0x73726574,
        0x0501202c,
        0x0000150a,
        0x0002003b,
        0x00000008,
        0x0000004d,
        0x00001e61,
        0x04a2cb71,
        0x01961c24,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c25,
        0x001ba1d9,
        0x01961c26,
        0x001ba1d9,
        0x01961c27,
        0x001ba1d9,
        0x00020043,
        0x00000008,
        0x0000004d,
        0x00001e61,
        0x04a2cb71,
        0x01961c28,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c29,
        0x001ba1d9,
        0x01961c2a,
        0x001ba1d9,
        0x01961c2b,
        0x001ba1d9,
        0x0002004b,
        0x00000008,
        0x0000004d,
        0x00001e61,
        0x04a2cb71,
        0x01961c2c,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c2d,
        0x001ba1d9,
        0x01961c2e,
        0x001ba1d9,
        0x01961c2f,
        0x001ba1d9,
        0x00020053,
        0x00000008,
        0x0000004d,
        0x00001e61,
        0x04a2cb71,
        0x01961c30,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c31,
        0x001ba1d9,
        0x01961c32,
        0x001ba1d9,
        0x01961c33,
        0x001ba1d9,
        0x0002005b,
        0x00000008,
        0x0000004d,
        0x00001e61,
        0x04a2cb71,
        0x01961c34,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c35,
        0x001ba1d9,
        0x01961c36,
        0x001ba1d9,
        0x01961c37,
        0x001ba1d9,
        0x00020063,
        0x00000005,
        0x00001e61,
        0x04a2cb71,
        0x00000000,
        0x00000000,
        0x00000000,
        0x00000000,
        0x00000002
    };

    statusVec.initialize(false);
    EXPECT_EQ(0, statusVec[1]);

    statusCode = msgUtil->addMessage(Msg::TEST32::ABC_SYNTAX, &statusVec, syntaxStr1.c_str(), syntaxStr2.c_str());
    EXPECT_EQ(statusCode, Msg::TEST32::ABC_SYNTAX);
    EXPECT_TRUE(expectedMsgVec1 == statusVec);
    EXPECT_TRUE(Msg::Message::isWarning(statusCode));
    EXPECT_FALSE(Msg::Message::isSuccess(statusCode));
    EXPECT_FALSE(Msg::Message::isError(statusCode));
    EXPECT_FALSE(Msg::Message::isInformational(statusCode));
    EXPECT_FALSE(Msg::Message::isFatal(statusCode));
    EXPECT_TRUE(expectedMsgStr1 == msgUtil->getMessage(statusVec));

    statusCode = msgUtil->addMessage(Msg::TEST32::ABC_TEST1, &statusVec, syntaxStr2.c_str(), syntaxStr3.c_str());
    EXPECT_EQ(statusCode, Msg::TEST32::ABC_TEST1);
    EXPECT_TRUE(expectedMsgVec2 == statusVec);
    EXPECT_FALSE(Msg::Message::isWarning(statusCode));
    EXPECT_FALSE(Msg::Message::isSuccess(statusCode));
    EXPECT_FALSE(Msg::Message::isError(statusCode));
    EXPECT_TRUE(Msg::Message::isInformational(statusCode));
    EXPECT_FALSE(Msg::Message::isFatal(statusCode));
    EXPECT_TRUE(expectedMsgStr2 == msgUtil->getMessage(statusVec));

    EXPECT_EQ(Msg::TEST32::ABC_TEST2, msgUtil->addMessage(Msg::TEST32::ABC_TEST2,
                                                          &statusVec,
                                                          77,
                                                          7777,
                                                          77777777,
                                                          7777777777777700,
                                                          strAddr,
                                                          7777777777777701,
                                                          7777777777777702,
                                                          7777777777777703));
    EXPECT_TRUE(expectedMsgVec3 == statusVec);

    EXPECT_EQ(Msg::TEST32::ABC_TEST3, msgUtil->addMessage(Msg::TEST32::ABC_TEST3,
                                                          &statusVec,
                                                          77,
                                                          7777,
                                                          77777777,
                                                          7777777777777704,
                                                          strAddr,
                                                          7777777777777705,
                                                          7777777777777706,
                                                          7777777777777707));
    EXPECT_TRUE(expectedMsgVec4 == statusVec);

    EXPECT_EQ(Msg::TEST32::ABC_TEST4, msgUtil->addMessage(Msg::TEST32::ABC_TEST4,
                                                          &statusVec,
                                                          77,
                                                          7777,
                                                          77777777,
                                                          7777777777777708,
                                                          strAddr,
                                                          7777777777777709,
                                                          7777777777777710,
                                                          7777777777777711));
    EXPECT_TRUE(expectedMsgVec5 == statusVec);

    EXPECT_EQ(Msg::TEST32::ABC_TEST5, msgUtil->addMessage(Msg::TEST32::ABC_TEST5,
                                                          &statusVec,
                                                          77,
                                                          7777,
                                                          77777777,
                                                          7777777777777712,
                                                          strAddr,
                                                          7777777777777713,
                                                          7777777777777714,
                                                          7777777777777715));
    EXPECT_TRUE(expectedMsgVec6 == statusVec);

    EXPECT_EQ(Msg::TEST32::ABC_TEST6, msgUtil->addMessage(Msg::TEST32::ABC_TEST6,
                                                          &statusVec,
                                                          77,
                                                          7777,
                                                          77777777,
                                                          7777777777777716,
                                                          strAddr,
                                                          7777777777777717,
                                                          7777777777777718,
                                                          7777777777777719));
    EXPECT_TRUE(expectedMsgVec7 == statusVec);

    EXPECT_EQ(Msg::TEST32::ABC_TEST7, msgUtil->addMessage(Msg::TEST32::ABC_TEST7, &statusVec, 7777, 77777777, 0, 0, 2));
    EXPECT_TRUE(expectedMsgVec8 == statusVec);
}

TEST(TestMessage, OutputFormatting64)
{
    Msg::TEST64::MsgCode statusCode;
    Msg::MsgVec statusVec;
    Msg::Message* msgUtil = Msg::TEST64::registerMessages();
    std::string syntaxStr1 = "Rhode";
    std::string syntaxStr2 = "Island";
    std::string syntaxStr3 = "String with non-printable characters, \x01\x05\x0a\x15";
    void* strAddr = &syntaxStr1;
    std::vector<Msg::uint32>expectedMsgVec1 =
    {
        0x00000004,
        0x0000fff1,
        0x00000020,
        0x00000003,
        0x00000002,
        0x00000000,
        0x646f6852,
        0x00000065,
        0x6c734906,
        0x00646e61
    };
    std::string expectedMsgStr1 = "%TEST64-W-SYNTAX, Invalid syntax keywords Rhode, Island";
    std::vector<Msg::uint32>expectedMsgVec2 =
    {
        0x0000000e,
        0x0000fff1,
        0x00000020,
        0x00000003,
        0x00000002,
        0x00000000,
        0x646f6852,
        0x00000065,
        0x6c734906,
        0x00646e61,
        0x00000033,
        0x00000003,
        0x00000004,
        0x00000000,
        0x00000006,
        0x616c7349,
        0x0000646e,
        0x00000000,
        0x0000002a,
        0x69727453,
        0x7720676e,
        0x20687469,
        0x2d6e6f6e,
        0x6e697270,
        0x6c626174,
        0x68632065,
        0x63617261,
        0x73726574,
        0x0501202c,
        0x0000150a
    };
    std::string expectedMsgStr2 = "%TEST64-W-SYNTAX, Invalid syntax keywords Rhode, Island\n"
                                  "-TEST64-I-TEST1, STRING: Island - String with non-printable characters, ....";
    std::vector<Msg::uint32>expectedMsgVec3 =
    {
        0x00000018,
        0x0000fff1,
        0x00000020,
        0x00000003,
        0x00000002,
        0x00000000,
        0x646f6852,
        0x00000065,
        0x6c734906,
        0x00646e61,
        0x00000033,
        0x00000003,
        0x00000004,
        0x00000000,
        0x00000006,
        0x616c7349,
        0x0000646e,
        0x00000000,
        0x0000002a,
        0x69727453,
        0x7720676e,
        0x20687469,
        0x2d6e6f6e,
        0x6e697270,
        0x6c626174,
        0x68632065,
        0x63617261,
        0x73726574,
        0x0501202c,
        0x0000150a,
        0x0000003b,
        0x00000003,
        0x00000008,
        0x00000000,
        0x0000004d,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x01961c24,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c25,
        0x001ba1d9,
        0x01961c26,
        0x001ba1d9,
        0x01961c27,
        0x001ba1d9
    };
    std::vector<Msg::uint32>expectedMsgVec4 =
    {
        0x00000022,
        0x0000fff1,
        0x00000020,
        0x00000003,
        0x00000002,
        0x00000000,
        0x646f6852,
        0x00000065,
        0x6c734906,
        0x00646e61,
        0x00000033,
        0x00000003,
        0x00000004,
        0x00000000,
        0x00000006,
        0x616c7349,
        0x0000646e,
        0x00000000,
        0x0000002a,
        0x69727453,
        0x7720676e,
        0x20687469,
        0x2d6e6f6e,
        0x6e697270,
        0x6c626174,
        0x68632065,
        0x63617261,
        0x73726574,
        0x0501202c,
        0x0000150a,
        0x0000003b,
        0x00000003,
        0x00000008,
        0x00000000,
        0x0000004d,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x01961c24,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c25,
        0x001ba1d9,
        0x01961c26,
        0x001ba1d9,
        0x01961c27,
        0x001ba1d9,
        0x00000043,
        0x00000003,
        0x00000008,
        0x00000000,
        0x0000004d,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x01961c28,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c29,
        0x001ba1d9,
        0x01961c2a,
        0x001ba1d9,
        0x01961c2b,
        0x001ba1d9
    };
    std::vector<Msg::uint32>expectedMsgVec5 =
    {
        0x0000002c,
        0x0000fff1,
        0x00000020,
        0x00000003,
        0x00000002,
        0x00000000,
        0x646f6852,
        0x00000065,
        0x6c734906,
        0x00646e61,
        0x00000033,
        0x00000003,
        0x00000004,
        0x00000000,
        0x00000006,
        0x616c7349,
        0x0000646e,
        0x00000000,
        0x0000002a,
        0x69727453,
        0x7720676e,
        0x20687469,
        0x2d6e6f6e,
        0x6e697270,
        0x6c626174,
        0x68632065,
        0x63617261,
        0x73726574,
        0x0501202c,
        0x0000150a,
        0x0000003b,
        0x00000003,
        0x00000008,
        0x00000000,
        0x0000004d,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x01961c24,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c25,
        0x001ba1d9,
        0x01961c26,
        0x001ba1d9,
        0x01961c27,
        0x001ba1d9,
        0x00000043,
        0x00000003,
        0x00000008,
        0x00000000,
        0x0000004d,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x01961c28,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c29,
        0x001ba1d9,
        0x01961c2a,
        0x001ba1d9,
        0x01961c2b,
        0x001ba1d9,
        0x0000004b,
        0x00000003,
        0x00000008,
        0x00000000,
        0x0000004d,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x01961c2c,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c2d,
        0x001ba1d9,
        0x01961c2e,
        0x001ba1d9,
        0x01961c2f,
        0x001ba1d9
    };
    std::vector<Msg::uint32>expectedMsgVec6 =
    {
        0x00000036,
        0x0000fff1,
        0x00000020,
        0x00000003,
        0x00000002,
        0x00000000,
        0x646f6852,
        0x00000065,
        0x6c734906,
        0x00646e61,
        0x00000033,
        0x00000003,
        0x00000004,
        0x00000000,
        0x00000006,
        0x616c7349,
        0x0000646e,
        0x00000000,
        0x0000002a,
        0x69727453,
        0x7720676e,
        0x20687469,
        0x2d6e6f6e,
        0x6e697270,
        0x6c626174,
        0x68632065,
        0x63617261,
        0x73726574,
        0x0501202c,
        0x0000150a,
        0x0000003b,
        0x00000003,
        0x00000008,
        0x00000000,
        0x0000004d,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x01961c24,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c25,
        0x001ba1d9,
        0x01961c26,
        0x001ba1d9,
        0x01961c27,
        0x001ba1d9,
        0x00000043,
        0x00000003,
        0x00000008,
        0x00000000,
        0x0000004d,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x01961c28,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c29,
        0x001ba1d9,
        0x01961c2a,
        0x001ba1d9,
        0x01961c2b,
        0x001ba1d9,
        0x0000004b,
        0x00000003,
        0x00000008,
        0x00000000,
        0x0000004d,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x01961c2c,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c2d,
        0x001ba1d9,
        0x01961c2e,
        0x001ba1d9,
        0x01961c2f,
        0x001ba1d9,
        0x00000053,
        0x00000003,
        0x00000008,
        0x00000000,
        0x0000004d,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x01961c30,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c31,
        0x001ba1d9,
        0x01961c32,
        0x001ba1d9,
        0x01961c33,
        0x001ba1d9
    };
    std::vector<Msg::uint32>expectedMsgVec7 =
    {
        0x00000040,
        0x0000fff1,
        0x00000020,
        0x00000003,
        0x00000002,
        0x00000000,
        0x646f6852,
        0x00000065,
        0x6c734906,
        0x00646e61,
        0x00000033,
        0x00000003,
        0x00000004,
        0x00000000,
        0x00000006,
        0x616c7349,
        0x0000646e,
        0x00000000,
        0x0000002a,
        0x69727453,
        0x7720676e,
        0x20687469,
        0x2d6e6f6e,
        0x6e697270,
        0x6c626174,
        0x68632065,
        0x63617261,
        0x73726574,
        0x0501202c,
        0x0000150a,
        0x0000003b,
        0x00000003,
        0x00000008,
        0x00000000,
        0x0000004d,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x01961c24,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c25,
        0x001ba1d9,
        0x01961c26,
        0x001ba1d9,
        0x01961c27,
        0x001ba1d9,
        0x00000043,
        0x00000003,
        0x00000008,
        0x00000000,
        0x0000004d,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x01961c28,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c29,
        0x001ba1d9,
        0x01961c2a,
        0x001ba1d9,
        0x01961c2b,
        0x001ba1d9,
        0x0000004b,
        0x00000003,
        0x00000008,
        0x00000000,
        0x0000004d,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x01961c2c,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c2d,
        0x001ba1d9,
        0x01961c2e,
        0x001ba1d9,
        0x01961c2f,
        0x001ba1d9,
        0x00000053,
        0x00000003,
        0x00000008,
        0x00000000,
        0x0000004d,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x01961c30,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c31,
        0x001ba1d9,
        0x01961c32,
        0x001ba1d9,
        0x01961c33,
        0x001ba1d9,
        0x0000005b,
        0x00000003,
        0x00000008,
        0x00000000,
        0x0000004d,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x01961c34,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c35,
        0x001ba1d9,
        0x01961c36,
        0x001ba1d9,
        0x01961c37,
        0x001ba1d9
    };
    std::vector<Msg::uint32>expectedMsgVec8 =
    {
        0x00000047,
        0x0000fff1,
        0x00000020,
        0x00000003,
        0x00000002,
        0x00000000,
        0x646f6852,
        0x00000065,
        0x6c734906,
        0x00646e61,
        0x00000033,
        0x00000003,
        0x00000004,
        0x00000000,
        0x00000006,
        0x616c7349,
        0x0000646e,
        0x00000000,
        0x0000002a,
        0x69727453,
        0x7720676e,
        0x20687469,
        0x2d6e6f6e,
        0x6e697270,
        0x6c626174,
        0x68632065,
        0x63617261,
        0x73726574,
        0x0501202c,
        0x0000150a,
        0x0000003b,
        0x00000003,
        0x00000008,
        0x00000000,
        0x0000004d,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x01961c24,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c25,
        0x001ba1d9,
        0x01961c26,
        0x001ba1d9,
        0x01961c27,
        0x001ba1d9,
        0x00000043,
        0x00000003,
        0x00000008,
        0x00000000,
        0x0000004d,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x01961c28,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c29,
        0x001ba1d9,
        0x01961c2a,
        0x001ba1d9,
        0x01961c2b,
        0x001ba1d9,
        0x0000004b,
        0x00000003,
        0x00000008,
        0x00000000,
        0x0000004d,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x01961c2c,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c2d,
        0x001ba1d9,
        0x01961c2e,
        0x001ba1d9,
        0x01961c2f,
        0x001ba1d9,
        0x00000053,
        0x00000003,
        0x00000008,
        0x00000000,
        0x0000004d,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x01961c30,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c31,
        0x001ba1d9,
        0x01961c32,
        0x001ba1d9,
        0x01961c33,
        0x001ba1d9,
        0x0000005b,
        0x00000003,
        0x00000008,
        0x00000000,
        0x0000004d,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x01961c34,
        0x001ba1d9,
        0xdeadbeef,     // ignore in comparison
        0xdeadbeef,     // ignore in comparison
        0x01961c35,
        0x001ba1d9,
        0x01961c36,
        0x001ba1d9,
        0x01961c37,
        0x001ba1d9,
        0x00000063,
        0x00000003,
        0x00000005,
        0x00000000,
        0x00001e61,
        0x00000000,
        0x04a2cb71,
        0x00000000,
        0x00000000,
        0x00000000,
        0x00000000,
        0x00000000,
        0x00000002,
        0x00000000
    };

    statusVec.initialize(false);
    EXPECT_EQ(0, statusVec[1]);

    statusCode = msgUtil->addMessage(Msg::TEST64::ABC_SYNTAX, &statusVec, syntaxStr1.c_str(), syntaxStr2.c_str());
    EXPECT_EQ(statusCode, Msg::TEST64::ABC_SYNTAX);
    EXPECT_TRUE(expectedMsgVec1 == statusVec);
    EXPECT_TRUE(Msg::Message::isWarning(statusCode));
    EXPECT_FALSE(Msg::Message::isSuccess(statusCode));
    EXPECT_FALSE(Msg::Message::isError(statusCode));
    EXPECT_FALSE(Msg::Message::isInformational(statusCode));
    EXPECT_FALSE(Msg::Message::isFatal(statusCode));
    EXPECT_TRUE(expectedMsgStr1 == msgUtil->getMessage(statusVec));

    statusCode = msgUtil->addMessage(Msg::TEST64::ABC_TEST1, &statusVec, syntaxStr2.c_str(), syntaxStr3.c_str());
    EXPECT_EQ(statusCode, Msg::TEST64::ABC_TEST1);
    EXPECT_TRUE(expectedMsgVec2 == statusVec);
    EXPECT_FALSE(Msg::Message::isWarning(statusCode));
    EXPECT_FALSE(Msg::Message::isSuccess(statusCode));
    EXPECT_FALSE(Msg::Message::isError(statusCode));
    EXPECT_TRUE(Msg::Message::isInformational(statusCode));
    EXPECT_FALSE(Msg::Message::isFatal(statusCode));
    EXPECT_TRUE(expectedMsgStr2 == msgUtil->getMessage(statusVec));

    EXPECT_EQ(Msg::TEST64::ABC_TEST2, msgUtil->addMessage(Msg::TEST64::ABC_TEST2,
                                                          &statusVec,
                                                          77,
                                                          7777,
                                                          77777777,
                                                          7777777777777700,
                                                          strAddr,
                                                          7777777777777701,
                                                          7777777777777702,
                                                          7777777777777703));
    EXPECT_TRUE(expectedMsgVec3 == statusVec);

    EXPECT_EQ(Msg::TEST64::ABC_TEST3, msgUtil->addMessage(Msg::TEST64::ABC_TEST3,
                                                          &statusVec,
                                                          77,
                                                          7777,
                                                          77777777,
                                                          7777777777777704,
                                                          strAddr,
                                                          7777777777777705,
                                                          7777777777777706,
                                                          7777777777777707));
    EXPECT_TRUE(expectedMsgVec4 == statusVec);

    EXPECT_EQ(Msg::TEST64::ABC_TEST4, msgUtil->addMessage(Msg::TEST64::ABC_TEST4,
                                                          &statusVec,
                                                          77,
                                                          7777,
                                                          77777777,
                                                          7777777777777708,
                                                          strAddr,
                                                          7777777777777709,
                                                          7777777777777710,
                                                          7777777777777711));
    EXPECT_TRUE(expectedMsgVec5 == statusVec);

    EXPECT_EQ(Msg::TEST64::ABC_TEST5, msgUtil->addMessage(Msg::TEST64::ABC_TEST5,
                                                          &statusVec,
                                                          77,
                                                          7777,
                                                          77777777,
                                                          7777777777777712,
                                                          strAddr,
                                                          7777777777777713,
                                                          7777777777777714,
                                                          7777777777777715));
    EXPECT_TRUE(expectedMsgVec6 == statusVec);

    EXPECT_EQ(Msg::TEST64::ABC_TEST6, msgUtil->addMessage(Msg::TEST64::ABC_TEST6,
                                                          &statusVec,
                                                          77,
                                                          7777,
                                                          77777777,
                                                          7777777777777716,
                                                          strAddr,
                                                          7777777777777717,
                                                          7777777777777718,
                                                          7777777777777719));
    EXPECT_TRUE(expectedMsgVec7 == statusVec);

    EXPECT_EQ(Msg::TEST64::ABC_TEST7, msgUtil->addMessage(Msg::TEST64::ABC_TEST7, &statusVec, 7777, 77777777, 0, 0, 2));
    EXPECT_TRUE(expectedMsgVec8 == statusVec);
}

int
main(int argc, char**argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
