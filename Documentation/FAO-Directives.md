
# FAO Directive

## Description

### Directives for Character String Substitution

| Directive | Description |
| :---------: | :----------- |
| !AC | Inserts a counted ASCII string. It requires one parameter: the address of the string to be inserted. The first byte of the string must contain the length (in characters) of the string. |
| !AD | Inserts an ASCII string. It requires two parameters: the length of the string and the address of the string. Each of these parameters is a separate argument. |
| !AF | Inserts an ASCII string and replaces all nonprintable ASCII codes with periods (.). It requires two parameters: the length of the string and the address of the string. Each of these parameters is a separate argument. |
| !AS | Inserts an ASCID string. It requires one parameter: the address of a character string descriptor pointing to the string. $FAO assumes that the descriptor is a CLASS_S (static) or CLASS_D (dynamic) string descriptor. Other descriptor types might give incorrect results. |
| !AZ | Inserts a zero-terminated (ASCIZ) string. It requires one parameter: the address of a zero-terminated string. |

### Directives for Zero-Filled Numeric Conversion

| Directive | Description |
| :---------: | :----------- |
| !OB | Converts a byte value to the ASCII representation of the value's octal equivalent. It requires one parameter: the value to be converted. $FAO uses only the low-order byte of the longword parameter. |
| !OW | Converts a word value to the ASCII representation of the value's octal equivalent. It requires one parameter: the value to be converted. $FAO uses only the low-order word of the longword parameter. |
| !OL | Converts a longword value to the ASCII representation of the value's octal equivalent. It requires one parameter: the value to be converted. |
| !OQ | Converts on Alpha and Integrity server systems a quadword to the ASCII representation of its octal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 64-bit sign-extended value is written to the output buffer. It receives one parameter: the address of the value to be converted. This directive cannot be used from DCL. |
| !OA | Converts an address to the ASCII representation of its octal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 32-bit sign-extended value is written to the output buffer. It receives one parameter: the address of the value to be converted. |
| !OI | Converts an integer to the ASCII representation of its octal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 32-bit sign-extended value is written to the output buffer. It receives one parameter: the address of the value to be converted. |
| !OH | Converts an address to the ASCII representation of its octal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 64-bit sign-extended value is written to the output buffer. It receives one parameter: the address of the value to be converted. |
| !OJ | Converts an integer to the ASCII representation of its octal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 64-bit sign-extended value is written to the output buffer. It receives one parameter: the address of the value to be converted. |
| !XB | Converts a byte value to the ASCII representation of the value's hexadecimal equivalent. It requires one parameter: the value to be converted. $FAO uses only the low-order byte of the longword parameter. |
| !XW | Converts a word value to the ASCII representation of the value's hexadecimal equivalent. It requires one parameter: the value to be converted. $FAO uses only the low-order word of the longword parameter. |
| !XL | Converts a longword value to the ASCII representation of the value's hexadecimal equivalent. It requires one parameter: the value to be converted. |
| !XQ | Converts a quadword to the ASCII representation of its hexadecimal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 64-bit sign-extended value is written to the output buffer. |
| !XA | Converts an address to the ASCII representation of its hexadecimal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 32-bit sign-extended value is written to the output buffer. It receives one parameter: the address of the value to be converted. |
| !XI | Converts an integer to the ASCII representation of its hexadecimal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 32-bit sign-extended value is written to the output buffer. It receives one parameter: the address of the value to be converted. |
| !XH | Converts an address to the ASCII representation of its hexadecimal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 64-bit sign-extended value is written to the output buffer. It receives one parameter: the address of the value to be converted. |
| !XJ | Converts an integer to the ASCII representation of its hexadecimal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 64-bit sign-extended value is written to the output buffer. It receives one parameter: the address of the value to be converted. |
| !ZB | Converts an unsigned byte value to the ASCII representation of the value's decimal equivalent. It requires one parameter: the value to be converted. $FAO uses only the low-order byte of the longword parameter. |
| !ZW | Converts an unsigned word value to the ASCII representation of the value's decimal equivalent. It requires one parameter: the value to be converted. $FAO uses only the low-order word of the longword parameter. |
| !ZL | Converts an unsigned longword value to the ASCII representation of the value's decimal equivalent. It requires one parameter: the value to be converted. |
| !ZQ | Converts an unsigned quadword to the ASCII representation of its decimal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 64-bit zero-extended value is written to the output buffer. It receives one parameter: the address of the value to be converted. |
| !ZA | Converts an unsigned address to the ASCII representation of its decimal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 32-bit value is written to the output buffer. It receives one parameter: the address of the value to be converted. |
| !ZI | Converts an unsigned integer to the ASCII representation of its decimal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 32-bit value is written to the output buffer. It receives one parameter: the address of the value to be converted. |
| !ZH | Converts an unsigned address to the ASCII representation of its decimal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 64-bit zero-extended value is written to the output buffer. It receives one parameter: the address of the value to be converted. |
| !ZJ | Converts an unsigned integer to the ASCII representation of its decimal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 64-bit zero-extended value is written to the output buffer. It receives one parameter: the address of the value to be converted. |

### Directives for Blank-Filled Numeric Conversion

| Directive | Description |
| :---------: | :----------- |
| !UB | Converts an unsigned byte value to the ASCII representation of the value's decimal equivalent. It requires one parameter: the value to be converted. $FAO uses only the low-order byte of the longword parameter. |
| !UW | Converts an unsigned word value to the ASCII representation of the value's decimal equivalent. It requires one parameter: the value to be converted. $FAO uses only the low-order word of the longword parameter. |
| !UL | Converts an unsigned longword value to the ASCII representation of the value's decimal equivalent. It requires one parameter: the value to be converted. |
| !UQ | Converts an unsigned quadword to the ASCII representation of its decimal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 64-bit value is written to the output buffer. It receives one parameter: the address of the value to be converted. |
| !UA | Converts an unsigned address to the ASCII representation of its decimal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 32-bit value is written to the output buffer. It receives one parameter: the address of the value to be converted. |
| !UI | Converts an unsigned integer to the ASCII representation of its decimal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 32-bit value is written to the output buffer. It receives one parameter: the address of the value to be converted. |
| !UH | Converts an unsigned address to the ASCII representation of its decimal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 64-bit value is written to the output buffer. It receives one parameter: the address of the value to be converted. |
| !UJ | Converts an unsigned integer to the ASCII representation of its decimal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 64-bit value is written to the output buffer. It receives one parameter: the address of the value to be converted. |
| !SB | Converts a signed byte value to the ASCII representation of the value's decimal equivalent. It requires one parameter: the value to be converted. $FAO uses only the low-order byte of the longword parameter. |
| !SW | Converts a signed word value to the ASCII representation of the value's decimal equivalent. It requires one parameter: the value to be converted. $FAO uses only the low-order word of the longword parameter. |
| !SL | Converts a signed longword value to the ASCII representation of the value's decimal equivalent. It requires one parameter: the value to be converted. |
| !SQ | Converts on Alpha and Integrity server systems a signed quadword to the ASCII representation of its decimal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 32-bit value is written to the output buffer. It receives one parameter: the address of the value to be converted. This directive cannot be used from DCL. |
| !SA | Converts a signed address to the ASCII representation of its decimal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 32-bit value is written to the output buffer. It receives one parameter: the address of the value to be converted. |
| !SI | Converts a signed integer to the ASCII representation of its equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 32-bit value is written to the output buffer. It receives one parameter: the address of the value to be converted. |
| !SH | Converts a signed address to the ASCII representation of its decimal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 32-bit value is written to the output buffer. It receives one parameter: the address of the value to be converted. |
| !SJ | Converts a signed integer to the ASCII representation of its decimal equivalent. Must use the indirect directive @ to output the quadword value for $FAOL; otherwise, a 32-bit value is written to the output buffer. It receives one parameter: the address of the value to be converted. |

### Directives for Output String Formatting

| Directive | Description |
| :---------: | :----------- |
| !/ | Inserts a new line, that is, a carriage return and line feed. It takes no parameters. |
| !_ | Inserts a tab. It takes no parameters. |
| !^ | Inserts a form feed. It takes no parameters. |
| !! | Inserts an exclamation point. It takes no parameters. |
| !%S | Inserts the letter S if the most recently converted numeric value is not 1. An uppercase S is inserted if the character before the !%S directive is an uppercase character; a lowercase s is inserted if the character is lowercase. |
| !%T | Inserts the system time. It takes one parameter: the address of a quadword time value to be converted to ASCII. If you specify 0, the current system time is inserted. |
| !%U | Converts a longword integer UIC to a standard UIC specification in the format [xxx,yyy], where xxx is the group number and yyy is the member number. It takes one parameter: a longword integer. The directive inserts the surrounding brackets ([ ]) and comma (,). |
| !%I | Converts a longword to the appropriate alphanumeric identifier. If the longword represents a UIC, surrounding brackets ([ ]) and comma (,) are added as necessary. If no identifier exists and the longword represents a UIC, the longword is formatted as in !%U. Otherwise it is formatted as in !XL with a preceding !%X added to the formatted result. |
| !%D | Inserts the system date and time. It takes one parameter: the address of a quadword time value to be converted to ASCII. If you specify 0, the current system date and time is inserted. |
| !n%C | Inserts a character string when the most recently evaluated argument has the value n. (Recommended for use with multilingual products.) |
| !%E | Inserts a character string when the value of the most recently evaluated argument does not match any preceding !n%C directives. (Recommended for use with multilingual products.) |
| !%F | Makes the end of a plurals statement. |
| !n< | See description of next directive (!>). |
| !> | This directive and the preceding one (!n<) are used together to define an output field width of “n” characters within which all data and directives to the right of !n< and to the left of !> are left-justified and blank-filled. It takes no parameters. |
| !n*c | Repeats the character c in the output string n times. |

### Directives for Parameter Interpretation

| Directive | Description |
| :---------: | :----------- |
| !– | Causes $FAO to reuse the most recently used parameter in the list. It takes no parameters. |
| !+ | Causes $FAO to skip the next parameter in the list. It takes no parameters. |
