#!/usr/bin/bash
make clean
rm -rf CMakeFiles
rm -rf ccov
rm -rf src/CMakeFiles
rm -rf test/CMakeFiles
find . -name 'CMakeCache.txt' -delete
find . -maxdepth 0 -name '*.cmake' -delete
find src -name '*.cmake' -delete
find test -name '*.cmake' -delete
find . -name 'cmake_install.cmake' -delete
find . -name '*.hh' -delete
find . -name '*.output' -delete
find . -name 'Makefile' -delete
find . -name '*.lis' -delete
find . -name '*.profraw' -delete
rm -rf src/MessageParser.hxx
rm -rf src/MessageParser.cxx
rm -rf src/MessageScanner.hxx
rm -rf src/MessageScanner.cxx

