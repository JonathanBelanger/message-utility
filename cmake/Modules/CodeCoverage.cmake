#
# Copyright (C) Jonathan D. Belanger 2024.
# All Rights Reserved.
#
# This software is furnished under a license and may be used and copied only in accordance with the terms of such
# license and with the inclusion of the above copyright notice.  This software or any other copies thereof may not be
# provided or otherwise made available to any other person.  No title to and ownership of the software is hereby
# transferred.
#
# The information in this software is subject to change without notice and should not be construed as a commitment by
# the author or co-authors.
#
# The author and any co-authors assume no responsibility for the use or reliability of this software.
#
# Description:
#
#   This CMake file is used to build the Message Utility executable.
#
# Revision History:
#   V01.000 18-May-2024 Jonathan D. Belanger
#   Initially written.
#

#[=======================================================================[.rst:
CodeCoverage
------------

To enable any code coverage instrumentation/targets, the CMake option of
`CODE_COVERAGE` needs to be set to `ON`.



The module defines the following variables:

::

  CODE_COVERAGE_OUTPUT_DIRECTORY    - The path where the output is written
  CODE_COVERAGE_ADDED               - ON when CODE_COVERAGE is set to ON
  LLVM_COV_PATH                     - The path to llvm-cov
  LLVM_PROFDATA_PATH                - The path to xcrun and llvm-progdata



The minimum required version of the Message Utility can be specified using
the standard syntax, e.g.  :command:`find_package(Message 1.0.0)`



::

  ====================================================================
  Example:



::

   add_code_coverage()  - For all targets

   add_library(theLib lib.cpp

   add_executable(theExe main.cpp)
   target_link_libraries(theExe PRIVATE theLib)
   target_code_coverage(theExe)     - As an executable target, adds the `cov-theExe` target



::

   add_executable(theExe main.cpp non_covered.cpp)
   target_code_coverage(theExe EXCLUDE non_covered.cpp test/*)



::

   add_code_coverage_all_targets(EXCLUDE test/*)    - Adds the `ccov-all` target

  ====================================================================
#]=======================================================================]

# Options
OPTION(CODE_COVERAGE "Builds targets with code coverage instrumentation.  (Requires GCC or Clang)" OFF)

# Programs
FIND_PROGRAM(LLVM_COV_PATH llvm-cov)
FIND_PROGRAM(LLVM_PROFDATA_PATH llvm-profdata)
FIND_PROGRAM(LCOV_PATH lcov)
FIND_PROGRAM(GENHTML_PATH genhtml)

# Hide behind the 'advanced' mode flag for GUI/ccmake
MARK_AS_ADVANCED(FORCE LLVM_COV_PATH LLVM_PROFDATA_PATH LCOV_PATH GENHTML_PATH)

# Variables
IF("X${CMAKE_COVERAGE_DIR}X" STREQUAL "XX")
    SET(CMAKE_COVERAGE_DIR ${CMAKE_BINARY_DIR}/ccov)
ENDIF()
SET_PROPERTY(GLOBAL PROPERTY JOB_POOLS ccov_serial_pool=1)
IF(${CMAKE_VERSION} VERSION_LESS "3.17.0")
    SET(remove_command "remove")
ELSE()
    SET(remove_command "rm")
ENDIF()

# Common Initializations/checks
IF(CODE_COVERAGE AND NOT CODE_COVERAGE_ADDED)
    SET(CODE_COVERAGE_ADDED ON)

    # Common targets
    FILE(MAKE_DIRECTORY ${CMAKE_COVERAGE_DIR})

    IF(CMAKE_C_COMPILER_ID MATCHES "(Apple)?[Cc]lang" OR CMAKE_CXX_COMPILER_ID MATCHES "(Apple)?[Cc]lang")
        IF(CMAKE_C_COMPILER_ID MATCHES "AppleClang" OR CMAKE_CXX_COMPILER_ID MATCHES "AppleClang")
            MESSAGE(STATUS "Building with XCode-provided llvm code coverage tools (via `xcrun`)")
            SET(LLVM_COV_PATH xcrun llvm-cov)
            SET(LLVM_PROFDATA_PATH xcrun llvm-profdata)
        ELSE()
            MESSAGE(STATUS "Building with llvm code coverage tools")
        ENDIF()

        IF(NOT LLVM_COV_PATH)
            MESSAGE(FATAL_ERROR "llvm-cov not found!  Aborting...")
        ELSE()
            EXECUTE_PROCESS(COMMAND ${LLVM_COV_PATH} --version
                            OUTPUT_VARIABLE LLVM_COV_VERSION_CALL_OUTPUT)
            STRING(REGEX MATCH "[0-9]+\\.[0-9]+\\.[0-9]+" LLVM_COV_VERSION ${LLVM_COV_VERSION_CALL_OUTPUT})

            IF(LLVM_COV_VERSION VERSION_LESS "7.0.0")
                MESSAGE(WARNING
                        "TARGET_CODE_COVERAGE()/ADD_CODE_COVERAGE_ALL_TARGETS() 'EXCLUDE' option only available on llvm-cov >= 7.0.0")
            ENDIF()
        ENDIF()

        # Targets
        ADD_CUSTOM_TARGET(ccov-clean
                          COMMAND ${CMAKE_COMMAND} -E ${remove_command} -f ${CMAKE_COVERAGE_DIR}/binaries.list
                          COMMAND ${CMAKE_COMMAND} -E ${remove_command} -f ${CMAKE_COVERAGE_DIR}/profraw.list)

        ADD_CUSTOM_TARGET(ccov-libs
                          COMMAND ;
                          COMMENT "libs ready for coverage report.")
    ELSEIF(CMAKE_C_COMPILER_ID MATCHES "GNU" OR CMAKE_CXX_COMPILER_ID MATCHES "GNU")

        # Messages
        MESSAGE(STATUS "Building with lcov Code Coverage Tools")

        IF(CMAKE_BUILD_TYPE)
            STRING(TOUPPER ${CMAKE_BUILD_TYPE} upper_build_type)
            IF(NOT ${upper_build_type} STREQUAL "DEBUG")
                MESSAGE(WARNING "Code coverage results with an optimized (non-Debug) build may be misleading")
            ENDIF()
        ELSE()
            MESSAGE(WARNING "Code coverage results with an optimized (non-Debug) build may be misleading")
        ENDIF()

        IF(NOT LCOV_PATH)
            MESSAGE(FATAL_ERROR "lcov not found!  Aborting...")
        ENDIF()

        IF(NOT GENHTML_PATH)
            MESSAGE(FATAL_ERROR "genhtml not found!  Aborting...")
        ENDIF()

        # Targets
        ADD_CUSTOM_TARGET(ccov-clean
                          COMMAND ${LCOV_PATH} --directory ${CMAKE_BINARY_DIR} --zerocounters)
    ELSE()
        MESSAGE(FATAL_ERROR "Code coverate requires Clang or GCC!  Aborting...")
    ENDIF()
ENDIF()

FUNCTION(TARGET_CODE_COVERAGE TARGET_NAME)

    # Argument parsing
    SET(options AUTO ALL EXTERNAL PUBLIC INTERFACE PLAIN)
    SET(single_value_keywords COVERAGE_TARGET_NAME)
    SET(multi_value_keywords EXCLUDE OBJECTS PRE_ARGS ARGS)
    CMAKE_PARSE_ARGUMENTS(target_code_coverage
                          "${options}" "${single_value_keywords}" "${multi_value_keywords}" ${ARGN})

    IF(${CMAKE_VERSION} VERSION_LESS "3.17.0")
        SET(remove_command "remove")
    ELSE()
        SET(remove_command "rm")
    ENDIF()

    # Set the visibility of target functions to PUBLIC, INTERFACE or default to PRIVATE
    IF(target_code_coverage_PUBLIC)
        SET(TARGET_VISIBILITY PUBLIC)
        SET(TARGET_LINK_VISIBILITY PUBLIC)
    ELSEIF(target_code_coverage_INTERFACE)
        SET(TARGET_VISIBILITY INTERFACE)
        SET(TARGET_LINK_VISIBILITY INTERFACE)
    ELSEIF(target_code_coverage_PLAIN)
        SET(TARGET_VISIBILITY PUBLIC)
        SET(TARGET_LINK_VISIBILITY)
    ELSE()
        SET(TARGET_VISIBILITY PRIVATE)
        SET(TARGET_LINK_VISIBILITY PRIVATE)
    ENDIF()

    IF(NOT target_code_coverage_COVERAGE_TARGET_NAME)

        # If a specific name was given, use that instead.
        SET(target_code_coverage_COVERAGE_TARGET_NAME ${TARGET_NAME})
    ENDIF()

    IF(CODE_COVERAGE)

        # Add code coverage instrumentation to the target's linker command
        IF(CMAKE_C_COMPILER_ID MATCHES "(Apple)?[Cc]lang" OR CMAKE_CXX_COMPILER_ID MATCHES "(Apple)?[Cc]lang")
            TARGET_COMPILE_OPTIONS(${TARGET_NAME} ${TARGET_VISIBILITY} -fprofile-instr-generate -fcoverage-mapping)
            TARGET_LINK_OPTIONS(${TARGET_NAME} ${TARGET_VISIBILITY} -fprofile-instr-generate -fcoverage-mapping)
        ELSEIF(CMAKE_C_COMPILER_ID MATCHES "GNU" OR CMAKE_CXX_COMPILER_ID MATCHES "GNU")
            TARGET_COMPILE_OPTIONS(${TARGET_NAME} ${TARGET_VISIBILITY}
                                   -fprofile-arcs -ftest-coverage
                                   $<$<COMPILE_LANGUAGE:CXX>:-fno-elide-constructors> -fno-default-inline)
            TARGET_LINK_OPTIONS(${TARGET_NAME} ${TARGET_VISIBILITY} gcov)
        ENDIF()

        # Targets
        GET_TARGET_PROPERTY(target_type ${TARGET_NAME} TYPE)

        # Add shared library to processing for 'all' targets
        IF(target_type STREQUAL "SHARED_LIBRARY" AND target_code_coverage_ALL)
            IF(CMAKE_C_COMPILER_ID MATCHES "(Apple)?[Cc]lang" OR CMAKE_CXX_COMPILER_ID MATCHES "(Apple)?[Cc]lang")
                ADD_CUSTOM_TARGET(ccov-run-${target_code_coverage_COVERAGE_TARGET_NAME}
                                  COMMAND ${CMAKE_COMMAND} -E echo "-object=$<TARGET_FILE:${TARGET_NAME}>" >>
                                          ${CMAKE_COVERAGE_DIR}/binaries.list
                                  DEPENDS ${TARGET_NAME})
                IF(NOT TARGET ccov-libs)
                    MESSAGE(FATAL_ERROR
                            "Calling target_code_coverage with 'ALL' must be after a call to 'add_code_coverage_all_targets'.")
                ENDIF()

                ADD_DEPENDENCIES(ccov-libs ccov-run-${target_code_coverage_COVERAGE_TARGET_NAME})
            ENDIF()
        ENDIF()

        # For executables add targets to run and produce output
        IF(target_type STREQUAL "EXECUTABLE")
            IF(CMAKE_C_COMPILER_ID MATCHES "(Apple)?[Cc]lang" OR CMAKE_CXX_COMPILER_ID MATCHES "(Apple)?[Cc]lang")

                # If there are static or shared objects to also work with, generate the string to add them here
                FOREACH(LINK_OBJECT ${target_code_coverage_OBJECTS})

                    # Check to see if the target is a shared object
                    IF(TARGET ${LINK_OBJECT})
                        GET_TARGET_PROPERTY(LINK_OBJECT_TYPE ${LINK_OBJECT} TYPE)
                        IF(${LINK_OBJECT_TYPE} STREQUAL "STATIC_LIBRARY" OR ${LINK_OBJECT_TYPE} STREQUAL "SHARED_LIBRARY")
                            SET(LINKED_OBJECTS ${LINKED_OBJECTS -object=$<TARGET_FILE:${LINK_OBJECT}>)
                        ENDIF()
                    ENDIF()
                ENDFOREACH()

                # Run the executable, generating raw profile data, make the run data available for further processing.
                # Separated to allow Windows to run this target serially.
                ADD_CUSTOM_TARGET(ccov-run-${target_code_coverage_COVERAGE_TARGET_NAME}
                                  COMMAND ${CMAKE_COMMAND} -E env ${CMAKE_CROSSCOMPILING_EMULATOR}
                                          ${target_code_coverage_PRE_ARGS}
                                          LLVM_PROFILE_FILE=${target_code_coverage_COVERAGE_TARGET_NAME}.profraw
                                          $<TARGET_FILE:${TARGET_NAME}> ${target_code_coverage_ARGS}
                                  COMMAND ${CMAKE_COMMAND} -E echo "-object=$<TARGET_FILE:${TARGET_NAME}>"
                                          ${LINKED_OBJECTS} >>
                                          ${CMAKE_COVERAGE_DIR}/binaries.list
                                  COMMAND ${CMAKE_COMMAND} -E echo
                                          "${CMAKE_CURRENT_BINARY_DIR}/${target_code_coverage_COVERAGE_TARGET_NAME}.profraw" >>
                                          ${CMAKE_COVERAGE_DIR}/profraw.list
                                  JOB_POOL ccov_serial_pool
                                  DEPENDS ccov-libs ${TARGET_NAME})

                # Merge the generated profile data so llvm-cov can process it.
                ADD_CUSTOM_TARGET(ccov-processing-${target_code_coverage_COVERAGE_TARGET_NAME}
                                  COMMAND ${LLVM_PROFDATA_PATH} merge
                                          -sparse ${target_code_coverage_COVERAGE_TARGET_NAME}.profraw
                                          -o ${target_code_coverage_COVERAGE_TARGET_NAME}.profdata
                                  DEPENDS ccov-run-${target_code_coverage_COVERAGE_TARGET_NAME})

                # Regex only works on LLVM >= 7.0.0
                IF(LLVM_COV_VERSION VERSION_GREATER_EQUAL "7.0.0")
                    FOREACH(EXCLUDE_ITEM ${target_code_coverage_EXCLUDE})
                        SET(EXCLUDE_REGEX ${EXCLUDE_REGEX} -ignore-filename-regex='${EXCLUDE_ITEM}')
                    ENDFOREACH()
                ENDIF()

                # Print out details of the coverage information to the command-line
                ADD_CUSTOM_TARGET(ccov-show-${target_code_coverage_COVERAGE_TARGET_NAME}
                                  COMMAND ${LLVM_COV_PATH} show $<TARGET_FILE:${TARGET_NAME}>
                                          -instr-profile=${target_code_coverage_COVERAGE_TARGET_NAME}.profdata
                                          -show-line-counts-or-regions ${LINKED_OBJECTS} ${EXCLUDE_REGEX}
                                  DEPENDS ccov-processing-${target_code_coverage_COVERAGE_TARGET_NAME})

                # Print out a summary of the coverage information to the command-line
                ADD_CUSTOM_TARGET(ccov-report-${target_code_coverage_COVERAGE_TARGET_NAME}
                                  COMMAND ${LLVM_COV_PATH} report $<TARGET_FILE:${TARGET_NAME}>
                                          -instr-profile=${target_code_coverage_COVERAGE_TARGET_NAME}.profdata
                                          ${LINKED_OBJECTS} ${EXCLUDE_REGEX}
                                  DEPENDS ccov-processing-${target_code_coverage_COVERAGE_TARGET_NAME})

                # Export coverage information so continuous intgration tools (e.g, Jenkins) can consume it.
                ADD_CUSTOM_TARGET(ccov-export-${target_code_coverage_COVERAGE_TARGET_NAME}
                                  COMMAND ${LLVM_COV_PATH} $<TARGET_FILE:${TARGET_NAME}>
                                          -instr-profile=${target_code_coverage_COVERAGE_TARGET_NAME}.profdata
                                          -format="text" ${LINKED_OBJECTS} ${EXCLUDE_REGEX} >
                                          ${CMAKE_COVERAGE_DIR}/${target_code_coverage_COVERAGE_TARGET_NAME}.json
                                  DEPENDS ccov-processing-${target_code_coverage_COVERAGE_TARGET_NAME})

                # Generate HTML output of the coverage inforamtion for user review
                ADD_CUSTOM_TARGET(ccov-${target_code_coverage_COVERAGE_TARGET_NAME}
                                  COMMAND ${LLVM_COV_PATH} show $<TARGET_FILE:${TARGET_NAME}>
                                          -instr-profile=${target_code_coverage_COVERAGE_TARGET_NAME}.profdata
                                          -show-line-counts-or-regions
                                          -output-dir=${CMAKE_COVERAGE_DIR}/${target_code_coverage_COVERAGE_TARGET_NAME}
                                          -format="html" ${LINKED_OBJECTS} ${EXCLUDE_REGEX}
                                  DEPENDS ccov-processing-${target_code_coverage_COVERAGE_TARGET_NAME})

            ELSEIF(CMAKE_C_COMPILER_ID MATCHES "GNU" OR CMAKE_CXX_COMPILER_ID MATCHES "GNU")
                SET(coverage_info "${CMAKE_COVERAGE_DIR}/${target_code_coverage_COVERAGE_TARGET_NAME}.info")

                # Run the executable, generating the coverage information.
                ADD_CUSTOM_TARGET(conv-run-${target_code_coverage_COVERAGE_TARGET_NAME}
                                  COMMAND ${CMAKE_CROSSCOMPILING_EMULATOR} ${target_code_coverage_PRE_ARGS}
                                          $<TARGET_FILE:${TARGET_NAME}> ${target_code_coverage_ARGS}
                                  DEPENDS ${TARGET_NAME})

                # Generate exclusion string for later processing
                FOREACH(EXCLUDE_ITEM ${target_code_coverage_EXCLUDE})
                    SET(EXCLUDE_REGEX ${EXCLUDE_REGEX} --remove ${coverage_info} '${EXCLUDE_ITEM}')
                ENDFOREACH()

                IF(EXCLUDE_REGEX)
                    SET(EXCLUDE_COMMAND ${LCOV_PATH} ${EXCLUDE_REGEX} --output-file ${coverage_info})
                ELSE()
                    SET(EXCLUDE_COMMAND ;)
                ENDIF()

                IF(NOT ${target_code_coverage_EXTERNAL})
                    SET(EXTERNAL_OPTION --no-external)
                ENDIF()

                # Capture coverage data
                ADD_CUSTOM_TARGET(ccov-capture-${target_code_coverage_COVERAGE_TARGET_NAME}
                                  COMMAND ${CMAKE_COMMAND} -E ${remove_command} -f ${coverage_info}
                                  COMMAND ${LCOV_PATH} --directory ${CMAKE_BINARY_DIR} --zerocounters
                                  COMMAND ${CMAKE_CROSSCOMPILING_EMULATOR} ${target_code_coverage_PRE_ARGS}
                                          $<TARGET_FILE:${TARGET_NAME}> ${target_code_coverage_ARGS}
                                  COMMAND ${LCOV_PATH} --directory ${CMAKE_BINARY_DIR}
                                          --base-directory ${CMAKE_SOURCE_DIR} --capture ${EXTERNAL_OPTION}
                                          --output-file ${coverage_info}
                                  COMMAND ${EXCLUDE_COMMAND}
                                  DEPENDS ${TARGET_NAME})

                # Generate HTML output of the coverage information for review.
                ADD_CUSTOM_TARGET(ccov-${target_code_coverage_COVERAGE_TARGET_NAME}
                                  COMMAND ${GENHTML_PATH}
                                          -o ${CMAKE_COVERAGE_DIR}/${target_code_coverage_COVERAGE_TARGET_NAME}
                                          ${coverage_info}
                                  DEPENDS ccov-capture-${target_code_coverage_COVERAGE_TARGET_NAME})
            ENDIF()

            ADD_CUSTOM_COMMAND(TARGET ccov-${target_code_coverage_COVERAGE_TARGET_NAME} POST_BUILD
                               COMMAND ;
                               COMMENT "Open ${CMAKE_COVERAGE_DIR}/${target_code_coverage_COVERAGE_TARGET_NAME}/index.html in your browser to review the coverage report.")

            # AUTO
            IF(target_code_coverage_AUTO)
                IF(NOT TARGET ccov)
                    ADD_CUSTOM_TARGET(ccov)
                ENDIF()
                ADD_DEPENDENCIES(ccov ccov-${target_code_coverage_COVERAGE_TARGET_NAME})

                IF(NOT CMAKE_C_COMPILER_ID MATCHES "GNU" AND NOT CMAKE_CXX_COMPILER_ID MATCHES "GNU")
                    IF(NOT TARGET ccov-report)
                        ADD_CUSTOM_TARGET(ccov-report)
                    ENDIF()
                    ADD_DEPENDENCIES(ccov-report ccov-report-${target_code_coverage_COVERAGE_TARGET_NAME})
                ENDIF()
            ENDIF()

            # ALL
            IF(target_code_coverage_ALL)
                IF(NOT TARGET ccov-all-processing)
                    MESSAGE(FATAL_ERROR
                            "Calling target_code_coverage with 'ALL' must be after a call to 'add_code_coverage_all_targets'.")
                ENDIF()

                ADD_DEPENDENCIES(ccov-all-processing ccov-run-${target_code_coverage_COVERAGE_TARGET_NAME})
            ENDIF()
        ENDIF()
    ENDIF()
ENDFUNCTION()

FUNCTION(ADD_CODE_COVERAGE)
    IF(CODE_COVERAGE)
        IF(CMAKE_C_COMPILER_ID MATCHES "(Apple)?[Cc]lang" OR CMAKE_CXX_COMPILER_ID MATCHES "(Apple)?[Cc]lang")

            ADD_COMPILE_OPTIONS(-fprofile-instr-generate -fcoverage-mapping)
            ADD_LINK_OPTIONS(-fprofile-instr-generate -fcoverage-mapping)
        ELSEIF(CMAKE_C_COMPILER_ID MATCHES "GNU" OR CMAKE_CXX_COMPILER_ID MATCHES "GNU")

            ADD_COMPILE_OPTIONS(-fprofile-arcs -ftest-coverage
                                $<$<COMPILE_LANGUAGE:CXX>:-fno-elide-constructors> -fno-default-inline)
            LINK_LIBRARIES(gcov)
        ENDIF()
    ENDIF()
ENDFUNCTION()

FUNCTION(ADD_CODE_COVERAGE_ALL_TARGETS)

    # Argument parsing
    SET(multi_value_keywords EXCLUDE)
    CMAKE_PARSE_ARGUMENTS(add_code_coverage_all_targets "" "" "${multi_value_keywords}" ${ARGN})

    IF(${CMAKE_VERSION} VERSION_LESS "3.17.0")
        SET(remove_command "remove")
    ELSE()
        SET(remove_command "rm")
    ENDIF()

    IF(CODE_COVERAGE)
        IF(CMAKE_C_COMPILER_ID MATCHES "(Apple)?[Cc]lang" OR CMAKE_CXX_COMPILER_ID MATCHES "(Apple)?[Cc]lang")

            # Merge the profile data for all of the run executables
            IF(WIN32)
                ADD_CUSTOM_TARGET(ccov-all-processing
                                  COMMAND powershell -Command $$FILELIST = Get-Content ${CMAKE_COVERAGE_DIR}/profraw.list\;
                                          merge -o ${CMAKE_COVERAGE_DIR}/all-merged.profdata -sparse $$FILELIST)
            ELSE()
                ADD_CUSTOM_TARGET(ccov-all-processing
                                  COMMAND ${LLVM_PROFDATA_PATH} merge -o ${CMAKE_COVERAGE_DIR}/all-merged.profdata
                                          -sparse `cat ${CMAKE_COVERAGE_DIR}/profraw.list`)
            ENDIF()

            # Regex exclude
            IF(LLVM_COV_VERSION VERSION_GREATER_EQUAL "7.0.0")
                FOREACH(EXCLUDE_ITEM ${add_code_coverage_all_targets_EXCLUDE})
                    SET(EXCLUDE_REGEX ${EXCLUDE_REGEX} -ignore-filename-regex='${EXCLUDE_ITEM}')
                ENDFOREACH()
            ENDIF()

            # Print summary of the code coverage information to the command-line
            IF(WIN32)
                ADD_CUSTOM_TARGET(ccov-all-report
                                  COMMAND powershell -Command $$FILELIST = Get-Content ${CMAKE_COVERAGE_DIR}/binaries.list;
                                          ${LLVM_COV_PATH} report $$FILELIST
                                          -instr-profile=${CMAKE_COVERAGE_DIR}/all-merged.profdata ${EXCLUDE_REGEX}
                                  DEPENDS ccov-all-processing)
            ELSE()
                ADD_CUSTOM_TARGET(ccov-all-report
                                  COMMAND ${LLVM_COV_PATH} report `cat ${CMAKE_COVERAGE_DIR}/binaries.list`
                                          -instr-profile=${CMAKE_COVERAGE_DIR}/all-merged.profdata ${EXCLUDE_REGEX}
                                  DEPENDS ccov-all-processing)
            ENDIF()

            # Export coverage information so continuous integration tools (e.g., Jenkins) can consume it
            IF(WIN32)
                ADD_CUSTOM_TARGET(ccov-all-export
                                  COMMAND powershell -Command $$FILELIST = Get-Content ${CMAKE_COVERAGE_DIR}/binaries.list;
                                          ${LLVM_COV_PATH} export $$FILELIST
                                          -instr-profile=${CMAKE_COVERAGE_DIR}/all-merged.profdata
                                          -format="text" ${EXCLUDE_REGEX} > ${CMAKE_COVERAGE_DIR}/coverage.json
                                  DEPENDS ccov-all-processing)
            ELSE()
                ADD_CUSTOM_TARGET(ccov-all-export
                                  COMMAND ${LLVM_COV_PATH} export `cat ${CMAKE_COVERAGE_DIR}/binaries.list`
                                          -instr-profile=${CMAKE_COVERAGE_DIR}/all-merged.profdata
                                          -format="text" ${EXCLUDE_REGEX} > ${CMAKE_COVERAGE_DIR}/coverage.json
                                  DEPENDS ccov-all-processing)
            ENDIF()

            # Generate HTML output for all added targets for user review
            IF(WIN32)
                ADD_CUSTOM_TARGET(ccov-all
                                  COMMAND powershell -Command $$FILELIST = Get-Content ${CMAKE_COVERAGE_DIR}/binaries.list;
                                          ${LLVM_COV_PATH} show $$FILELIST
                                          -instr-profile=${CMAKE_COVERAGE_DIR}/all-merged.profdata
                                          -show-line-counts-or-regions
                                          -output-dir=${CMAKE_COVERAGE_DIR}/all-merged
                                          -format="html" ${EXCLUDE_REGEX}
                                  DEPENDS ccov-all-processing)
            ELSE()
                ADD_CUSTOM_TARGET(ccov-all
                                  COMMAND ${LLVM_COV_PATH} show `cat ${CMAKE_COVERAGE_DIR}/binaries.list`
                                          -instr-profile=${CMAKE_COVERAGE_DIR}/all-merged.profdata
                                          -show-line-counts-or-regions
                                          -output-dir=${CMAKE_COVERAGE_DIR}/all-merged
                                          -format="html" ${EXCLUDE_REGEX}
                                  DEPENDS ccov-all-processing)
            ENDIF()
        ELSEIF(CMAKE_C_COMPILER_ID MATCHES "GNU" OR CMAKE_CXX_COMPILER_ID MATCHES "GNU")
            SET(coverage_info "${CMAKE_COVERAGE_DIR}/all-merged.info")

            # Nothing required for gcov
            ADD_CUSTOM_TARGET(ccov-all-processing
                              COMMAND ;)
            # Regex exclude
            SET(EXCLUDE_REGEX)
            FOREACH(EXCLUDE_ITEM ${add_code_coverage_all_targets_EXCLUDE})
                SET(EXCLUDE_REGEX ${EXCLUDE_REGEX} -ignore-filename-regex='${EXCLUDE_ITEM}')
            ENDFOREACH()

            IF(EXCLUDE_REGEX)
                SET(EXCLUDE_COMMAND ${LCOV_PATH} ${EXCLUDE_REGEX} --output-file ${coverage_info})
            ELSE()
                SET(EXCLUDE_COMMAND ;)
            ENDIF()

            ADD_CUSTOM_TARGET(ccov-all-capture
                              COMMAND ${CMAKE_COMMAND} -E ${remove_command} -f ${coverage_info}
                              COMMAND ${LCOV_PATH} --directory ${CMAKE_BINARY_DIR} --capture --output-file ${coverate_info}
                              COMMAND ${EXCLUDE_COMMAND}
                              DEPENDS ccov-all-processing)

            ADD_CUSTOM_TARGET(ccov-all
                              COMMAND ${GENHTML_PATH -o ${CMAKE_COVERAGE_DIR}/all-merged
                                      ${coverage_info} -p ${CMAKE_SOURCE_DIR}
                              DEPENDS ccov-all-capture)

        ENDIF()

        ADD_CUSTOM_COMMAND(TARGET ccov-all POST_BUILD
                           COMMAND ;
                           COMMENT "Open ${CMAKE_COVERAGE_DIR}/all-merged/index.html in your browser to review the coverage report.")

    ENDIF()
ENDFUNCTION()
